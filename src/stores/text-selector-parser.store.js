import { observable, decorate, action,computed,runInAction } from 'mobx';

class TextSelectorParser{
    
    sObj={};

    paraId(pElem){

        try{ 
            
            if (pElem.attributes['data-para-index']){
                //console.log("paraId matches first");
                return parseInt(pElem.attributes['data-para-index'].value);
            }

            if (pElem.querySelector('.text-para')!==null){
                //console.log("paraId matches second");
                return parseInt(pElem.querySelector('.text-para').getAttribute('data-para-index'));
            }

            if (pElem.parentElement && pElem.parentElement.attributes['data-para-index']){
                //console.log("paraId matches third");
                return parseInt(pElem.parentElement.attributes['data-para-index'].value);
            }
            
            throw new Error("None of the options match data-para-index");
            

        }catch(err){
            throw new Error(err);
        }

    }

    get fromParaId(){

        let fromParaId=null;
        try{
            let sObj=this.sObj;
            let pElem=sObj.range.startContainer.parentElement;
            fromParaId= this.paraId(pElem);
        }catch(err){
            console.log("Could not fetch fromParaId of parent (attribute data-para-index) out of sObj",this.sObj);
            console.log("err",err);
        }
        return fromParaId;
    }

    get lastParaId(){

        let lastParaId=null;
        try{
            let sObj=this.sObj;
            let pElem=sObj.range.startContainer.parentElement;
            lastParaId= this.paraId(pElem);
        }catch(err){
            console.log("Could not fetch lastParaId of parent (attribute data-para-index) out of sObj",this.sObj);
            console.log("err",err);
        }
        return lastParaId;
    }
}

decorate(TextSelectorParser,{sObj:observable});

export default new TextSelectorParser;
import { observable, decorate, action,computed,runInAction } from 'mobx';
import Auth from './../modules/auth/Auth';
import _ from 'lodash';
import BibleIndex from './../handlers/BibleIndex';

class TextStore{

    text = {};
    notes=[];
    customDocName=null;
    isCustomized=true;
    showGenericTitles=false;
    documentName="";
    showLocations=false;
    showUnknowns=false;
    showPersons=false;
    showComments=true;
    dPhrasesObj={};
    chaptersSelection={};

    constructor(){
        this.bibleIndex=new BibleIndex();
    }
    
    
    set text(text){
        this.text=text;
    }

    toggleIsCustomized(param=null){
        

        this.isCustomized=!this.isCustomized;
    }

    set distinctPhrasesObj(dPhrasesObj){
        this.dPhrasesObj=dPhrasesObj;

    }

    
    get distinctPhrases(){

        let phrases=[],indexes=[],
            noteType=this.dPhrasesObj.noteType,
            titleKey=this.dPhrasesObj.titleKey;

        if (!this.text[noteType] || this.text[noteType].length==0){
            return {words:[],indexes:[]};
        }
        this.text[noteType].forEach(r=>{
            if (phrases.indexOf(r[titleKey])==-1){
                phrases.push(r[titleKey]);
                indexes.push(r.id);
            }
        });
        return {words:phrases,indexes:indexes};
    }

    async loadChaptersSelection(selection){

        this.chaptersSelection=selection;
        
        console.log("text.store loadChaptersSelection with selection",selection);
        let textObj=null;

        let [res,err] = await Auth.superAuthFetch('/api/Paragraphs/getParagraphsRange?fromBook='+selection.fromBook+'&fromChapter='+selection.fromChapter
            +'&fromParaNum='+selection.fromParaNum+'&toBook='+selection.toBook+'&toChapter='+selection.toChapter+'&toParaNum='+selection.toParaNum
            );
        //let [res,err] = await Auth.superAuthFetch('/api/Paragraphs/getParagraphs?book='+selection.fromBook+'&chapter='+selection.fromChapter);
        console.log("getParagraphs err",err);
        console.log("getParagraphs res",res);
        
        if (err){
                console.error("loadChaptersSelection err",err);
                this.text={};
                return;
         }

        runInAction(()=>{

            //console.log("loadChaptersSelection is launched again!?!?!?!?!?");
            
            this.text={
                ...res,
                title:selection.fromBookHe+" "+selection.fromChapterHe
            };
            this.notes=res.notes;
            
            //this.text={books:[{questions:[],paras:[],notes:[],title:""}]};
        });


    }

    updateText(entry,val){
        
        let fRes={...this.text};

        if (fRes && fRes[entry] && Array.isArray(fRes[entry])){
            fRes[entry].push(val);    
            this.text=fRes;
        }
    }


    replaceNewNote(entry,val){
        
        let fRes={...this.text};

        //console.log("FRES?! before",fRes);

        if (fRes && fRes[entry] && Array.isArray(fRes[entry])){
            let newNoteIndex=fRes[entry].findIndex(x=>x.isNew && x.isNew===true);
            if (newNoteIndex!==-1){
                fRes[entry][newNoteIndex]=val;    
                this.text=fRes;
            }
            
            
        }
        console.log("this.after",this.text);

    }

    replaceNoteById(entry,val,id){
        
        let fRes={...this.text};

        if (fRes && fRes[entry] && Array.isArray(fRes[entry])){
            let newNoteIndex=fRes[entry].findIndex(x=>x.id && x.id==id);
            if (newNoteIndex!==-1){
                console.log("Found fRes[entry][newNoteIndex]",fRes[entry][newNoteIndex]);
                fRes[entry][newNoteIndex]=val;    
                console.log("New entry now is val",val);
                this.text=fRes;
            }
        }
        console.log("fRes[entry] after",fRes[entry]);

    }
    removeNoteById(noteType,noteId){
        if (!this.text[noteType]){
            console.log("Cannot removeNoteById, no such noteType (%s) ",noteType);
            return;
        }        

        let noteIndex=this.text[noteType].findIndex(x=>x.id && x.id===noteId);
        if (noteIndex==-1){
            console.log("Cannot removeNoteById, no note with id (%d)",noteId);
            return;
        }
        this.text[noteType].splice(noteIndex,1);

    }
    removeNewNote(noteType){

        if (!this.text[noteType]){
            console.log("Cannot removeNewNote, no such noteType (%s) ",noteType);
            return;
        }

        let newNoteIndex=this.text[noteType].findIndex(x=>x.isNew && x.isNew===true);
        if (newNoteIndex==-1){
            console.log("Cannot removeNewNote, no note with isNew true");
            return;
        }
        this.text[noteType].splice(newNoteIndex,1);
        
    
    }

    async loadDocument(docId){

        this.text={};
        let [docRes,err]=await Auth.superAuthFetch('/api/Documents/getUserDocument?docId='+docId);
        if (err){
            console.log("Could not load document, err",err);
            return;
        }

        runInAction(()=>{

            console.log("document res!!",docRes);
            this.text={...docRes};
            this.notes=docRes.notes;
            
        });
        


    }


    setDocumentName(docArgs){
        console.log("set document name with args",docArgs);

        let fromBook=docArgs.fromBookHe;
        let fromChapter=this.bibleIndex.getHeNumByIndex(docArgs.fromChapter-1);
        let fromParaNum=this.bibleIndex.getHeNumByIndex(docArgs.fromParaNum);
        let toBook=this.bibleIndex.getBookNameByLang(docArgs.fromBook,'he');
        let toChapter=this.bibleIndex.getHeNumByIndex(docArgs.toChapter-1);
        let toParaNum=this.bibleIndex.getHeNumByIndex(docArgs.toParaNum);

        let documentName="";

        if (fromBook==toBook){
            documentName= fromBook+" "+fromChapter+":"+fromParaNum+" - "+toChapter+":"+toParaNum;
        }else{
            documentName= fromBook+" "+fromChapter+":"+fromParaNum+" - "+toBook+" "+toChapter+":"+toParaNum;
        }
        this.documentName=documentName;
    }
    
}

decorate(TextStore,{text:observable,documentName:observable,customDocName:observable,
    isCustomized:observable,loadDocument:action,loadChaptersSelection:action,toggleIsCustomized:action,
    showGenericTitles:observable,showLocations:observable,showUnknowns:observable,showPersons:observable,
    showComments:observable,distinctPhrases:computed,chaptersSelectio:observable});

export default new TextStore;
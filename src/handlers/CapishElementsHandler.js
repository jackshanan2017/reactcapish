import Consts from './../constants/Consts';
import ElementsHandler from './ElementsHandler';

export default class CapishElementsHandler extends ElementsHandler{

    constructor(ctx){
        super(ctx);
    }

    addElement=(key,comp,groupKey=null)=>{

        if (groupKey==Consts.ELEMENTS_GROUP_POPUPS && this.groups[groupKey]){
            let elems=this.groups[groupKey];
            
            elems.forEach(elKey=>{
                this.removeElement(elKey)
            });
        }

        super.addElement(key,comp,groupKey);


    }


}
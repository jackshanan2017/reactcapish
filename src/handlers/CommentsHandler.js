export default class CommentsHandler{

    
    constructor(showCommentFn=()=>{}){
        this.showCommentFn=showCommentFn;
    }
    
    highlightComment=(noteData)=>{

        //console.log("Hightlight comment with noteData",noteData);

        let toParaEl=document.querySelector("[data-para-index='"+noteData.to_para_id+"']")//.innerHTML;        
        let fromParaEl=document.querySelector("[data-para-index='"+noteData.from_para_id+"']");

        //console.log("highlightComment toParaEl",toParaEl);
        //console.log("highlightComment fromParaEl",fromParaEl);

        if (noteData.to_para_id==noteData.from_para_id){

            //console.log("highlight single para");

            fromParaEl.innerHTML=fromParaEl.innerHTML.substr(0,noteData.from_char)
            +"<span class='highlight show-comment'>"+fromParaEl.innerHTML
            .substr(noteData.from_char,noteData.to_char-noteData.from_char+1)
            +"</span>"+fromParaEl.innerHTML.substr(noteData.to_char+1);

            fromParaEl.querySelector(".highlight").addEventListener("click",(e)=>{
                    //this.props.textContext.showComment(noteData.id);
                    let elem = e.target || e.srcElement;
                    if (elem.classList.contains('show-comment')){
                        console.log("show comment with id?",noteData.id);
                        this.showCommentFn(noteData);    
                    }
                    
                    
            });

        }else{

            //console.log("highlight multiple paras");

            let paraNum=noteData.from_para_id;
            let paraEl=null;
            
            while (paraNum<=noteData.to_para_id){

                if (paraNum==noteData.from_para_id){

                    fromParaEl.innerHTML=fromParaEl.innerHTML.substr(0,noteData.from_char)+"<span class='highlight show-comment'>"
                    +fromParaEl.innerHTML.substr(noteData.from_char)+"</span>";
                    paraEl=fromParaEl;

                }else if(paraNum==noteData.to_para_id){

                    toParaEl.innerHTML="<span class='highlight show-comment'>"+toParaEl.innerHTML.substr(0,noteData.to_char)+"</span>"
                    +toParaEl.innerHTML.substr(noteData.to_char);
                    paraEl=toParaEl;

                }else{

                    paraEl=document.querySelector("[data-para-index='"+paraNum+"']");
                    paraEl.innerHTML="<span class='highlight'>"+paraEl.innerHTML+"</span>";

                }

                paraEl.querySelector(".highlight").addEventListener("click",e=>{
                    let elem = e.target || e.srcElement;
                    if (elem.classList.contains('show-comment')){
                        console.log("show comment with id?",noteData.id);
                        this.showCommentFn(noteData);    
                    }
                });
      
                paraNum++;

            }
        }
    }
}
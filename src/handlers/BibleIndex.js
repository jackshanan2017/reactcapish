import bibleIndex from './../constants/agg-index.json';

export default class BibleIndex{

    constructor(){

        this.bibleIndex=bibleIndex;

        //this.bibleIndex={
            //Genesis:{name:{he:"בראשית"},chapters:[20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20]},
            //Exodus:{name:{he:"שמות"},chapters:[20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20]},
            //Leviticus:{name:{he:"ויקרא"},chapters:[20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20]},
            //Numbers:{name:{he:"במדבר"},chapters:[20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20]},
            //Deuteronomy:{name:{he:"דברים"},chapters:[20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20]},
        //};

        this.heNums=['א','ב','ג','ד','ה','ו','ז','ח','ט','י','יא','יב','יג','יד','טו','טז','יז','יח','יט','כ',
        'כא','כב','כג','כד','כה','כו','כז','כח','כט','ל','לא','לב','לג','לד','לה','לז','לח','לט','מ',
        'מא','מב','מג','מד','מה','מו','מז','מח','מט','נ','נא','נב','נג','נד','נה','נו','נז','נח','נט','ס',
        'סא','סב','סג','סד','סה','סו','סז','סח','סט','ע','עא','עב'];
    }

    getChaptersStringForSefaria(){
        let list=[];
        Object.keys(this.bibleIndex).forEach(bookName=>{
            //console.log("bookName",bookName);
            let bookObj=this.bibleIndex[bookName];
            bookObj.chapters.forEach((ch,chIndex)=>{
                list.push(bookName+"."+(parseInt(chIndex)+1));
            });
        });
        return list;
    }


    
    getParasListByChapter(bookName='Genesis',chapterIndex=0){
        return this.heNums.slice(0,this.bibleIndex[bookName].chapters[chapterIndex]);

    }
    getChaptersListByBookName(bookName='Genesis'){

        return this.heNums.slice(0,this.bibleIndex[bookName].chapters.length);
    }
    getBooksList(){
        return Object.keys(this.bibleIndex).map( (key)=>{
            //{en:"Genesis",he:"בראשית"},
            return {en:key,he:this.bibleIndex[key].name.he};
        });
    }

    getHeNumByIndex(index){
        return this.heNums[index];
    }

    heNumToIndex(heNum){
        return this.heNums.indexOf(heNum)+1;
    }

    getEnglishBookName(bookname,lang){

       let enBookName=null;
       Object.keys(this.bibleIndex).forEach(enBook=>{
           if (this.bibleIndex[enBook].name[lang] == bookname){
               enBookName=enBook;
           }
       });
       return enBookName;
    }

    
    getBookNameByLang(bookName,lang){
        if (!this.bibleIndex[bookName]){
            console.loG("BibleIndex.getBookNameByLang could not find book by name:",bookName);
            return "";
        }

        return this.bibleIndex[bookName].name[lang];

    }
    
    getBookNameByIndex(i,lang='en'){

        //console.log("this.bibleIndex[0]?",this.bibleIndex[ (Object.keys(this.bibleIndex)[0])   ] );

        if (!this.bibleIndex[ (Object.keys(this.bibleIndex)[i])]){
            console.log("No index for bibleIndex[%d]",i);
            return null;
        }

        let entry=this.bibleIndex[ (Object.keys(this.bibleIndex)[i])];

        if (lang==='en'){
            return Object.keys( this.bibleIndex  )[i];
        }

        if (lang!=='en'){
            if (entry.name[lang]){
                return entry.name[lang];
            }
        }

        return null;

    }    

    getBibleIndex(){
        return this.bibleIndex;
    }

}

//module.exports=BibleIndex;
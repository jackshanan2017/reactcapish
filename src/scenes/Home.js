import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Redirect } from 'react-router';
//import Login from './../modules/auth/Login';
import Auth from './../modules/auth/Auth';
import { observer,inject } from 'mobx-react';
//import { extendObservable } from 'mobx';
import StudyPage from './StudyPage';
import capishLogoImg from './../img/capish-logo-yougotit.gif';
import hertzogImg from './../img/hertzog-tanach.gif';
import './home.scss';
import lampImg from './../img/capish-homepage-lamp.gif';
import ElementsHandler from './../handlers/ElementsHandler';
import {LoginModalMenu} from './../components/modals/LoginModal';
import {LoginModalForm} from './../components/modals/LoginModal';
import Consts from './../constants/Consts';



class Home extends Component {

    constructor(props) {
        super(props);
        
        this.elementsHandler=new ElementsHandler(this);
        this.state={elements:this.elementsHandler.getElements()};

    }

    openLoginModal=async()=>{

        let isAuth=await Auth.isAuthenticated();

        console.log("openLoginModal isAuth?",isAuth);

        if (isAuth){
            console.log("already authenticated");
            window.location='/study-page';
            return;
        }
        this.elementsHandler.addElement(Consts.ELEMENTS_LOGIN_MODAL_MENU,<LoginModalMenu key={0} ctx={this} />);
        //this.elementsHandler.addElement(Consts.ELEMENTS_LOGIN_MODAL_FORM,<LoginModalForm key={0} ctx={this} />);
    
    }



    

    render() {
        return(
            <div className='home-view'>
                {this.state.elements.map(e=>e.elem)}
                <nav className="navbar navbar-default">
                    <div className="container">    
                        
                        <div className="navbar-header">
                          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                          </button>
                        </div>


                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                          
                          <div className="row nav-items">

                              <ul className="col-sm-3 nav-left">
                                <li>
                                    <button className='student-submit-btn'>הבא</button>
                               </li>
                                <li>
                                    <input type='text' className='student-code-input' placeholder="סטודנט? הקלד קוד כאן" />
                                </li>
                              </ul>
                              
                              <div className="col-sm-6 nav-center">
                                
                                <ul>
                                <li>יצירה</li>
                                <li>אודות</li>
                                <li onClick={this.openLoginModal}>לוג-אין</li>
                                </ul>

                              </div>

                              <ul className="col-sm-3 nav-right">            
                                <li className="capish-logo-new"><a className="" href="#"><img src={capishLogoImg} /></a></li>
                                <li className="hertzog-logo"><a className="" href="#"><img src={hertzogImg} /></a></li>
                              </ul>

                          </div>
                          


                        </div>
                    </div>
                </nav>

                <section className="container body">
                    <div className="row">
                        <div className="col-sm-6 lamp-container">
                            <img className="lamp-img ib" src={lampImg} />
                        </div>
                        <div className="col-sm-6 description-container">
                            <div className='description-box ib'>
                                <div className='title'><span className='logo-title'>קאפיש.</span> פשוט, להבין.</div>
                                <div className='sub-title'>לימוד תנ"ך על ידי קריאה פעילה עם מגוון כלים המאפשרים שיתופיות, עומק, חשיבה יצירתית ולמידה חוויתית.</div>
                            </div>
                        </div>
                    </div>
                </section>


            </div>
        );
    }
}
export default inject('TextStore')(observer(Home));
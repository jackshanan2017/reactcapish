import React, { Component } from 'react';
import './StudyPage.scss';
import './../components/modals/Modals.scss';
import TopNavBar from  './../components/TopNavBar';
import TextHolder from './../components/TextHolder';
import ControlBar from './../components/ControlBar';
import CommentsHandler from './../handlers/CommentsHandler';
import Consts from './../constants/Consts';

import CapishElementsHandler from './../handlers/CapishElementsHandler';
import AsyncTools from './../modules/tools/AsyncTools';
import ChaptersSelectionModal from './../components/modals/ChaptersSelectionModal';
import { observer,inject } from 'mobx-react';
import { observe,observable,autorun,intercept,when } from 'mobx';
import CommentBox from './../components/CommentBox';
import BibleIndex from './../handlers/BibleIndex';

import keyImg from './../img/key.png';
import iglassImg from './../img/inspect-glass.png';
import heartImg from './../img/heart-env.png';
import prevImg from './../img/prev-page.png';
import apersonImg from './../img/add-person.png';
import avatarImg from './../img/avatar-sketch.png';
import pinImg from './../img/pin.png';


const DEBUG_MODE=false;


function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    return null;
    console.log('Query variable %s not found', variable);
}






class StudyPage extends Component{
    
    constructor(props){
        super(props);

        this.textHolderElem=React.createRef();
        this.elementsHandler=new CapishElementsHandler(this);
        this.bibleIndex=new BibleIndex();
        this.state={text:null,elements:this.elementsHandler.getElements()};
        this.controllers={showLocations:false}; //DEPRECATED?

        //this.mousePos=observable({pos:{x:0,y:0}});

        
        
    }

    getControllers=()=>{
        return this.controllers;
    }


    async componentDidMount(){
        
        
        let document=getQueryVariable('document');
        console.log("document?",document);
        if (document && !isNaN(document) && document>0 && document<99999999){
            
            await this.props.TextStore.loadDocument(document);
            this.drawNotes(this.props.TextStore.notes);

        }else{

            if (DEBUG_MODE){
                this.loadChaptersSelection(null,null);

            }else{
                this.launchChapterSelectionDailog();    
            }
            
        }
        

        //let t=observable(this.props.TextStore.text);
        //observe(t,(change)=>{
            //console.log("TextStore has changed ~~~~~~~~~~~~~~~~~~~~~~~`",change);
        //});


    }

    
    loadChaptersSelection=async(selection,closeModalFn)=>{

        if (DEBUG_MODE){
            selection={fromBook:"Exodus",fromBookHe:"שמות",fromChapter:"10",fromChapterHe:"י"
            ,fromParaNum:"0",toBook:"Exodus",toChapter:10,toParaNum:"28"};
            let closeModalFn=()=>{};
            

        }
        
        
        console.log("loadChaptersSelection with selection",selection);
        

        await this.props.TextStore.loadChaptersSelection(selection);

        if (!DEBUG_MODE){
            closeModalFn();    
        }
        

        this.commentsHandler=new CommentsHandler(
            (commentObj)=>{

            this.elementsHandler.addElement(Consts.ELEMENTS_NOTE_BOX,<CommentBox 
                commentObj={commentObj}
                key={Consts.ELEMENTS_NOTE_BOX} ctx={this} />,Consts.ELEMENTS_GROUP_POPUPS);
        });

        //console.log("NOTES?",this.props.TextStore.notes);
        this.drawNotes(this.props.TextStore.notes);


    }
    
    launchChapterSelectionDailog=()=>{
       
        this.elementsHandler.addElement(Consts.ELEMENTS_CHAPTER_SELECTION_DIALOG,<ChaptersSelectionModal key={0} ctx={this} TextStore={this.props.TextStore} onSubmitSelection={this.loadChaptersSelection} />);

    }

    drawNotes=(notes)=>{

        console.log("Draw notes with notes?",notes);
                
        if (!this.commentsHandler){
            this.commentsHandler=new CommentsHandler();
        }
        notes.forEach((note)=>{
             console.log("commentsHandler.highlightComment with note",note);
             this.commentsHandler.highlightComment(note);
        });
    }


    render(){

        return(
            
            <div>
                <TopNavBar ctx={this} />
                <ControlBar ctx={this}/>

                <div className="container">
                    <div className="body-text">
                      <TextHolder ref={this.textHolderElem} elementsHandler={this.elementsHandler} ctx={this}/>
                    </div>
                </div>

                {this.state.elements.map(e=>e.elem)}

            </div>


        )
    }
}

export default inject('TextStore')(observer(StudyPage));
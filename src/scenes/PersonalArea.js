import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Redirect } from 'react-router';
//import Login from './../modules/auth/Login';
import Auth from './../modules/auth/Auth';
import { observer,inject } from 'mobx-react';
//import { extendObservable } from 'mobx';
import StudyPage from './StudyPage';
import hertzogImg from './../img/hertzog-tanach.gif';
import './PersonalArea.scss';
import lampImg from './../img/capish-homepage-lamp.gif';
import CapishElementsHandler from './../handlers/CapishElementsHandler';
//import {LoginModalMenu} from './../components/modals/LoginModal';
//import {LoginModalForm} from './../components/modals/LoginModal';
import Consts from './../constants/Consts';
import capishLogoImg from './../img/capish-logo-yougotit.gif';
import shareWith from './../img/share-with.png';
import menuIcon from './../img/menu-icon.png';
import ConfirmBoxModal from './../components/modals/ConfirmBoxModal';


class DocumentBox extends Component{
    constructor(props){
        super(props);
        this.ctx=this.props.ctx;
        this.doc=this.props.doc;
        this.TextStore=this.props.TextStore;
        this.state={editTitle:false,title:this.doc.title};
        this.docEditTitle=React.createRef();

        
    }

    hideDocMenu=(e,isInner=false)=>{

        let elem;
        if (isInner) {
            elem=e.target.parentElement;
        }else{
            elem=e.target;
        }
        console.log("mouse out?!");
        if (elem.classList.contains('show')){
            elem.classList.remove('show');
        }

    }

    


    toggleDocMenu=(e,isTarget=false)=>{
        let docMenu=null;
        if (isTarget){
            docMenu=e.nextElementSibling;
        }else{
            docMenu=e.target.nextElementSibling;
        }

        //console.log("e.target?",e.target.nextElementSibling);
        console.log("docMenu?",docMenu);
        
        if (!docMenu.classList.contains('show')){
            docMenu.classList.add('show');
        }else{
            docMenu.classList.remove('show');
        }
    }

    loadDocument=()=>{
         
          window.location='/study-page?document='+this.doc.id;
        
    }

    deleteDocument=async()=>{

        let confirmAction=async()=>{

            let [res,err]=await Auth.superAuthFetch('/api/Documents/'+this.doc.id, 
                { method: 'DELETE', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }} );
            
            this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_CONFIRM_BOX);

            console.log("delete res",res);
            window.location.reload();

        }
        this.ctx.elementsHandler.addElement(Consts.ELEMENTS_CONFIRM_BOX,<ConfirmBoxModal elementsHandler={this.ctx.elementHandler} key={Consts.ELEMENTS_CONFIRM_BOX} title={'האם למחוק את הפריט?'} confirmAction={confirmAction}/>)

    }
    saveDocTitle=async()=>{

        if (this.state.title.trim()==""){
            return;
        }

        let [res,err]=await Auth.superAuthFetch('/api/Documents/'+this.doc.id, 
                { method: 'PATCH', 
                body:JSON.stringify({title:this.state.title}),
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }} );

        this.setState({editTitle:false});

    }

    toggleEditTitle=(e)=>{

        let menuIconEl=e.target.closest('.doc-header').querySelector('.menu-icon');
        console.log("menuIconEl?",menuIconEl);
        
        this.setState({editTitle:!this.state.editTitle},()=>{

            if (this.state.editTitle){
                this.toggleDocMenu( menuIconEl,true );
                this.docEditTitle.current.select();
            }
            

        });



    }

    /*
    cloneDocument=async(e)=>{

        let newDocument={fromParaId:this.doc.fromParaId,toParaId:this.docToParaId,userId:this.doc.userId};
        newDocument.title='עותק של '+this.state.title;

        console.log("new document",newDocument);
        
        let [res,err]=await Auth.superAuthFetch('/api/Documents/',
                { method: 'POST', 
                body:JSON.stringify(newDocument),
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }} );
        
        window.location.reload();           

    }
    */

    changeTitle=(e)=>{
        this.setState({title:e.target.value});
    }
    selectText=(e)=>{
        e.target.select();
    }
    
    docTitle=()=>{
            if (this.state.editTitle){
                return(
                    <div>
                    <input value={this.state.title} className='doc-edit-title' ref={this.docEditTitle} onChange={this.changeTitle} onClick={this.selectText} 
                    onBlur={this.saveDocTitle} />
                    <i className='fas fa-save' onClick={this.saveDocTitle} />
                    </div>
                    );
            }

            return (
                <div className='doc-title'>{this.state.title}</div>
            );
    }

    showSoonMsg=()=>{
        alert("אפשרויות השיתוף יהיו זמינות בגרסה הבאה של קאפיש!")
    }

    render(){

        


        return (<div className='document-box'>
            
                <div className='doc-header'>
                    {this.docTitle()}
                    <img className='menu-icon' src={menuIcon} onClick={this.toggleDocMenu} />                
                    <div className='document-menu' onMouseLeave={this.hideDocMenu}>
                        <ul className='inner' onMouseLeave={(e)=>this.hideDocMenu(e,true)}>
                            <li onClick={this.loadDocument}>טען מסמך</li>
                            <li onClick={this.toggleEditTitle}>שנה שם</li>
                            <li onClick={this.deleteDocument}>מחק</li>
                        </ul>
                    </div>
                </div>
            

                <div className='doc-body'>
                    <div className='share-with' onClick={this.showSoonMsg}><img src={shareWith} alt="שתף הלימוד" title="שתף הלימוד" /> </div>
                    <div className='doc-owner'><span className="user-circle">{this.doc.CustomUser.username[0]}</span>{this.doc.CustomUser.username}</div>
                
                </div>
            </div>
            );
    }
}





class PersonalArea extends Component {

  
    constructor(props) {
        super(props);
        
        this.elementsHandler=new CapishElementsHandler(this);
        this.state={elements:this.elementsHandler.getElements()};


        this.state={elements:this.elementsHandler.getElements(),documents:[]};
        this.chosenDocument=null;

        this.isAuth=Auth.isAuthenticated();

    }

    componentDidMount(){

     (async()=>{
       let [docRes,err]=await Auth.superAuthFetch('/api/Documents/getUserDocuments');  
       if (err){console.error(err)};
       console.log("docRes?",docRes);

       if (Array.isArray(docRes) && docRes.length>0){
         this.setState({documents:docRes});
       }
       
     })();
     
  }

  logout=()=>{
      Auth.logout(()=>{
          window.location='/';
      });
  }

  
/*
  chooseDocument=(e,doc)=>{
    
    
    let docElems = document.querySelectorAll('.document-box');
    [].forEach.call(docElems,(docElem)=> {
      docElem.classList.remove('chosen');
    });


    let chosen=e.target.classList.toggle('chosen');
    console.log("chosen?",chosen);
    this.chosenDocument=chosen ? doc : null;
  }
*/




    

    render() {
        return(
            <div className='personal-area-view'>
                {this.state.elements.map(e=>e.elem)}
                

                <section className="container body">
                    <div className="row">
                        <div className="col-sm-3 right-container no-float side-menu">
                            
                            <div className='brand-img'><a href="/"><img src={capishLogoImg} /></a></div>
                            
                            <ul className='side-menu-items' aria-label='קבוצת לימוד'>
                                <li>הכיתה של דפנה</li>
                                <li>הכיתה של רז</li>    
                            </ul>
                            
                            <hr />
                            <ul className='side-menu-items' aria-label='שיתפו איתי'>
                                <li>היום</li>
                                <li>לאחרונה</li>
                            </ul>
                            <hr />
                            <ul className='side-menu-items' aria-label='לימוד עצמאי'>
                                <li>לימוד עצמאי מתוך המאגר</li>
                            </ul>

                            <div className='tac mt50'><button onClick={this.logout} className='capish-button ib'>התנתק</button></div>
                        </div>
                        <div className="col-sm-9 left-container no-float">

                        <div className='user-documents'>
                        {this.state.documents.map((doc,k)=>
                          <DocumentBox key={k} doc={doc} TextStore={this.props.TextStore} ctx={this}/>
                          
                          )
                        }
                          </div>
                            
                        </div>
                    </div>
                </section>

                {this.state.elements.map(e=>e.elem)}
            </div>
        );
    }
}
export default inject('TextStore')(observer(PersonalArea));
import React, { Component } from 'react';
import './TextHolder.scss';
import { observer,inject } from 'mobx-react';
import explIcon from './../img/explication-icon.png';
import explIconHover from './../img/explication-icon-hover.png';
import qIcon from './../img/question-icon.png';
import qIconHover from './../img/question-icon-hover.png';
import titleIcon from './../img/title-icon.png';
import titleIconHover from './../img/title-icon-hover.png';
import commentIcon from './../img/comment-icon.png';
import commentIconHover from './../img/comment-icon-hover.png';
import AsyncTools from './../modules/tools/AsyncTools';
import Auth from './../modules/auth/Auth';
import ElementsHandler from './../handlers/ElementsHandler';
import Consts from './../constants/Consts';
import CommentBox from './CommentBox';
import {observable, autorun, action} from "mobx";
import {NewTitleBox} from './TitleBox';
import {NewCommentBox} from './CommentBox';
import {NewQuestionBox} from './QuestionBox';
import QuestionBox from './QuestionBox';
import {TitleBox} from './TitleBox';
import CommentaryBox from './CommentaryBox';
import LocationBox from './LocationBox';
import PersonBox from './PersonBox';
import NoteBoxModal from './modals/NoteBoxModal';
import {DefineUnknownsModal} from './modals/DefineUnknownsModal';
import Paragraph from './Paragraph';
import _ from 'lodash';



class CMenu extends Component{

    constructor(props){
        super(props);
        this.ctx=this.props.ctx;
        this.state={menuIcons:{qIcon:qIcon,commentIcon:commentIcon,explIcon:explIcon,titleIcon:titleIcon}};
    }

    setMenuImage=(imageKey,image)=>{
        
        let menuIcons=this.state.menuIcons;
        menuIcons[imageKey]=image;
        this.setState({menuIcons});
    }
    

    render(){

        return(<div >
                <div className="row">
                    <div className="col-xs-3 icon-item" onClick={this.ctx.addQuestion} 
                    onMouseOver={()=>this.setMenuImage('qIcon',qIconHover)} onMouseLeave={()=>this.setMenuImage('qIcon',qIcon)}>
                        <img className='menu-icon' src={this.state.menuIcons.qIcon} alt="הוספת שאלה"/>
                        <div className="ii-title">הוספת שאלה</div>
                    </div>
                    
                    <div className="col-xs-3 icon-item" onClick={this.ctx.addComment} 
                    onMouseOver={()=>this.setMenuImage('commentIcon',commentIconHover)} onMouseLeave={()=>this.setMenuImage('commentIcon',commentIcon)}
                    >
                        <img className='menu-icon' src={this.state.menuIcons.commentIcon} alt="הוספת הערה" />
                        <div className="ii-title">הוספת הערה</div>
                    </div>
                    <div className="col-xs-3 icon-item" onClick={this.ctx.addTitle}
                    onMouseOver={()=>this.setMenuImage('titleIcon',titleIconHover)} onMouseLeave={()=>this.setMenuImage('titleIcon',titleIcon)}
                    >
                        <img className='menu-icon' src={this.state.menuIcons.titleIcon} alt="הוספת כותרת" />
                        <div className="ii-title">הוספת כותרת</div>
                    </div>
                    <div className="col-xs-3 icon-item" onClick={this.ctx.showCommentary}
                    onMouseOver={()=>this.setMenuImage('explIcon',explIconHover)} onMouseLeave={()=>this.setMenuImage('explIcon',explIcon)}
                    >
                        <img className='menu-icon' src={this.state.menuIcons.explIcon} alt="פרשנות" />
                        <div className="ii-title">פרשנות</div>
                    </div>
                </div>

        </div>);

    }
}



class TextHolder extends Component{
    
    constructor(props){
        super(props);
        this.isContextMenuEnabled=true;
        this.elementsHandler=this.props.elementsHandler;
        this.ctx=this.props.ctx;
        this.cMenu=null;

        this.sObj={};

        this.state={
            sObj:{sText:""},show:{commentBox:false}
        }

        this.mX=0;
        this.mY=0;
    
    }

   
    showComment=(commentObj)=>{

        
        this.elementsHandler.addElement(Consts.ELEMENTS_NOTE_BOX,<CommentBox 
                commentObj={commentObj}
                key={Consts.ELEMENTS_NOTE_BOX} ctx={this} />,Consts.ELEMENTS_GROUP_POPUPS);

    }

    getSelectionObj() {
        
        let selObj={sText:"",parentEl:null};

        if (window.getSelection) {
            let sel=window.getSelection();

            selObj.sText=sel.toString();


            if (sel.rangeCount) {
                //console.log("SELECTION RANGE",sel.getRangeAt(0));
                selObj.range=sel.getRangeAt(0);
                selObj.parentEl = sel.getRangeAt(0).commonAncestorContainer;
                if (selObj.parentEl.nodeType != 1) {
                    selObj.parentEl = selObj.parentEl.parentNode;
                }
            }

        } else if (document.selection && document.selection.type != "Control") {
            selObj.sText = document.selection.createRange().text;
            selObj.parentEl=document.selection.createRange().parentElement();

        }

        console.log("selObj?",selObj);
        
        return selObj;
    }

    getMousePos=(e)=> {
        e = e || window.event;

        var pageX = e.pageX;
        var pageY = e.pageY;

        // IE 8
        if (pageX === undefined) {
            pageX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            pageY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }

        return {x:pageX,y:pageY+15};
    }


    replaceNewNote=(entry,val)=>{this.props.TextStore.replaceNewNote(entry,val);}

    updateText=(entry,val)=>{this.props.TextStore.updateText(entry,val);}

    addQuestion=()=>{
        

        //this.setState({cMenu:{display:'none'}});
        this.hideCMenu();

        this.isContextMenuEnabled=false;
        
        if (!this.props.TextStore.isCustomized){
            //alert('כדי להוסיף כותרת, עליך לשייך אליך קודם את המסמך');
            this.elementsHandler.addElement(Consts.ELEMENTS_NOTE_BOX,<NoteBoxModal elementsHandler={this.elementsHandler} key={Consts.ELEMENTS_NOTE_BOX} title={'על מנת להוסיף שאלה, עליך לשייך את המסמך אליך קודם לכן'} />)
            return false;
        }
        
        let noteData={question:"",firstAnswer:"",secondAnswer:"",noteType:"question",};
        let aAnswers=["thirdAnswer","fourthAnswer","fifthAnswer","sixthAnswer"];
        
        aAnswers.forEach(aa=>{ noteData[aa]="";})
        
        let sObj=this.sObj;

        //let sText=this.props.sObj.sText;

        try{
            
            noteData.last_para_id=this.props.TextSelectorParser.lastParaId;
            //noteData.last_para_id=parseInt(sObj.range.endContainer.parentElement.attributes['data-para-index'].value);

            console.log("noteData",noteData);

            let nd={
                last_para_id:noteData.last_para_id,
                //id:noteData.id,
                question:noteData.question,
                firstAnswer:noteData.firstAnswer,
                secondAnswer:noteData.secondAnswer,
                isNew:true

            };

            
            this.props.TextStore.updateText('questions',nd);        

        }catch(err){
            console.error(err);
            //this.props.oncloseCommentBox();
            return;
        }



    }

    hideCMenu=()=>{
        
        this.cMenu.style.display='none';
        this.elementsHandler.removeElement(Consts.ELEMENTS_COMMENTARY_BOX);
        this.elementsHandler.removeElement(Consts.ELEMENTS_NOTE_BOX);

    }
    addTitle=()=>{

        //this.setState({cMenu:{display:'none'}});
        this.hideCMenu();

        let u=Auth.getUserDetails();
        if (!this.props.TextStore.isCustomized && u.role!='m9fScC'){
            this.elementsHandler.addElement(Consts.ELEMENTS_NOTE_BOX,<NoteBoxModal elementsHandler={this.elementsHandler} key={Consts.ELEMENTS_NOTE_BOX} title={'על מנת להוסיף כותרת, עליך לשייך את המסמך אליך קודם לכן'} />)
            return false;
        }
        
        let titleData={};
        let sObj=this.sObj;
        let sText=sObj.sText;

        try{
            
            titleData.from_para_id=parseInt(sObj.range.endContainer.parentElement.attributes['data-para-index'].value);
            

        }catch(err){
            console.error(err);
            return;
        }

        titleData={...titleData,title:this.state.title,noteType:"title",isNew:true};

        console.log("titleData",titleData);

        this.props.TextStore.updateText('titles',titleData);        
        
         
    }
    addComment=()=>{
        
        //this.setState({cMenu:{display:'none'}});
        this.hideCMenu();

        let u=Auth.getUserDetails();
        if (!this.props.TextStore.isCustomized && u.role!='m9fScC'){
            this.elementsHandler.addElement(Consts.ELEMENTS_NOTE_BOX,
                <NoteBoxModal elementsHandler={this.elementsHandler} key={Consts.ELEMENTS_NOTE_BOX} 
                title={'על מנת להוסיף הערה אישית, עליך לשייך את המסמך אליך קודם לכן'} />)
            return false;
        }
        this.elementsHandler.addElement(Consts.ELEMENTS_NEW_COMMENT_BOX,
            <NewCommentBox textContext={this} sObj={this.sObj} 
            oncloseCommentBox={()=>this.elementsHandler.removeElement(Consts.ELEMENTS_NEW_COMMENT_BOX)}/>,
            Consts.ELEMENTS_GROUP_POPUPS);

    }

    showCommentary=()=>{

        //this.setState({cMenu:{display:'none'}});
        
        let comBox=this.elementsHandler.getElementByKey(Consts.ELEMENTS_COMMENTARY_BOX);
        //console.log("comBox?",comBox);
        console.log("showCommentary, this.sObj?",this.sObj);
        var timestamp = new Date().getUTCMilliseconds();
        if (comBox!==null){
            console.log("comBox is not null");
            this.elementsHandler.replaceElementByKey(Consts.ELEMENTS_COMMENTARY_BOX,
            <CommentaryBox ts={timestamp} key={Consts.ELEMENTS_COMMENTARY_BOX} ctx={this} sObj={this.sObj} elementKey={Consts.ELEMENTS_COMMENTARY_BOX} />);
        }else{

            this.hideCMenu();
            this.elementsHandler.addElement(Consts.ELEMENTS_COMMENTARY_BOX,<CommentaryBox ts={timestamp} key={Consts.ELEMENTS_COMMENTARY_BOX} ctx={this} sObj={this.sObj} elementKey={Consts.ELEMENTS_COMMENTARY_BOX} />,Consts.ELEMENTS_GROUP_POPUPS);    
        }

        

    }

    componentDidMount(){

        
        this.cMenu=document.querySelector("#cmenu");
        document.querySelector(".text-box").addEventListener('mouseup', (e)=> { 

            let sObj=this.getSelectionObj();
            let sText=sObj.sText;
            let mPos=this.getMousePos(e);

            console.log("local sObj",sObj);

            if (!sObj.parentEl || !sObj.parentEl.classList || 
                (!sObj.parentEl.classList.contains('text-para') && !sObj.parentEl.classList.contains('text-box')
               )){
                console.log("Not inside a text-para class, exit");
                if (this.cMenu.style.display=='block'){
                    
                    //this.sObj={};
                    this.erasePreviousSelection();
                    this.cMenu.style.display='none';
                    
                }
                return;
            }

            if (!this.isContextMenuEnabled || sText=="" || sText==null || sText.length<3){
                if (this.cMenu.style.display=='block'){

                    this.sObj={};
                    this.props.TextSelectorParser.sObj={};
                    this.erasePreviousSelection();
                    this.cMenu.style.display='none';
                    
                }    
                console.log("this.isContextMenuDisabled?",this.isContextMenuEnabled);                
                return;
            }
                
            //window.rangy.getSelection().expand("word", {trim: true,includeTrailingSpace: false});
            this.erasePreviousSelection();
            this.cMenu.style.display='block';
            this.cMenu.style.left=mPos.x-80+"px";
            this.cMenu.style.top=mPos.y+"px";
            
            console.log("Setting TextSelectorParser.sObj with ",this.sObj);
            this.sObj={...sObj};
            this.props.TextSelectorParser.sObj={...this.sObj};
            //setTimeout(this.drawSelection,100);
            
            });
        }
        
        erasePreviousSelection=()=>{
            
            //Still buggy!! 
            //console.log("erase previous selection is launched");
            window.$('.selected-text').contents().unwrap();

        }

        drawSelection=()=>{

        console.log("Hightlight comment with noteData",noteData);

        let noteData={};
        let sObj={...this.sObj};
                    
        noteData.from_para_id=this.props.TextSelectorParser.fromParaId;
        noteData.to_para_id=this.props.TextSelectorParser.lastParaId;

        noteData.from_char=sObj.range.startOffset;
        noteData.to_char=sObj.range.endOffset;

        let toParaEl=document.querySelector("[data-para-index='"+noteData.to_para_id+"']")//.innerHTML;        
        let fromParaEl=document.querySelector("[data-para-index='"+noteData.from_para_id+"']");

        //console.log("highlightComment toParaEl",toParaEl);
        //console.log("highlightComment fromParaEl",fromParaEl);

        if (noteData.to_para_id==noteData.from_para_id){

            console.log("highlight single para");

            fromParaEl.innerHTML=fromParaEl.innerHTML.substr(0,noteData.from_char)
            +"<span class='selected-text'>"+fromParaEl.innerHTML.substr(noteData.from_char,noteData.to_char-noteData.from_char)
            +"</span>"+fromParaEl.innerHTML.substr(noteData.to_char);


        }else{

            //console.log("highlight multiple paras");

            let paraNum=noteData.from_para_id;
            let paraEl=null;
            
            while (paraNum<=noteData.to_para_id){

                if (paraNum==noteData.from_para_id){

                    fromParaEl.innerHTML=fromParaEl.innerHTML.substr(0,noteData.from_char)+"<span class='selected-text'>"+fromParaEl.innerHTML.substr(noteData.from_char)+"</span>";
                    paraEl=fromParaEl;

                }else if(paraNum==noteData.to_para_id){

                    toParaEl.innerHTML="<span class='selected-text'>"+toParaEl.innerHTML.substr(0,noteData.to_char)+"</span>"+toParaEl.innerHTML.substr(noteData.to_char);
                    paraEl=toParaEl;

                }else{

                    paraEl=document.querySelector("[data-para-index='"+paraNum+"']");
                    paraEl.innerHTML="<span class='selected-text'>"+paraEl.innerHTML+"</span>";

                }      
                paraNum++;

            }
        }
    }

    showParas=()=>{
        
        let text=this.props.TextStore.text;
        //console.log("showParas text?",text);
        if (_.isEmpty(text)){return;}
        //console.log("showParas text2?");
        let els=[],elem=null;
        //console.log("TEXT",text);
        
        const paras=text.paras;
        const questions=_.has(text,"questions") ? text.questions : [];
        const titles= text.titles || [];
        const genericTitles = text.genericTitles || [];
        const locations=_.has(text,"locations") ? text.locations : [];

        let ii=0;
        let uniqueKey=2342342234;
        let bookName=null,chapterName=null;

        if (paras && paras.length>0){
            
            
            let prevChapter=null;
            let qq=[];
            
            for (let k=0;k<paras.length;k++){
            //paras.forEach((para,k)=>{
                let para=paras[k];
                let paraLocations=locations.filter(l=>l.last_para_id==para.id);
                let paraUnknowns=text.unknowns.filter(u=>u.last_para_id==para.id);
                let paraPersons=text.persons.filter(u=>u.last_para_id==para.id);
                                
                if (prevChapter!==para.chapter){
                    
                    bookName=this.ctx.bibleIndex.getBookNameByLang(para.book,'he');
                    chapterName=this.ctx.bibleIndex.getHeNumByIndex(para.chapter-1);

                    els.push(<div key={uniqueKey} className="chapter-title">{bookName}&nbsp;{chapterName}</div>);
                    uniqueKey++;
                }

                prevChapter=para.chapter;
                
                let notes={paraLocations,paraUnknowns,paraPersons};


                if (titles.length>0){

                    qq=titles.filter(q=>q.from_para_id===para.id);
                    if (qq.length>0){
                        //console.log("showing titles for title (qObj)?",qq[0]);
                        elem=<TitleBox key={ii} 
                        aboveParaId={para.id}
                        sObj={this.sObj} 
                        qObj={qq[0]} ctx={this.ctx} TextStore={this.props.TextStore} textHolder={this} />
                        els.push( elem );
                        ii++;
                    }
                }

                if (genericTitles.length>0 && this.props.TextStore.showGenericTitles){
                    qq=genericTitles.filter(q=>q.from_para_id===para.id);
                    if (qq.length>0){
                        elem=<TitleBox key={ii} sObj={this.sObj} qObj={qq[0]} ctx={this.ctx} TextStore={this.props.TextStore} textHolder={this} />
                        els.push(elem);
                        ii++;
                    }
                }
                

                elem=<Paragraph ctx={this} index={para.id} paraNum={para.paraNum} key={ii} para={para.text} notes={notes} />;
                els.push( elem );
                ii++;

                
                if (questions.length>0){
                    let q=questions.filter(q=>q.last_para_id===para.id);
                    if (q.length>0){
                        //console.log("q[0] for QuestionBox??",q[0]);
                        elem=<QuestionBox 
                            TextStore={this.props.TextStore}
                            textHolder={this}
                            key={ii} 
                            sObj={this.sObj} 
                            qObj={q[0]} 
                            ctx={this.ctx} />
                        els.push( elem );
                        ii++;
                    }
                }
            }  
            //});

        }else{
            console.log("this.props.lines is zero");
        }

        return els;
    }

    

    

    render(){

        return(
            <div>
                
                <div id="cmenu" style={{display:'none',backgroundColor:'#fff',position:'absolute','zIndex':9}}>
                    <CMenu ctx={this} />                
                </div>

                <div className="row">
                    <div className="col-sm-3"></div>
                    <div className="col-sm-9">
                        <div className="text-box">
                            {this.showParas()}
                        </div>

                    </div>
                </div>
            </div>

            )
    }
}
export default inject('TextStore','TextSelectorParser')(observer(TextHolder));
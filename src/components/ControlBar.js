import React, { Component } from 'react';
import './../scenes/StudyPage.scss';
import Auth from './../modules/auth/Auth';
import { observer,inject } from 'mobx-react';
import ToggleButton from 'react-toggle-button';
import LoadDocumentModal from './modals/LoadDocumentModal';
import ElementsHandler from './../handlers/ElementsHandler';
import Consts from './../constants/Consts';
import keyImg from './../img/key.png';
import iglassImg from './../img/inspect-glass.png';
import commentsImg from './../img/topmenubar/comments.gif';
import commentsHoverImg from './../img/topmenubar/comments-hover.png';
import heartImg from './../img/heart-env.png';
import prevImg from './../img/prev-page.png';
import apersonImg from './../img/add-person.png';
import personsImg from './../img/topmenubar/persons.png';
import personsHoverImg from './../img/topmenubar/persons-hover.png';
import './ControlBar.scss';
//import pinImg from './../img/pin.png';
import pinImg from './../img/topmenubar/pin.gif';
import pinHoverImg from './../img/topmenubar/pin-hover.gif';
//import ToggleButton from 'react-toggle-button';

class ControlBar extends Component{
    
    constructor(props){
      super(props);
      
      
      this.elementsHandler=this.props.ctx.elementsHandler;
      this.state={wordsList:[],editTitle:false,form:{titleName:""},wordsStyle:{height:'50px'},liStyle:{backgroundColor:"#000000"},showTitleStyle:{display:'none'}
       };
      this.toggleBtns=['showLocations','showPersons','showComments'];

    }



    changeDocumentName=(e)=>{
      
      this.props.TextStore.customDocName=e.target.value;
    }


    saveDocument=async()=>{
      this.setState({editTitle:false});

      
     const text=this.props.TextStore.text;
     
     let document={
       fromParaId:text.paras[0].id,
       toParaId:text[text.length-1].paras[   text[text.length-1].paras.length-1   ].id,
       title:this.props.TextStore.documentName
     };

     console.log("document to save",document);
     //let [err,res] = await AsyncTools.superFetch('/api/);
     
     let [docRes,err]=await Auth.superAuthFetch('/api/Documents/saveDocument',
       {   headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
       method:'POST',
       body:JSON.stringify( {docData:document})   });
     

    }
    closeLoadDocumentModal=()=>{
      this.elementsHandler.removeElement(Consts.ELEMENTS_LOAD_DOCUMENT_MODAL);
    }

    openLoadDocumentModal=async()=>{

      this.elementsHandler.addElement(Consts.ELEMENTS_LOAD_DOCUMENT_MODAL,<LoadDocumentModal key={Consts.ELEMENTS_LOAD_DOCUMENT_MODAL} onLoadDocument={this.loadDocument} ctx={this.props.ctx} closeModal={this.closeLoadDocumentModal}/>);


    }

    toggleGenericTitles=(e)=>{
      let el=e.target;
      if (!el.classList.contains('selected')){el.classList.add('selected');}else{el.classList.remove('selected');}
      this.props.TextStore.showGenericTitles=!this.props.TextStore.showGenericTitles;
    }

    

    toggleUnknowns=(e)=>{
      console.log("toggle unknowns is launched");
      let el=e.target;
      if (!el.classList.contains('selected')){el.classList.add('selected');}else{el.classList.remove('selected');}
      window.$(".text-line .unknown").toggleClass('show-unknown');
      this.props.TextStore.showUnknowns=!this.props.TextStore.showUnknowns;
      
    }

    toggleLocations=()=>{
      
      this.turnOffOthers('showLocations');  
      window.$(".text-line .location").toggleClass('show-location');
      this.props.TextStore.showLocations=!this.props.TextStore.showLocations;
      this.renderPhrasesBox('locations','מקומות','title',{backgroundColor:'#41b2ba',color:"#fff"},'location',this.props.TextStore.showLocations);
      
    }

    togglePersons=()=>{
      console.log("toggle persons is launched");
      
      this.turnOffOthers('showPersons');  
      window.$(".text-line .person").toggleClass('show-person');
      this.props.TextStore.showPersons=!this.props.TextStore.showPersons;
      this.renderPhrasesBox('persons','דמויות','term',{backgroundColor:'#f3af1c',color:'#fff'},'person',this.props.TextStore.showPersons);
      
    }

    toggleComments=()=>{
      
      console.log("toggle comments is launched");
      
      this.turnOffOthers('showComments');
      this.renderPhrasesBox(); 
      window.$(".text-line .highlight").toggleClass('show-comment');
      this.props.TextStore.showComments=!this.props.TextStore.showComments;

    }

    turnOffAll=()=>{
      
      window.$(".text-line .person").removeClass('show-person');
      this.props.TextStore.showPersons=false;

      window.$(".text-line .highlight").removeClass('show-comment');
      this.props.TextStore.showComments=false;
      
      window.$(".text-line .location").removeClass('show-location');
      this.props.TextStore.showLocations=false;

      this.renderPhrasesBox();
      
    }

    renderPhrasesBox=(noteType=null,titleHe=null,titleKey=null,liStyle=null,noteSingleType=null,show=true)=>{
      
      
      let words=[],indexes=[];
      
      //if (this.state.wordsList.length==0 && noteType!==null){
      if (noteType!==null && show){
        this.props.TextStore.distinctPhrasesObj={noteType,titleKey};
        ({words,indexes} = this.props.TextStore.distinctPhrases);  
        //console.log("DISTINCTIVE WORDS",words);
        //console.log("DISTINCTIVE INDEXES",indexes);
      }
      
      //let wordsLi=words.map(w=><li>{w}</li>);
      
      let addOffset=0;
      if (words.length>0 && words.length<4) addOffset=33;
      if (words.length>3) addOffset=13;
      
      //console.log("Math.ceil(words.length/3)",Math.ceil(words.length/3));

      let wordsStyle={height: parseInt(addOffset+50+Math.ceil(50*Math.ceil(words.length/3)))+"px"};
      let showTitleStyle={display:'block'};
      
      if (words.length==0){
        showTitleStyle={display:'none'};
        
      }

      console.log("noteType?",noteType);
      console.log("WORDS.LENGTH?",words.length);
     
      //console.log("wordsStyle",wordsStyle);
      this.setState({wordsIndexes:indexes,wordsList:words,wordsStyle,
        showTitle:titleHe,showTitleStyle,liStyle:liStyle,noteSingleType},()=>{
          
          let closeElem=document.querySelector(".close-words-container");
          if (words.length==0){
            closeElem.classList.remove("show");
          }else{
            closeElem.classList.add("show");
          }
        });

    }

    closeWordsContainer=()=>{
      this.turnOffAll();
    }

    turnOffOthers=(butSelf)=>{
      if (this.props.TextStore[butSelf]==true){
        return;
      }
      

      this.toggleBtns.forEach(tBtn=>{
        if (this.props.TextStore[tBtn]==true && tBtn!=butSelf){
          switch(tBtn){
            case 'showComments':
            window.$(".text-line .highlight").removeClass('show-comment');
            this.props.TextStore.showComments=false;
            break;

            case 'showLocations':
            window.$(".text-line .location").removeClass('show-location');
            this.props.TextStore.showLocations=false;

            break;
            case 'showPersons':
            window.$(".text-line .person").removeClass('show-person');
            this.props.TextStore.showPersons=false;
            break;
          }
        }
      });

    }

    

    render(){

        //let titleEditor=null;
        const documentName=this.props.TextStore.documentName;
        let showLocations=this.props.TextStore.showLocations;
        let showUnknowns=this.props.TextStore.showUnkowns;
        let showPersons=this.props.TextStore.showPersons;
        let showComments=this.props.TextStore.showComments;

        const u=Auth.getUserDetails();

        //console.log("showLocations?",showLocations);
        /*
        if (this.state.editTitle && this.state.isCustomized){
          titleEditor=<li><i className="far fa-save" onClick={this.saveDocument}/><input type="text" value={documentName} onChange={this.changeDocumentName} name="titleName" /></li>;
        }else{
          titleEditor=<li onClick={()=>{
            
            if (!this.state.isCustomized) return;
            this.setState({editTitle:!this.state.editTitle})}

          }>{documentName}
          </li>;
        }
        */
        return(

        <div className="control-bar">
        <div className="control-bar-stripe"></div>
        <div className="container">
          <ul className="left-container">
            {/*u && u.role && u.role=='m9fScCA' && <li onClick={this.toggleUnknowns}>מונחים</li>*/}
            {<li onClick={this.toggleUnknowns}>מונחים</li>}
          </ul>

          <div className='icons-container' style={this.state.wordsStyle}>
            <ul>
              <li><img className='keywords-icon' src={keyImg} alt="מילות מפתח" title="מילות מפתח" /></li>
              <li onClick={this.toggleComments}><img src={showComments ? commentsHoverImg : commentsImg} alt="הערות" title="הערות" /></li>
              <li onClick={this.togglePersons}><img src={showPersons ? personsHoverImg : personsImg} alt="דמויות" title="דמויות" /></li>
              <li><img onClick={this.toggleLocations} className="pin" src={showLocations ? pinHoverImg : pinImg} alt="מקומות" title="מקומות" /></li>              
            </ul>

            <div className='show-title' style={this.state.showTitleStyle}>{this.state.showTitle}</div>
            
            <div className='words-container' >
              <ul>
               {this.state.wordsList.map((w,k)=>
                 <li className='colored-word' style={this.state.liStyle} key={k}
                onClick=
                {()=>{
                  const selector="[data-"+this.state.noteSingleType+"-id='"+this.state.wordsIndexes[k]+"']";
                  if (document.querySelector(selector)!==null){document.querySelector(selector).click();  }
                }}
                 >{w}</li>)
             }
              </ul>
              <div className='close-words-container' onClick={this.closeWordsContainer}><i className="fas fa-times"/></div>
            </div>
          </div>
          
          <ul className="right-container">
            <li className='toggle-generic-titles-container'>נושאים
              <ToggleButton value={this.props.TextStore.showGenericTitles} onClick={this.toggleGenericTitles}               colors={{inactiveThumb: {base: 'rgb(250,250,250)',},activeThumb: {base: 'rgb(48,222,235)',},active: {base: 'rgb(207,221,245)',hover: 'rgb(177, 191, 215)',},inactive: {base: 'rgb(65,66,68)',hover: 'rgb(95,96,98)',}}}/>
            </li>
          </ul>
        </div>
        </div>
            );
    }
}
export default inject('TextStore')(observer(ControlBar));
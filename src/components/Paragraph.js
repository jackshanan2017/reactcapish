import React, { Component } from 'react';
import './TextHolder.scss';
import { observer,inject } from 'mobx-react';
import AsyncTools from './../modules/tools/AsyncTools';
import Auth from './../modules/auth/Auth';
import Consts from './../constants/Consts';
import CommentBox from './CommentBox';
import {observable, autorun, action} from "mobx";
import LocationBox from './LocationBox';
import PersonBox from './PersonBox';
import NoteBoxModal from './modals/NoteBoxModal';
import {DefineUnknownsModal} from './modals/DefineUnknownsModal';
import _ from 'lodash';

function filterHebrew(st){

  //st=filterTaamim(st);
  let fSt="";
  st=st.replace(/,\-;/," ");
  for (let i=0;i<st.length;i++){
    let c=st[i];
    if (c.match(/[\sאבגדהוזחטיכלמנסעפצקרשתץףםן־]/)){fSt+=""+c;}
  }
  return fSt;
}

function filterTaamim(st){
  return st;

  let fSt="";
  st=st.replace(/,\-;/," ");
  for (let i=0;i<st.length;i++){
    let c=st[i];
    if (c.match(/[\u05B0\u05B1\u05B2\u05B3\u05B4\u05B5\u05B6\u05B7\u05B8\u05C2\u05C1\u05B9\u05BC\u05BB\sאבגדהוזחטיכלמנסעפצקרשתץףםן־]/)){fSt+=""+c;}
    
  }
  return fSt;
}

function cleanText(st){

  st=st.replace("(פ)","");
  //st=filterTaamim(st);
  return st;
}

class Paragraph extends Component{

    constructor(props){
         super(props);


         //console.log("Paragraph is constructed with paraNum",this.props.index);
         this.initialize();
    }

    initialize=()=>{

        let para=filterTaamim(this.props.para);

        this.locations=this.props.notes && this.props.notes.paraLocations ? this.props.notes.paraLocations : [];
        this.unknowns= this.props.notes && this.props.notes.paraUnknowns ? this.props.notes.paraUnknowns : [];
        this.persons= this.props.notes && this.props.notes.paraPersons ? this.props.notes.paraPersons : [];

        this.markWords=[];
        this.markUnknowns=[];
        this.markPersons=[];

        this.ctx=this.props.ctx;
         
        if (this.locations.length>0) this.findLocations(para);
        if (this.unknowns.length>0)  this.findUnknowns(para);
        if (this.persons.length>0)   this.findPersons(para);
         

    }

    componentDidUpdate(){

        //console.log("componetDidUpdate is launched with index",this.props.index);
        this.initialize();
        this.renderNotes();

    }

    findLocations=(para)=>{
        this.locations.forEach(location=>{
            let other=filterHebrew(location.term);
                para.split(" ").forEach((word,wi)=>{
                    if (filterHebrew(word).indexOf(other)!==-1 || filterHebrew(word)==other){this.markWords.push({word,location,other});}
                });
        });


    }

    findUnknowns=(para)=>{
        this.unknowns.forEach(unknown=>{
            let term=filterHebrew(unknown.term);
            para.split(" ").forEach((word,wi)=>{if (filterHebrew(word).indexOf(term)!==-1 || filterHebrew(word)==term){this.markUnknowns.push({word,unknown,term});}});
        });
    }

    findPersons=(para)=>{
        this.persons.forEach(person=>{
            let term=filterHebrew(person.term);
            para.split(" ").forEach((word,wi)=>{if (filterHebrew(word).indexOf(term)!==-1 || filterHebrew(word)==term){this.markPersons.push({word,person,term});}});
        });
    }

    renderNotes=()=>{
        try{
            if (this.markPersons.length>0){
                for (let i=0;i<this.markPersons.length;i++){
                    let mPerson=this.markPersons[i];
                    let paraEl=document.querySelector("[data-para-index='"+this.props.index+"']");
                    
                    let classes='person';
                    if (this.props.TextStore.showLocations==true) classes+=' show-person';

                    paraEl.innerHTML=paraEl.innerHTML.replace(mPerson.word,"<span class='"+classes+"' data-person-id='"+mPerson.person.id+"'>"+mPerson.word+"</span>");
                    setTimeout(()=>{
                        paraEl.querySelector("[data-person-id='"+mPerson.person.id+"']").addEventListener('click',(e)=>{
                            if (!window.$(e.target).hasClass('show-person')){
                                console.log("persons are disabled");
                                return;
                            }
                            console.log("person (%d) is clicked",mPerson.person.id);
                            this.ctx.elementsHandler.addElement(Consts.ELEMENTS_PERSONS_BOX,
                                <PersonBox key={Consts.ELEMENTS_PERSONS_BOX} textContext={this} 
                                para={paraEl} 
                                elementKey={Consts.ELEMENTS_PERSONS_BOX} 
                                locationObj={mPerson.person} 
                                ctx={this.ctx} />);
                        });
                    },100);
                }
            }

            if (this.markWords.length>0){
                for (let i=0;i<this.markWords.length;i++){
                    let mWord=this.markWords[i];
                    let paraEl=document.querySelector("[data-para-index='"+this.props.index+"']");
                    
                    let classes='location';
                    if (this.props.TextStore.showLocations==true) classes+=' show-location';
                    
                    paraEl.innerHTML=paraEl.innerHTML.replace(mWord.word,"<span class='"+classes+"' data-location-id='"+mWord.location.id+"'>"+mWord.word+"</span>");
                    setTimeout(()=>{
                        paraEl.querySelector("[data-location-id='"+mWord.location.id+"']").addEventListener('click',(e)=>{
                            if (!window.$(e.target).hasClass('show-location')){
                                console.log("locations are disabled");
                                return;
                            }
                            console.log("location (%d) is clicked",mWord.location.id);
                            this.ctx.elementsHandler.addElement(
                                Consts.ELEMENTS_LOCATION_BOX,
                                <LocationBox key={Consts.ELEMENTS_LOCATION_BOX} textContext={this} 
                                para={paraEl} elementKey={Consts.ELEMENTS_LOCATION_BOX} 
                                locationObj={mWord.location} word={mWord.other} ctx={this.ctx} />);
                        });
                    },100);
                }
            }

            if (this.markUnknowns.length>0){
                //console.log("markUnknowns.length is bigger than zero");
                for (let i=0;i<this.markUnknowns.length;i++){
                    let mWord=this.markUnknowns[i];
                    let paraEl=document.querySelector("[data-para-index='"+this.props.index+"']");
                    
                    let classes='unknown';
                    if (this.props.TextStore.showUnknowns==true) classes+=' show-unknown';
                    
                    //console.log("replacing with innerHTML","<span class='"+classes+"' data-unknown-id='"+mWord.unknown.id+"'>"+mWord.word+"</span>");
                    paraEl.innerHTML=paraEl.innerHTML.replace(mWord.word,"<span class='"+classes+"' data-unknown-id='"+mWord.unknown.id+"'>"+mWord.word+"</span>");
                    
                    setTimeout(()=>{
                        
                        paraEl.querySelector("[data-unknown-id='"+mWord.unknown.id+"']").addEventListener('click',(e)=>{
                        
                            if (!window.$(e.target).hasClass('show-unknown')){
                                console.log("unknowns are disabled");
                                return;
                            }
                            console.log("location (%d) is clicked",mWord.unknown.id);
                            this.ctx.elementsHandler.addElement(Consts.ELEMENTS_DEFINE_UNKOWNS_MODAL,
                                <DefineUnknownsModal 
                                key={Consts.ELEMENTS_DEFINE_UNKOWNS_MODAL} 
                                para={paraEl} 
                                elementKey={Consts.ELEMENTS_DEFINE_UNKOWNS_MODAL} 
                                termObj={mWord.unknown} 
                                term={mWord.term} 
                                ctx={this.ctx} />,Consts.ELEMENTS_GROUP_POPUPS);
                        });
                        

                    },100);
                }
            }
        }catch(err){
           console.log("renderNotes err",err);
        }
    }
    componentDidMount(){
         this.renderNotes();
    }
    render(){

        const heNums=['א','ב','ג','ד','ה','ו','ז','ח','ט','י','יא','יב','יג','יד','טו','טז','יז','יח','יט','כ',
        'כא','כב','כג','כד','כה','כו','כז','כח','כט','ל','לא','לב','לג','לד','לה','לז','לח','לט','מ',
        'מא','מב','מג','מד','מה','מו','מז','מח','מט','נ','נא','נב','נג','נד','נה','נו','נז','נח','נט','ס',
        'סא','סב','סג','סד','סה','סו','סז','סח','סט','ע','עא','עב'];
        
        const k=this.props.index;
        
        return(
            <div className="text-line">
                <span className="para-num">({heNums[this.props.paraNum-1]})</span>&nbsp;
                <span className="text-para" data-para-index={k}>{filterTaamim(cleanText(this.props.para))}</span>
             </div>
        );
    }
}


export default inject('TextStore')(observer(Paragraph));
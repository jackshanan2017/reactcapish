import React, { Component } from 'react';
import AsyncTools from './../modules/tools/AsyncTools';
import Auth from './../modules/auth/Auth';
import spinner from './../img/spinner.svg';
import commentaryIcon from './../img/commentary-icon.png';

function waitFor(t){
    return new Promise((res,rej)=>{
        setTimeout(()=>{res()},t);
    })
}


export default class CommentaryBox extends Component{

    constructor(props){
        super(props);
        console.log("CommentaryBox constructor");
        this.ctx=this.props.ctx;
        this.state={
            currentContent:'loading',
            paragraph:"paragraph",
            commentaries:[],
            groupCommentaries:[],
            currentgroup:null
        }
    }
    async componentDidMount(){

        console.log("CommentaryBox componentDidMount");
        document.querySelector(".commentary-box").style.width="5%";
        setTimeout(()=>{window.$('.spinner').toggleClass('show');},800);
        setTimeout(()=>{document.querySelector(".commentary-box").style.width="30%";    },100);        
        await this.loadCommentaries();    
        
    }

    async componentDidUpdate(prevProps,prevState){
        //console.log("CommentaryBox componentDidUpdate with prevProps?",prevProps);
        if (prevProps.ts==this.props.ts){
            return;
        }
        await this.loadCommentaries();
    }

    loadCommentaries=async()=>{

        console.log("load commentaries is launched?");

        this.paraIndex=this.props.sObj.parentEl.dataset.paraIndex;
        this.selText={};
        let sObj=this.props.sObj;
        //console.log("commentaryBox sObj?",sObj);
        try{
                 
            if (!sObj.range.startContainer.parentElement.attributes['data-para-index']){
                console.log("CommentaryBox startContainer no parent with attr data-para-index");
                this.selText.from_para_id=parseInt(sObj.range.startContainer.parentElement.parentElement.attributes['data-para-index'].value);    
            }else{
                console.log("CommentaryBox startContainer HAS parent with attr data-para-index");
                this.selText.from_para_id=parseInt(sObj.range.startContainer.parentElement.attributes['data-para-index'].value);
            }
            
            if (!sObj.range.endContainer.parentElement.attributes['data-para-index']){
                console.log("CommentaryBox endContainer no parent with attr data-para-index");
                this.selText.to_para_id=parseInt(sObj.range.endContainer.parentElement.parentElement.attributes['data-para-index'].value);    
            }else{
                console.log("CommentaryBox endContainer HAS parent with attr data-para-index");
                this.selText.to_para_id=parseInt(sObj.range.endContainer.parentElement.attributes['data-para-index'].value);    
            }
            
            this.selText.from_char=sObj.range.startOffset;
            this.selText.to_char=sObj.range.endOffset;
        
        }catch(err){
            console.log(err);
            console.log("sObj???",sObj);
        }
        //console.log("this.selText",this.selText);
        this.selText.to_para_id=this.selText.from_para_id;
        let [res,err]=await Auth.superAuthFetch('/api/Commentaries/getCommentariesForParaRange?fromParaId='+this.selText.from_para_id+'&toParaId='+this.selText.to_para_id);
        //await waitFor(15000);
        if (err){
            console.log("No commentaries, err",err);
            this.setState({currentContent:'commentariesList'});
            return;
        }

        this.setState({commentaries:res,currentContent:'commentariesList'});

        //console.log("Commentaries res",res);
    }


    showCommentariesForGroup=(group)=>{

        console.log("show commentaries for group",group);
        if (this.state.currentGroup==group && this.state.groupCommentaries.length>0){
            this.setState({currentGroup:null,groupCommentaries:[]});
            return;
        }
        
        this.setState({currentGroup:group,currentContent:'commentariesList',groupCommentaries:this.state.commentaries[group]});

    }



    closeBox=()=>{
        
        document.querySelector(".commentary-box").style.width="0%";
        setTimeout(()=>{
            this.ctx.elementsHandler.removeElement(this.props.elementKey);
        },1000);
        
    }

    showCurrentContent=()=>{

        //console.log("showCurrentContent with state",this.state);
        switch (this.state.currentContent){
            case 'loading':
            return (<div className="spinner"><img src={spinner} alt='Loading...' /></div>);
            break;
            case 'commentariesList':
            return(<div>
            <ul className='commentaries-list'>
                {
                    Object.keys(this.state.commentaries).map( (group,groupId)=>{
                    let comm=this.state.commentaries[group];
                    let groupHeName=comm[0].collectiveTitleHe;
                    return <li key={groupId} key={groupId}>
                     
                    <div className='commentary-group' onClick={()=>this.showCommentariesForGroup(group)} >
                        {groupHeName} ({comm.length})
                        <span className='group-toggle'>{group===this.state.currentGroup && this.state.groupCommentaries.length>0 ? "-" : "+"}</span>
                    </div>
                    <div className="commentary-content">
                        {group==this.state.currentGroup && this.state.groupCommentaries.map((gComm,gk)=>{

                            return <div key={gk}>
                                <div className='commentary-title'>{gComm.titleHe}</div>
                                
                                <div className='commentary-body'
                                    dangerouslySetInnerHTML={{__html: gComm.bodyHe}}
                                />
                                <hr />
                            </div>

                        })}
                    </div>

                    </li>;
                })

            }
            </ul></div>);
            break;
            case 'commentaryContent':
            return (
            <div className="commentary-content">
                    {this.state.groupCommentaries.map((gComm,gk)=>{

                        return <div key={gk}>
                            <div className='commentary-title'>{gComm.titleHe}</div>
                            <div className='commentary-body'>{gComm.bodyHe}</div>
                            <hr />
                        </div>

                    })}
                </div>);
            break;
        }
    }
    


    render(){
        return (
            <div className="commentary-box">
                <span className='close-box' onClick={this.closeBox}><i className="fas fa-times"/></span>
                <div className="title"><img className='commentary-icon' src={commentaryIcon} />פרשנות</div>
                <div className="body-container">
                 {this.showCurrentContent()}
                </div>
            </div>
        );
    }
}
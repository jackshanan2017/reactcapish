import React, { Component } from 'react';
import Auth from './../modules/auth/Auth';
import CommentsHandler from './../handlers/CommentsHandler';
import {Modal} from 'react-bootstrap';
import Consts from './../constants/Consts';
import { observer,inject } from 'mobx-react';
import { Dropdown } from 'semantic-ui-react';
import commentIcon from './../img/comment-icon-hover.png';
import ImageUploader from './../modules/fileshandler/client/components/ImageUploader';


const options = [
  { key: 0, text: 'הערה כללית', value: 'English' 
  ,label: { color: 'orange', empty: true, circular: true }
},
  { key: 1, text: 'שלא אשכח', value: 'French' 
  ,label: { color: 'yellow', empty: true, circular: true }
}
  
]

class CategoriesDropDown extends Component {
  state = { options }

  handleAddition = (e, { value }) => {
    this.setState((prevState) => ({
      options: [
      { 
          label: { color: 'yellow', empty: true, circular: true },
          text: value, value }, ...prevState.options],
    }))
  }

  handleChange = (e, { value }) => {
      this.setState({ currentValue: value });
  }

  render() {
    const { currentValue } = this.state

    return (
      <Dropdown
        options={this.state.options}
        placeholder='בחר קטגוריה'
        search
        selection
        fluid
        allowAdditions
        additionLabel='הוסף קטגוריה חדשה: '
        value={currentValue}
        onAddItem={this.handleAddition}
        onChange={this.handleChange}
      />
    )
  }
}





class CommentBox extends Component{

    constructor(props){
        super(props);
        this.ctx=this.props.ctx;
        this.state={
            line:"",
            body:""
        }
        console.log("this.props.commentObj",this.props.commentObj);
    }
    componentDidMount(){}

    removeComment=async(e)=>{

        console.log("removeComment is launched");
        try{
        
        let content=e.target.innerHTML;
        let commentId=this.props.commentObj.id||this.commentId;
        let [res,err]=await Auth.superAuthFetch('/api/Notesmaps/removeNote?noteType=comment&noteId='+commentId);
        //console.log("paraNum?",this.props.paraNum);
        
        document.querySelector("[data-para-index='"+this.props.commentObj.from_para_id+"'] .highlight").className='';
        
        this.props.TextStore.removeNoteById('notes',this.props.commentObj.id);
        this.removeConfirmBox();
        this.closeBox();
        
        }catch(err){
            console.log("err removing comment",err);
            document.location.reload();
        }


    }
    removeConfirmBox=()=>{
        this.ctx.elementsHandler.removeElement(Consts.CONFIRM_ELEMENT_BOX);        
    }



    launchDeleteCommentConfirmBox=()=>{
        
        
        let ConfirmDeletePopup=()=>{
            return(
                <Modal className='capish-modal confirm-box' show={true}>
                  <Modal.Header className='modal-header'></Modal.Header>
                  <Modal.Body>
                      <div className='confirm-box-message'>האם להסיר את ההערה?</div>
                  </Modal.Body>
                  <Modal.Footer className='btns-row'>
                    
                    <div className='col-xs-6'>
                        <button variant="primary" className='capish-btn' onClick={this.removeComment}>
                          אישור
                        </button>
                    </div>
                    <div className='col-xs-6'>
                    <button variant="primary" className='capish-btn' onClick={this.removeConfirmBox}>
                      ביטול
                    </button>
                    </div>
                    
                    
                  </Modal.Footer>
                </Modal>
            );
        }
        

        this.ctx.elementsHandler.addElement(Consts.CONFIRM_ELEMENT_BOX,<ConfirmDeletePopup key={Consts.CONFIRM_ELEMENT_BOX}/>);
    }


    closeBox=()=>{
        
        this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_NOTE_BOX);
    }
    render(){

        const cObj=this.props.commentObj;
        const body=cObj.note;
        const quote=cObj.quote;
        
        const imgSrc= cObj.imageId && cObj.imageId.src ||
        this.props.commentObj.image && this.props.commentObj.image.path || null;

        return (

            <div className="new-comment-box comment-box">

                <div className="title">
                    <i className="far fa-trash-alt" onClick={this.launchDeleteCommentConfirmBox}/>
                    <img src={commentIcon} className='title-img' />"{quote}"
                </div>
                <div className="body-container">
                    <div className="body">{body}
                    
                    {imgSrc &&
                    <div className='im-container'>
                          <img src={imgSrc} />
                    </div>}
                    </div>
                    <div className="buttons-container">
                        <div className="save-btn btn" onClick={this.closeBox}>סגירה</div>
                    </div>
                </div>
                
            </div>

        );
    }
}



export class NewCommentBox extends Component{
    
    constructor(props){
        super(props);
        this.state={
            form:{body:""},
            line:this.props.sObj.sText
        }
        this.commentsHandler=new CommentsHandler(this.props.textContext.showComment);
    }
    

    saveComment=async()=>{

        if (this.state.form.body.trim()==""){
            alert("כדי לשמור יש לכתוב את ההערה תחילה");
            return;
        }
        
        let noteData={from_para_id:null,to_para_id:null,from_char:null,to_char:null,note:this.state.form.body,quote:this.state.line
            ,noteType:"comment"};

        console.log("saveComment is launched with sObj",this.props.sObj);
        let sObj=this.props.sObj;
        let sText=this.props.sObj.sText;

        try{
            noteData.from_para_id=parseInt(sObj.range.startContainer.parentElement.attributes['data-para-index'].value);
            noteData.to_para_id=parseInt(sObj.range.endContainer.parentElement.attributes['data-para-index'].value);
            noteData.from_char=sObj.range.startOffset;
            noteData.to_char=sObj.range.endOffset;

        }catch(err){
            console.error(err);
            this.props.oncloseCommentBox();
            return;
        }

        console.log("this.state",this.state);
        if (this.state.imageId){
            noteData.imageId=this.state.imageId;
        }
        console.log("NOTE DATA BEFORE SAVE",noteData);
        //return;


        let [commentRes,err]=await Auth.superAuthFetch('/api/notes/saveNote',{   headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },method:'POST',body:JSON.stringify( {noteData})   });

        if (err){
            console.log("Could not save comment, aborting with err",err);
            return;
        }
        console.log("commentRes?",commentRes);

        noteData.id=commentRes.noteId;
        this.commentId=noteData.id;
        console.log("this.commentId?",this.commentId);

        //console.log("NOTE DATA",noteData);
        this.commentsHandler.highlightComment(noteData);
        
        this.props.oncloseCommentBox();

    }



    handleChange=(name,e)=>{
        this.state.form[name]=e.target.value;
    }

    handleFileChange = (fileEvent) => {
        let name = (fileEvent.target && fileEvent.target.name) || null;
        let value = (fileEvent.target && fileEvent.target.value) || null;
        if (name && value) {
            this.setState({ [name]: value });
        }
    }


    render(){

        return(
            <div className="new-comment-box">
                <div className="title"><img src={commentIcon} className='title-img' />הוספת הערה</div>
                <div className="body-container">
                    <div className="line">"{this.state.line}"</div>
                    <div className="selection">
                        <CategoriesDropDown />
                    </div>
                    <div className="body"><textarea onChange={(e)=>this.handleChange("body",e)}>{this.state.form.body}</textarea>

                    <div className='add-image-label'>הוספת תמונה</div>
                    <ImageUploader
                        category='comments' // image is saved into public/images/[category]
                        name='imageId' // [IMAGE_NAME_LIKE_IN_DATABASE]
                        required={false}
                        onChange={this.handleFileChange}
                        label='Show us your dog'
                        hideInputBox={true}
                        hideLabel={true}
                        // defaultThumbnailImageSrc=[PATH_TO_YOUR_DEFAULT_IMAGE]//a path in public, example:'/images/mydefaultimg.png'
                    />


                    </div>
                    <div className="buttons-container" style={{'marginTop':'40px'}}>
                        <div className="save-btn btn" onClick={this.saveComment}>שמירה</div>
                        <div className="save-btn btn" onClick={this.props.oncloseCommentBox}>ביטול</div>
                    </div>
                </div>
            </div>
        );

    }
}


export default inject('TextStore')(observer(CommentBox));
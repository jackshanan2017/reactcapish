import React,{useState, useEffect} from 'react';
import Consts from './../../constants/Consts';
import {Modal} from 'react-bootstrap';
import './Modals.scss';

export default function NoteBoxModal(props){

    const title=props.title;
    
    const confirmAction=()=>{
        props.elementsHandler.removeElement(Consts.ELEMENTS_NOTE_BOX);
    }


    return(

      <Modal show={true} style={{direction:'rtl'}} className='capish-modal'>
      
          <Modal.Header>
            <Modal.Title className='modal-title'>{title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              
          </Modal.Body>
          <Modal.Footer className='tac'>
            
            <button onClick={confirmAction} variant="primary" className="capish-btn ib" style={{'width':'80px'}}>
              אוקי
            </button>
          </Modal.Footer>
        </Modal>);
}

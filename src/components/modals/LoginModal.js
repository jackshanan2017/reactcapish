import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import './Modals.scss';
import capishLogoImg from './../../img/capish-logo-yougotit.gif';
import Consts from './../../constants/Consts';
import Auth from './../../modules/auth/Auth';
import AsyncTools from './../../modules/tools/AsyncTools';
import { withRouter } from "react-router-dom";



export class LoginModalMenu extends Component {

    constructor(props) {
        super(props);

        this.ctx = this.props.ctx;
        this.state = { show: true };

    }

    closeModal = () => {
        this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_LOGIN_MODAL_MENU);
    }

    launchLoginModalForm = () => {

        this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_LOGIN_MODAL_MENU);
        this.ctx.elementsHandler.addElement(Consts.ELEMENTS_LOGIN_MODAL_FORM, <LoginModalForm key={0} ctx={this} />);

    }

    launchRegistrationModalForm = () => {

        this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_LOGIN_MODAL_MENU);
        this.ctx.elementsHandler.addElement(Consts.ELEMENTS_REGISTARTION_MODAL_FORM, <RegistrationModalForm key={0} ctx={this.ctx} />);

    }



    render() {



        return (

            <Modal show={this.state.show} style={{ direction: 'rtl' }} className='capish-modal' onHide={this.closeModal}>

                <Modal.Header closeButton className='modal-header'>
                    <Modal.Title className='tac top-title'>הצטרפות</Modal.Title>
                </Modal.Header>

                <Modal.Body className="tac modal-body">

                    <div className='title-top tac'>הצטרפו ללימוד מעמיק עם</div>
                    <img className='capish-logo-img ib' src={capishLogoImg} />
                    <div className='tac'>
                        <div className='capish-btn ib' onClick={this.launchLoginModalForm}>התחבר באמצאות דוא"ל<i className='fas fa-envelope' /></div>
                    </div>
                    <div className='register-here-q tac'>אין לכם חשבון?</div>
                    <div className='register-here tac' onClick={this.launchRegistrationModalForm}>הרשמו כאן</div>

                </Modal.Body>
                <Modal.Footer className='modal-footer'></Modal.Footer>
            </Modal>

        );
    }


}




class _LoginModalForm extends Component {

    constructor(props) {
        super(props);

        this.ctx = this.props.ctx;
        //this.state = { show: true, form: { email: "admin@carmel6000.amitnet.org", pw: "E2PSzAmJ-5-ldKnl" } };
        this.state = { show: true, form: { email: "", pw: "" } };

    }

    closeModal = () => {
        console.log("close modal is launched?");
        this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_LOGIN_MODAL_MENU);
    }

    handleChange = (e, key) => {

        let form = this.state.form;
        form[key] = e.target.value;
        this.setState({ form });

    }

    handleLogin = () => {

        console.log("this.state.form?", this.state.form);

        let pw = this.state.form.pw;
        let email = this.state.form.email;

        this.setState({ isLoading: true });

        Auth.authenticate(email, pw, (res, role) => {

            console.log("after authenticate res",res);

            this.setState({ isLoading: false });

            if (res.success === false) {
                console.log("login failed with error", res.msg);
                return;
            }

            if (res.success === true) {

                console.log("login success");
                window.location = '/study-page';

            }
        });


    }



    render() {

        let loginButton;

        if (this.state.isLoading) {

            loginButton = <div className='capish-btn ib'>המתן...</div>
        } else {

            loginButton = <div className='capish-btn ib' onClick={this.handleLogin}>כנס</div>
        }


        return (

            <Modal show={this.state.show} style={{ direction: 'rtl' }}
                className='capish-modal' onHide={this.closeModal}>

                <Modal.Header closeButton className='modal-header'>
                    <Modal.Title className='tac top-title'>התחברות</Modal.Title>
                </Modal.Header>

                <Modal.Body className="tac modal-body">
                    <input type='text' placeholder='אימייל' value={this.state.form.email} onChange={(e) => this.handleChange(e, 'email')} className='login-email item' required />
                    <input type='password' placeholder='סיסמא' value={this.state.form.pw} onChange={(e) => this.handleChange(e, 'pw')} className='login-pw item' required />


                    {loginButton}

                </Modal.Body>

                <Modal.Footer className='modal-footer'></Modal.Footer>



            </Modal>

        );
    }


}

//export withRouter(LoginModalForm) as LoginModalForm;
export const LoginModalForm = withRouter(_LoginModalForm);













































































































class _RegistrationModalForm extends Component {

    constructor(props) {
        super(props);

        //console.log("props?!",props);

        this.ctx=this.props.ctx;
        this.state = { errMessages:[],isRegDone:false,show: true, form: { email: "", password: "", username: "" } };

    }

    closeModal = () => {

        this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_REGISTRATION_MODAL_MENU);
    }

    handleChange = (e, key) => {

        let form = this.state.form;
        form[key] = e.target.value;
        this.setState({ form });

    }

    handleRegistation = async () => {

        console.log("this.state.form?", this.state.form);
        let errs=[];

        if (this.state.form.email.trim()==""){

          errs.push(<li key="0">כתובת המייל ריקה, מלא את השדא ונסה שוב</li>);
          this.setState({errMessages:errs,isLoading:false});
          return;

        }

        if (this.state.form.username.trim()==""){

          errs.push(<li key="0">לא רשמת את שמך המלא, מלא את השדא ונסה שוב</li>);
          this.setState({errMessages:errs,isLoading:false});
          return;

        }

        if (this.state.form.password.trim()=="" || this.state.form.password.length < 7){

          errs.push(<li key="0">יש לבחור סיסמא בעלת יותר משישה תוים</li>);
          this.setState({errMessages:errs,isLoading:false});
          return;

        }

        //let pw=this.state.form.pw;
        //let email=this.state.form.email;

        this.setState({ isLoading: true });

        let payload = this.state.form;

        console.log("payload?", payload);



        let res = await AsyncTools.superFetch('/api/CustomUsers', {
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
            method: "POST",
            body: JSON.stringify(payload)
        })

        console.log("register res", res);
        console.log("res[0]?",res[0]);
        //console.log("res[0].id?",!isNaN(res[0].id));

        if (res[0] && !isNaN(res[0].id)){

          this.setState({isRegDone:true});


        }

        if (res[1] && res[1].error ){

          

          let err=res[1].error.message;

          if (err.includes('Email already exists')){

            errs.push(<li key="0">האימייל שבחרת כבר קיים במערכת</li>);

          }

          this.setState({errMessages:errs,isLoading:false});




        }




    }



    render() {

        let regButton;

        if (!this.state.isRegDone){

            if (this.state.isLoading) {

              regButton = <div id="regButton" className='capish-btn ib'>המתן...</div>
            } else {

              regButton = <div id="regButton" className='capish-btn ib' onClick={this.handleRegistation}>הרשם</div>
            }

        }else{

            regButton= <div style={{'marginTop':'20px'}}>הרשמתך התקבלה. כדי להפעיל את חשבונך לחץ על הקישור שנשלח אליך למייל</div>
        }
        


        return (

            <Modal show={this.state.show} style={{ direction: 'rtl' }}
                className='capish-modal reg-modal' onHide={this.closeModal}>

                <Modal.Header closeButton className='modal-header'>
                    <Modal.Title className='tac top-title'>הרשמה</Modal.Title>
                </Modal.Header>

                <Modal.Body className="tac modal-body">

                    <label htmlFor="username">שם מלא</label>
                    <input id="username" type='text' value={this.state.form.username} onChange={(e) => this.handleChange(e, 'username')} required />

                    <label htmlFor="role">תפקיד</label>
                    <select id="role"><option>מורה</option><option>תלמיד</option></select>

                    <label htmlFor="email">אימייל</label>
                    <input id="email" type='text' value={this.state.form.email} onChange={(e) => this.handleChange(e, 'email')} required />

                    <label htmlFor="pw">סיסמא</label>
                    <input id="pw" type='password' value={this.state.form.password} onChange={(e) => this.handleChange(e, 'password')} required />



                    {regButton}
                    
                    <ul>
                    {this.state.errMessages}
                    </ul>


                </Modal.Body>

                <Modal.Footer className='modal-footer'></Modal.Footer>



            </Modal>

        );
    }


}

export const RegistrationModalForm = withRouter(_RegistrationModalForm);
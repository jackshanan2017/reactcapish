import React, { Component } from 'react';
import { Modal } from 'react-bootstrap';
import './Modals.scss';
import capishLogoImg from './../../img/capish-logo-yougotit.gif';
import Consts from './../../constants/Consts';
import Auth from './../../modules/auth/Auth';
import AsyncTools from './../../modules/tools/AsyncTools';
import { withRouter } from "react-router-dom";
import NoteBoxModal from './NoteBoxModal';


class NewPersonForm extends Component{
    constructor(props){
        super(props);
        this.state={description:"",term:''};
    }
    render(){
        return (
        <div className="row item">
            <div className="col-xs-8 tal">
                <textarea onChange={(e) => this.props.updateForm('description',e)} required />
            </div>
            <div className="col-xs-4 tar">תיאור:</div>
        </div>);
    }
}

class NewLocationForm extends Component{
    constructor(props){
        super(props);
        this.state={description:""};
    }
    render(){
        return(
        <div>
            <div className="row item">
                <div className="col-xs-8 tal">
                    <input type='text' value={this.props.formData.long || ''} className='long' onChange={(e) => this.props.updateForm('long',e)}/>
                </div>
                <div className="col-xs-4 tar">נ.צ longitude:</div>
            </div>    
            <div className="row item">
                <div className="col-xs-8 tal">
                    <input type='text' value={this.props.formData.lat || ''} className='lat' onChange={(e) => this.props.updateForm('lat',e)} />
                </div>
                <div className="col-xs-4 tar">נ.צ latitude:</div>
            </div>
            
            <div className="row item">
                <div className="col-xs-8 tal">
                    <textarea value={this.props.formData.description || ''} onChange={(e) => this.props.updateForm('description',e)} />
                </div>
                <div className="col-xs-4 tar">תיאור:</div>
            </div>
        </div>);
    }
}


class MyAutoComplete extends Component{

    constructor(props){
        super(props);
        let currentObject={};
        let items=this.props.items;
        console.log("My auto complete items?",items);
        //this.labelKey=this.props.labelKey;

        
        /*
        if (!this.props.currentIndex && this.props.items[this.props.currentIndex]){            
            
            currentObject={index:this.props.currentIndex};
            currentObject[this.labelKey]=this.props.items[this.props.currentIndex][this.labelKey];
            
            items=this.props.items.filter(i=>i[this.labelKey].includes(currentObject[this.labelKey]));
        }*/
        if (this.props.defaultLabel){
            currentObject[this.props.labelKey]=this.props.defaultLabel;
            currentObject.index=null;
            currentObject.isNew=true;
            this.props.setSelected(this.props.fieldKey,currentObject);
        }

        this.state={items,currentObject,listName:this.props.listName};

    }

    componentDidUpdate(){
        if (this.props.listName!==this.state.listName && JSON.stringify(this.props.items)!==JSON.stringify(this.state.items)){
            console.log("Resetting items.....",this.props.items);
            this.setState({items:[],listName:this.props.listName});

        }

    }




    onType=(e)=>{

        let items=[];
        if (e.target.value.trim()==''){
            items=[];
        }else{
            //if (this.state.items && this.state.items>0){
                items=this.props.items.filter(i=>i[this.props.labelKey].includes(e.target.value));    
                if (items.length>15){
                    items=items.slice(0,14);    
                }
                
            //}            
        }
        let currentObject={id:null,isNew:true};
        currentObject[this.props.labelKey]=e.target.value;

        this.setState(
            {currentObject,items},()=>{
            this.props.setSelected(this.props.fieldKey,this.state.currentObject);
        });
        

    }

    setChosen=(i)=>{


        this.setState({currentObject:this.state.items[i],index:i,items:[]},()=>{
            this.props.setSelected(this.props.fieldKey,this.state.currentObject);    
            this.props.onSelectDropDown(this.state.currentObject);
        });
        
    }

    render(){
        if (!this.props.items) return;

        const labelKey=this.props.labelKey;
        //console.log("labelKey is?",labelKey);
        //console.log("this.state.items?",this.state.items);

        return(
            <div className='autocomplete'>
                <input className='ac-input-box' type='text' 
                onChange={this.onType}
                value={this.state.currentObject[labelKey] || ''} />
                <ul className='ac-suggestions-list'>
                    {this.state.items.map( (item,k)=> <li onClick={()=>this.setChosen(k)} 
                        key={k}>{item[labelKey]}</li>)}
                </ul>
            </div>

            );
    }
}

export class _DefineUnknownsModal extends Component {

    constructor(props) {
        super(props);
        this.ctx = this.props.ctx;
        this.state = {show: true,termType:'location',items:[],labelKey:'title',forms:{ person:{},location:{} } };
        this.locList=[];
        this.perList=[];
        this.dataFetched=false;

    }

    componentWillUnMount(){
        console.log("componentWillUnmount is launched");
        this.locList=[];
        this.perList=[];
    }

    async componentDidMount(){

          //TODO: Move this into a store
          if (!this.dataFetched){
              
              this.dataFetched=true;
              console.log("FETCHING LOCATIONS/PERSONS");

              //let locfieldsFilter="filter[fields][title]=true&filter[fields][id]=true";
              let [locList,err]=await Auth.superAuthFetch('/api/Locations/',{ method: 'GET', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }} );
              this.locList=locList;
              console.log("locations res",this.locList);

              //let perFieldsFilter="filter[fields][term]=true&filter[fields][id]=true";
              let [perList,perErr]=await Auth.superAuthFetch('/api/Persons/',{ method: 'GET', headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }} );

              this.perList=perList;
              console.log("persons res",this.perList);          

          }
          
          
          
          if (this.state.termType=='person'){
              this.setState({items:this.perList,labelKey:'term'});
          }else{
              console.log("setting this.state.items to be ",this.locList);
              this.setState({items:this.locList,labelKey:'title'});
          }

    }

    closeModal = () => {
        console.log("close modal is launched?");

        this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_DEFINE_UNKOWNS_MODAL);
    }

    handleChange = (e, key) => {

        let form = this.state.form;
        form[key] = e.target.value;
        this.setState({ form });

    }

    

    switchTermType=(e)=>{

        let newTerm=e.target.options[e.target.selectedIndex].value;
        console.log("newTerm?",newTerm);

        let items=[];
        let labelKey='';

        if (newTerm=='person'){
              items=this.perList;
              labelKey='term';
          }else{
              items=this.locList;
              labelKey='title';
          }

          console.log("items now is ",items);

        
        this.setState({termType:newTerm,items,labelKey});

    }

    savePersonForm=async(formData)=>{

        console.log("save person form is launched with formData",formData);

        if (formData.term.term.trim()=='' || formData.term.term.length<2){
            alert("לא ניתן לשמור דמות חדשה ללא שם");
            return;
        }
        if (formData.description.trim()=='' || formData.description.length<2){
             alert('לא ניתן לשמור דמות חדשה ללא תיאור קצר');
             return;
        }

        let body={};
        
        
        body={
            
            term:formData.term.term,
            contextTerm:this.props.termObj.context_term,
            notemapId:this.props.termObj.id,
            personId:parseInt(formData.term.id) || 0,
            description:formData.description, 
            isNew:formData.term.isNew?true:false
        };
        
        
        let [saveRes,err]=await Auth.superAuthFetch('/api/Persons/matchNewPerson',{ method: 'POST', body:JSON.stringify(body),headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }} );
        console.log("saveRes",saveRes);
        console.log("err?",err);

        //this.ctx.elementsHandler.addElement(Consts.ELEMENTS_NOTE_BOX,<NoteBoxModal elementsHandler={this.elementsHandler} key={Consts.ELEMENTS_NOTE_BOX} title={'המונח נשמר בהצלחה, על מנת לצפות בשינויים יש לטעון את הטקסט מחדש'} />);
        alert('המונח נשמר בהצלחה, על מנת לצפות בשינויים יש לטעון את הטקסט מחדש');
        //this.ctx.elementsHandler.addElement(Consts.ELEMENTS_CONFIRM_BOX,<ConfirmBoxModal elementsHandler={this.ctx.elementHandler} key={Consts.ELEMENTS_CONFIRM_BOX} title={'האם למחוק את הפריט?'} confirmAction={confirmAction}/>)
        window.location.reload();
        

    }
    saveLocationForm=async(formData)=>{
        console.log("save location form is launched with formData",formData);

        
        if (!formData.term || formData.term.title.trim()=='' || formData.term.title.length<2){
            alert("לא ניתן לשמור מקום חדש ללא שם");
            return;
        }
        if (!formData.long || !formData.lat || formData.long=='' || formData.lat==''){
             alert('לא ניתן לשמור מקום חדש ללא נ.צ גיאוגרפי');
             return;
        }

        if (!formData.description || formData.description.trim()=='' || formData.description.length<2){
            alert('לא ניתן לשמור מקום חדש ללא תיאור המקום');
            return;
        }

        let body={};
        try{
        
        body={
            term:formData.term.title,
            contextTerm:this.props.termObj.context_term,
            notemapId:this.props.termObj.id,
            locationId:parseInt(formData.term.id) || 0,
            long:parseInt(formData.long),
            lat:parseInt(formData.lat),
            description:formData.description, 
            isNew:formData.term.isNew?true:false
        };
        }catch(err){
            alert('יש להקפיד להקליד מספרים במקומות הנכונים');
            return;
        }

        console.log("BODY?",body);

        let [saveRes,err]=await Auth.superAuthFetch('/api/Locations/matchNewLocation',{ method: 'POST', body:JSON.stringify(body),headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }} );

        console.log("saveRes",saveRes);
        console.log("err?",err);

        //this.ctx.elementsHandler.addElement(Consts.ELEMENTS_NOTE_BOX,<NoteBoxModal elementsHandler={this.elementsHandler} key={Consts.ELEMENTS_NOTE_BOX} title={'המונח נשמר בהצלחה, על מנת לצפות בשינויים יש לטעון את הטקסט מחדש'} />);
        alert('המונח נשמר בהצלחה, על מנת לצפות בשינויים יש לטעון את הטקסט מחדש');
        //this.ctx.elementsHandler.addElement(Consts.ELEMENTS_CONFIRM_BOX,<ConfirmBoxModal elementsHandler={this.ctx.elementHandler} key={Consts.ELEMENTS_CONFIRM_BOX} title={'האם למחוק את הפריט?'} confirmAction={confirmAction}/>)
        window.location.reload();



    }
    saveForm=async()=>{

        console.log("Saving form of type (%s), with data",this.state.termType,this.state.forms[this.state.termType]);

        let formData=this.state.forms[this.state.termType];

        if (Object.keys(formData).length==0){
            alert('יש להזין נתונים');
            console.log("Form is empty");
            return;
        }


        if (this.state.termType=='person'){
            this.savePersonForm(formData);
        }else{
            this.saveLocationForm(formData);
        }

    }

    onUpdateForm=(key,e)=>{
        let forms=this.state.forms;
        forms[this.state.termType][key]=e.target.value;
        this.setState({forms});
    }

    updateFormWithCustomValue=(key,val)=>{
        let forms=this.state.forms;
        forms[this.state.termType][key]=val;

        this.setState({forms});
    }

    onSelectDropDown=(obj)=>{
        console.log("onSelectDropDown with values",obj);
        if (this.state.termType=='location'){
            let forms=this.state.forms;
            forms.location.long=obj.longitude;
            forms.location.lat=obj.latitude;
            forms.location.description=obj.fewWords;
            this.setState(forms);
        }

    }



    render() {

        let saveButton;

        if (this.state.isLoading) {

            saveButton = <div className='capish-btn ib'>המתן...</div>
        } else {

            saveButton = <div className='capish-btn ib' onClick={this.saveForm}>שמירה</div>
        }

        

        return (

            <Modal show={this.state.show} style={{ direction: 'rtl' }}
                className='capish-modal define-terms-modal' onHide={this.closeModal}>

                <Modal.Header closeButton className='modal-header'>
                    <Modal.Title className='tac top-title'>הגדרת מונח חדש</Modal.Title>
                </Modal.Header>

                <Modal.Body className="tac modal-body">
                    <div className="row item">
                        
                        
                    <div className="col-xs-8 tal autocomplete-container">
                        <MyAutoComplete
                          listName={this.state.termType}                                  
                          labelKey={this.state.labelKey}
                          items={this.state.items}                          
                          defaultLabel={this.props.term}
                          fieldKey={'term'}
                          setSelected={this.updateFormWithCustomValue}
                          onSelectDropDown={this.onSelectDropDown}
                        />
                     </div>

                        <div className="col-xs-4 tar">מונח מקור:</div>
                    </div>
                    <div className="row item">
                        <div className="col-xs-8 tal">{this.props.termObj.context_term}</div>
                        <div className="col-xs-4 tar">מונח בתוך ההקשר:</div>
                    </div>

                    <div className="row item">
                        <div className="col-xs-8 tal">
                            <select onChange={this.switchTermType} required defaultValue={this.state.termType}>
                                <option value='person'>דמות</option>
                                <option value='location'>מקום</option>
                            </select>
                        </div>
                        <div className="col-xs-4 tar">סוג:</div>
                    </div>

                    {this.state.termType=='person' ? 
                    <NewPersonForm  formData={this.state.forms[this.state.termType]} updateForm={this.onUpdateForm}/> 
                    : 
                    <NewLocationForm formData={this.state.forms[this.state.termType]} updateForm={this.onUpdateForm}/>}
                    


                    {saveButton}

                </Modal.Body>

                <Modal.Footer className='modal-footer'></Modal.Footer>



            </Modal>

        );
    }


}

export const DefineUnknownsModal = withRouter(_DefineUnknownsModal);
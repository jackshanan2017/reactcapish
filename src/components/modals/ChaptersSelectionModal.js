import React, { Component } from 'react';
import {Modal} from 'react-bootstrap';


export default class ChaptersSelectionModal extends Component{

  constructor(props){
          super(props);

          this.ctx=props.ctx;

          this.chaptersForm=React.createRef();
          
          this.books=this.ctx.bibleIndex.getBooksList();

          this.state={show:true,
            toBook:this.books.slice(0,2),
            fromChaptersList:this.ctx.bibleIndex.getChaptersListByBookName(),
            toChaptersList:this.ctx.bibleIndex.getChaptersListByBookName(),
            fromParasList:this.ctx.bibleIndex.getParasListByChapter(),
            toParasList:this.ctx.bibleIndex.getParasListByChapter()
          };

  }

  componentDidMount(){
    setTimeout(()=>{
      //console.log("this.chaptersForm.current",this.chaptersForm.current);
      let els=this.chaptersForm.current.elements;
      els['toParaNum'].selectedIndex=els['toParaNum'].options.length-1;

    },500);
    
  }

  hideModal=()=>{
    this.setState({show:false});
  }


  setChaptersSelection=()=>{
        
        let els=this.chaptersForm.current.elements;//document.querySelector("#chaptersForm").elements;
        const keys=["fromBook","fromChapter","fromParaNum","toBook","toParaNum"];
        let cSel={};
        keys.forEach(k=>{
          
          cSel[k]=els[k].options[els[k].selectedIndex].value;
          //console.log("for key (%s)",k,cSel[k]);


        });
        cSel.fromBookHe=els["fromBook"].options[els["fromBook"].selectedIndex].text;
        cSel.fromChapterHe=els["fromChapter"].options[els["fromChapter"].selectedIndex].text;
        cSel.toChapter=this.ctx.bibleIndex.heNumToIndex(els["toChapter"].options[els["toChapter"].selectedIndex].text);


        this.props.TextStore.setDocumentName(cSel);

        //console.log("Chapters selection",cSel);
        this.props.onSubmitSelection(cSel,this.hideModal);
        
        
  }

  resetSelections=()=>{
      //console.log("resetSelections?");
      this.chaptersForm.current.fromChapter.selectedIndex=0;
      this.chaptersForm.current.fromParaNum.selectedIndex=0;
      this.chaptersForm.current.toChapter.selectedIndex=0;
      let els=this.chaptersForm.current.elements;
      els['toParaNum'].selectedIndex=els['toParaNum'].options.length-1;
  }



  updateChapter=(e)=>{

    console.log("fromChapter is changed");

    let toChaptersList=[...this.state.toChaptersList];

    toChaptersList=toChaptersList.slice(e.target.selectedIndex,toChaptersList.length);

    let fromBookElem=this.chaptersForm.current.elements.fromBook;
    let currBookName=fromBookElem[fromBookElem.selectedIndex].value;
    let toParasList=this.ctx.bibleIndex.getParasListByChapter(currBookName,e.target.selectedIndex);

    this.setState({toChaptersList,toParasList},()=>{
      let els=this.chaptersForm.current.elements;
      els['toChapter'].selectedIndex=0;
      els['toParaNum'].selectedIndex=els['toParaNum'].options.length-1;
    });

  }

  updatePara=(e)=>{

    
    let fromBookElem=this.chaptersForm.current.elements.fromBook;
    let currBookName=fromBookElem[fromBookElem.selectedIndex].value;
    //console.log("currBookName",currBookName);

    let currChapterIndex=this.chaptersForm.current.elements.fromChapter.selectedIndex;
    //console.log("currChapterIndex",currChapterIndex);

    let toParasList=this.ctx.bibleIndex.getParasListByChapter(currBookName,currChapterIndex);
    //console.log("toParaList before slice",toParasList);

    toParasList=toParasList.slice(e.target.selectedIndex,toParasList.length);
    //console.log("toParaList after slice",toParasList);

    this.setState({toParasList});
    

  }


  updateBook=(e)=>{
    
      //console.log("e.target.name?",e.target.name);
      //console.log("selected value?",e.target.options[e.target.selectedIndex].value);

   
      //console.log("selected index?",e.target.selectedIndex);

      let fromBook=this.books.slice(e.target.selectedIndex,this.books.length);
      fromBook=fromBook.slice(0,2);


      let currBookName=e.target.options[e.target.selectedIndex].value;
      this.setState({
        toBook:fromBook,
        fromChaptersList:this.ctx.bibleIndex.getChaptersListByBookName(currBookName),
        toChaptersList:this.ctx.bibleIndex.getChaptersListByBookName(currBookName),
        fromParasList:this.ctx.bibleIndex.getParasListByChapter(currBookName),
        toParasList:this.ctx.bibleIndex.getParasListByChapter(currBookName)
      },()=>{
        this.resetSelections();
      });

      

  
  }
        

  render(){

    const books=this.books;
    

    return(

      <Modal show={this.state.show} style={{direction:'rtl'}} className='capish-modal chapters-selection-modal'>
      
          <Modal.Header>
            <Modal.Title className='modal-title'>פתיחת דף לימוד</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <form id='chaptersForm' ref={this.chaptersForm}>
              <div className='row mb20 mt20'>

                <div className='col-xs-10'>
                  <select name="fromBook" className='dropdown-book' onChange={this.updateBook}>{books.map((b,k)=><option key={k} value={b.en}>{b.he}</option>)}</select>&nbsp;
                  <select name="fromChapter" className='dropdown-chapter' onChange={this.updateChapter}>
                  {this.state.fromChaptersList.map((le,k)=><option key={k} value={k+1}>{le}</option>)}</select>
                  &nbsp;פס'&nbsp;
                  <select name="fromParaNum" className='dropdown-para' onChange={this.updatePara}>{this.state.fromParasList.map((le,k)=><option value={k} key={k}>{le}</option>)}</select>
                </div>
                <div className='col-xs-2' style={{textAlign:'left'}}>מ:</div>
                

              </div>

              <div className='row mb30'>
                <div className='col-xs-10'>
                  <select name="toBook" className='dropdown-book'>{this.state.toBook.map((b,k)=><option key={k} value={b.en}>{b.he}</option>)}</select>&nbsp;
                  <select name="toChapter" className='dropdown-chapter'>{this.state.toChaptersList.map((le,k)=><option key={k} value={k+1}>{le}</option>)}</select>
                  &nbsp;פס'&nbsp;
                  <select name="toParaNum" className='dropdown-para'>{this.state.toParasList.map((le,k)=><option value={k} key={k}>{le}</option>)}</select>
                </div>
                <div className='col-xs-2' style={{textAlign:'left'}}>עד:</div>
                
              </div>
              
              


              </form>
          </Modal.Body>
          <Modal.Footer className='tac'>
            <button variant="primary" onClick={this.setChaptersSelection} className="capish-btn ib">
              קאפיש!
            </button>
          </Modal.Footer>
        </Modal>

      );
  }        


}


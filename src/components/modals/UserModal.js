import React, { Component } from 'react';
import {Modal} from 'react-bootstrap';
import './Modals.scss';
import Consts from './../../constants/Consts';
import Auth from './../../modules/auth/Auth';
import { withRouter } from "react-router-dom";


class _UserModal extends Component{

  constructor(props){
          super(props);
          this.state={show:true};

  }

  hideModal=()=>{
    this.setState({show:false});
  }

  handleChange=(e,key)=>{

    let form=this.state.form;
    form[key]=e.target.value;
    this.setState({form});

  }

  handleLogin=()=>{

      console.log("this.state.form?",this.state.form);
      
        let pw=this.state.form.pw;
        let email=this.state.form.email;
      
        this.setState({ isLoading: true });
        
        Auth.authenticate(email, pw, (res, role) => {

            this.setState({ isLoading: false });

            if (res.success === false) {
                console.log("login failed with error", res.msg);
                return;
            }

            if (res.success === true) {

                console.log("login success");
                window.location='/study-page';

            }
        });
      

  }

        

  render(){

    let loginButton;

    if (this.state.isLoading){
      
      loginButton=<div className='capish-btn ib'>המתן...</div>
    }else{

      loginButton=<div className='capish-btn ib' onClick={this.handleLogin}>כנס</div>
    }

   
    return(

      <Modal show={this.state.show} style={{direction:'rtl'}} className='capish-modal'>
      
          <Modal.Header closeButton className='modal-header'>
            <Modal.Title className='tac top-title'>מסמכי העבודה שלי</Modal.Title>
          </Modal.Header>
          
          <Modal.Body className="tac modal-body">
            
          
          </Modal.Body>

          <Modal.Footer className='modal-footer'></Modal.Footer>
          
          
          
        </Modal>

      );
  }        


}

//export withRouter(LoginModalForm) as LoginModalForm;
export const UserModal= withRouter(_UserModal);


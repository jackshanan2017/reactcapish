import React,{useState, useEffect} from 'react';
import Consts from './../../constants/Consts';
import {Modal} from 'react-bootstrap';
import './Modals.scss';

export default function ConfirmBoxModal(props){

    const title=props.title;
    
    const denyAction=()=>{
        props.elementsHandler.removeElement(Consts.ELEMENTS_CONFIRM_BOX);
    }


    return(

      <Modal show={true} style={{direction:'rtl'}} className='capish-modal'>
      
          <Modal.Header>
            <Modal.Title className='modal-title'>{title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
              
          </Modal.Body>
          <Modal.Footer className='tac'>
            <button onClick={props.confirmAction} variant="primary" className="capish-btn ib" style={{'marginLeft':'20px','width':'80px'}}>
              כן
            </button>
            <button onClick={denyAction} variant="primary" className="capish-btn ib" style={{'width':'80px'}}>
              לא
            </button>
          </Modal.Footer>
        </Modal>);
}

import React, { Component } from 'react';
import {Modal} from 'react-bootstrap';
import Auth from './../../modules/auth/Auth';
import { observer,inject } from 'mobx-react';
import './Modals.scss';


class DocumentBox extends Component{


        
  render(){

    return (
      
        <div className='document-box'>{this.props.doc.title}</div>

        );

  }      
        
 }


class LoadDocumentModal extends Component{

  constructor(props){
          super(props);
          this.state={show:true,documents:[]};
          this.chosenDocument=null;

  }

  componentDidMount(){

     (async()=>{
       let [docRes,err]=await Auth.superAuthFetch('/api/Documents/getUserDocuments');  
       if (err){console.error(err)};
       console.log("docRes?",docRes);

       if (Array.isArray(docRes) && docRes.length>0){
         this.setState({documents:docRes});
       }
       
     })();
     
  }

  

  loadDocument=()=>{

    //this.props.loadDocument();
    //cSel.fromBookHe=els["fromBook"].options[els["fromBook"].selectedIndex].text;
    //this.ctx.setState();
    //let sel=document.querySelector("[name='selected-document']");
    //let docId=sel.options[sel.selectedIndex].value;
    if (!this.chosenDocument){
      alert('בחר מסמך כדי להמשיך');
      return;
    }


    (async()=>{

      await this.props.TextStore.loadDocument(this.chosenDocument.id);
      this.props.ctx.drawNotes(this.props.TextStore.notes);

    })();


    
    this.closeModal();



  }

  chooseDocument=(e,doc)=>{
    
    
    let docElems = document.querySelectorAll('.document-box');
    [].forEach.call(docElems,(docElem)=> {
      docElem.classList.remove('chosen');
    });


    let chosen=e.target.classList.toggle('chosen');
    console.log("chosen?",chosen);
    this.chosenDocument=chosen ? doc : null;
  }

  closeModal=()=>{

     this.props.closeModal();
  }


 
  render(){

    //const books=this.books;
    const heLetters=this.heLetters;

    return(

      <Modal show={this.state.show} style={{direction:'rtl'}} onHide={this.closeModal} 
      className='capish-modal load-document-modal'>
          <Modal.Header closeButton>
            <Modal.Title>ניהול מסמכי עבודה</Modal.Title>
          </Modal.Header>
          <Modal.Body>
          {/*
          <select name='selected-document'>
            {this.state.documents.map((doc,k)=><option key={k} value={doc.id}>{doc.title}</option>)}
          </select>
        */}

          <div className='user-documents'>
            {this.state.documents.map((doc,k)=>
              <div key={k} className='document-box' onClick={(e)=>this.chooseDocument(e,doc)}>{doc.title}</div>
              )
          }
          </div>
          </Modal.Body>
          <Modal.Footer className='tac'>
            <button variant="primary" onClick={this.loadDocument} className='capish-btn ib'>
              טען
            </button>
          </Modal.Footer>
        </Modal>

      );
  }        
}


export default inject('TextStore')(observer(LoadDocumentModal));
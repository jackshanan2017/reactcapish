import React, { Component } from 'react';
import AsyncTools from './../modules/tools/AsyncTools';
import Auth from './../modules/auth/Auth';
import spinner from './../img/spinner.svg';
import LangTools from './../modules/tools/LangTools';
import pinHoverImg from './../img/topmenubar/pin-hover.gif';
//import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

export default class LocationBox extends Component{

    constructor(props){
        super(props);
        this.ctx=this.props.ctx;
        
        this.state={
            location:this.props.locationObj,
            locationName:this.props.word,
            body:this.props.locationObj['few_words'] || "",
            alsoCalled:this.props.locationObj['also_called'] || ""
        }

        console.log("locationObj",this.state.location);


    }
    getLocationsForChapter=async()=>{
      
            
            //let [err,res]=await AsyncTools.superFetch('/api/Notesmaps?filter='+JSON.stringify(
                //{include:['Commentaries'],where:{lastParaId:this.selText.from_para_id,noteType:'commentary'}
            //}));

            
            let paraId=this.props.locationObj.last_para_id;
            console.log("location paraId?",paraId);
            if (isNaN(paraId)){
              console.log("paraId is not a number");
              return;
              
            }
            
            let [res,err]=await AsyncTools.superFetch('/api/Locations/getLocationsForChapterByParaId?paraId='+paraId);
            console.log("err",err);
            if (err){
                console.log("No locations, err",err);
                //this.setState({currentContent:'commentariesList'});
                return;
            }

            console.log("locations ?",res);
            
            //this.setState({chapterLocs:res});
            return res;

            //console.log("Locations res",res);


        
    }
    componentDidMount(){

        (async()=>{

        let chapterLocs=[];


        chapterLocs=await this.getLocationsForChapter();

        console.log("lat?",this.state.location.latitude);
        console.log("lng?",this.state.location.longitude);

        
        let lat=parseInt(this.state.location.latitude);
        let lng=parseInt(this.state.location.longitude);


        if (isNaN(lat) || isNaN(lng)){
            console.log("lat (%d) or lng (%d) are not numbers ",lat,lng);
            return;
        }

        this.map = new window.google.maps.Map(document.getElementById('map'), {
          center: {lat: lat, lng: lng},
          zoom: 8,
          streetViewControl:false,
          mapTypeId: window.google.maps.MapTypeId.TERRAIN,
          mapTypeControl: false
        });
        
        let myStyle=                                       
        [     
             {                                            
                featureType: 'administrative',                       
                elementType: 'all',                        
                stylers: [{visibility: 'off'}]             
              },                                                                                        
              {                                            
                featureType: 'road',                       
                elementType: 'all',                        
                stylers: [{visibility: 'off'}]             
              },                                           
              {                                            
                featureType: 'transit',                    
                elementType: 'all',                        
                stylers: [{visibility: 'off'}]             
              },                                           
              {                                            
                featureType: 'all',                         
                elementType: 'labels',                        
                stylers: [{visibility: 'off'}]             
              }                                            
        ];                                                 
                                                           
        this.map.setOptions({styles:myStyle});

        let title=LangTools.filterHebrew(this.state.locationName);
        let marker = new window.google.maps.Marker({
          position: {lat: lat, lng: lng},
          map: this.map,
          title: title,
          label:{text:title,fontSize:"11px"}
        });

        
        let olat=0;
        let olng=0;
        for (let i=0;i<chapterLocs.length;i++){
          
          let loc=chapterLocs[i];

          title= new String(LangTools.filterHebrew(loc.Locations.title)).replace("?","");
          olat=parseInt(loc.Locations.latitude);
          olng=parseInt(loc.Locations.longitude);

          new window.google.maps.Marker({position: {lat:olat, lng:olng},map: this.map,
            label:{text:title,fontSize:"11px"},title:title
          });

        }

        })();


        //document.querySelector('.location-box .para-quote').innerHTML='"'+this.props.para.innerHTML+'"';
        

    }

    



    closeBox=()=>{
        
        this.ctx.elementsHandler.removeElement(this.props.elementKey);
    }

    

    render(){

        //console.log("this.props.google",this.props.google);

        const mapStyles = {
          width: '90%',
          height: '250px',
        };

        const periods=this.props.locationObj.periods=="" ? "" : this.props.locationObj.periods.replace(/;/g,", ");

        return (
            <div className="location-box capish-common-box">
                <div className="close-icon" onClick={this.closeBox}><i className="fas fa-times"/></div>

                <div className="title"><img src={pinHoverImg} className='pinIcon' />מקום</div>
                
                <div className="body-container">

                  <div className='location-title'>{this.props.locationObj.title}</div>

                  <div className='periods-title'>מוזכרת בתקופות:</div>
                  <div className='periods'>{periods}</div>

                  {this.state.body!=="" && <div className='few-words-title'>כמה מילים:</div>}
                  {this.state.body!=="" && <div className='few-words'>{this.state.body}</div>}
                  
                      
                  <div className='map-container' id="map"></div>

                   
                </div>
            </div>
        );
    }
}
/*
export default GoogleApiWrapper({
  apiKey: 'AIzaSyC0Gutcwz_6R0ypn1MSV5noB3gqBtMibXo',language:'en'
})(LocationBox)
*/
import React, { Component } from 'react';
import capishLogoImg from './../img/capish-logo-yougotit.gif';
import hertzogImg from './../img/hertzog-tanach.gif';
import { observer,inject } from 'mobx-react';
import Auth from './../modules/auth/Auth';
import './TopNavBar.scss';
//import {UserModal} from './modals/UserModal';
import Consts from './../constants/Consts';
import LoadDocumentModal from './modals/LoadDocumentModal';
//import ToggleButton from 'react-toggle-button';
import editPenIcon from './../img/edit-pen.png';
import NoteBoxModal from './modals/NoteBoxModal';


class TopNavBar extends Component{

  constructor(props){
    super(props);
    this.ctx=this.props.ctx;
    //this.userName=Auth.getUserId();
    this.state={editTitle:false,form:{titleName:""}};
  }


  closeLoadDocumentModal=()=>{
      this.ctx.elementsHandler.removeElement(Consts.ELEMENTS_LOAD_DOCUMENT_MODAL);
  }

  launchUserModal=()=>{

    this.ctx.elementsHandler.addElement(Consts.ELEMENTS_LOAD_DOCUMENT_MODAL,<LoadDocumentModal key={Consts.ELEMENTS_LOAD_DOCUMENT_MODAL} onLoadDocument={this.loadDocument} ctx={this.props.ctx} closeModal={this.closeLoadDocumentModal}/>);
    
  }

  changeDocumentName=(e)=>{
      
      this.props.TextStore.documentName=e.target.value;

    }

  saveDocument=async()=>{
      

     let text=this.props.TextStore.text;

     if (this.props.TextStore.documentName==""){
       alert("לא ניתן לשמור מסמך ללא כותרת");
       return;
     }

     this.setState({editTitle:false});

     //console.log("text?!",text);

     let document={
       fromParaId:text.paras[0].id,
       toParaId:text.paras[text.paras.length-1].id,
       title:this.props.TextStore.documentName
     };

     console.log("document to save",document);
     //let [err,res] = await AsyncTools.superFetch('/api/);
     
     let [docRes,err]=await Auth.superAuthFetch('/api/Documents/saveDocument',
       {   headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
       method:'POST',
       body:JSON.stringify( {docData:document})   });
     

    }

    toggleEditMode=()=>{

          
          if (!this.props.TextStore.isCustomized) {
            this.ctx.elementsHandler.addElement(Consts.ELEMENTS_NOTE_BOX,<NoteBoxModal elementsHandler={this.ctx.elementsHandler} key={Consts.ELEMENTS_NOTE_BOX} title={'על מנת לערוך את המסמך, עליך לשייך את המסמך אליך קודם לכן'} />)
            return;
          }

          this.setState({editTitle:!this.state.editTitle});

      
    }


  render(){

      let titleEditor=null;
      const documentName=this.props.TextStore.documentName;
      
      if (this.state.editTitle && this.props.TextStore.isCustomized){

        titleEditor=<span className='document-title edited'>
        <input type="text" value={documentName} onChange={this.changeDocumentName} onBlur={this.saveDocument} name="titleName" />
        <i className="far fa-save" onClick={this.saveDocument}/>
        </span>;

      }else{
        titleEditor=<span className='document-title'>
          <img className="edit-pen-icon" onClick={this.toggleEditMode} src={editPenIcon} />{documentName}
        </span>;
      }

    


    return    (
    <nav className="navbar navbar-default">
        <div className="container">    
            
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              
            </div>

            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

              <ul className="nav navbar-nav">
                <li className="dropdown">
                  <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">מיכל אופיר<span className="caret"></span></a>
                  <ul className="dropdown-menu">
                    <li><a href="#" onClick={this.launchUserModal}>דפי העבודה שלי</a></li>
                    <li><a href="/personal-area">איזור אישי</a></li>
                    <li role="separator" className="divider"></li>
                    <li><a href="#">התנתק</a></li>
                  </ul>
                </li>
              
              </ul>

              {titleEditor}
              
              <ul className="nav navbar-nav navbar-right brands">            
                <li className="hertzog-logo"><a className="navbar-brand" href="#"><img src={hertzogImg} /></a></li>
                <li className="capish-logo"><a className="navbar-brand" href="/"><img src={capishLogoImg} /></a></li>
              </ul>
            </div>
        </div>
    </nav>
    );


    }
}

export default inject('TextStore')(observer(TopNavBar));
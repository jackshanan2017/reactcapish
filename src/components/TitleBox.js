import React, { Component } from 'react';
import Auth from './../modules/auth/Auth';
//import {observable, autorun, action} from "mobx";
import { observe,observable,autorun,intercept,when } from 'mobx';
import editPenIcon from './../img/edit-pen.png';

export class TitleBox extends Component{

    constructor(props){
        super(props);
        this.ctx=this.props.ctx;
        this.state={title:"",mouseOver:false,userEditMode:false,editMode:false};
        this.ref=React.createRef();
        let userData=Auth.getUserDetails();
        //this.state={userFirstLetter:userData['userName'][0].toUpperCase()};
        this.state={userFirstLetter:'i'};

    }


    componentDidMount(){
                
        this.ref.current.addEventListener("mouseover",()=>{
            if (this.state.mouseOver===true) return;
            this.setState({mouseOver:true});
        });

        this.ref.current.addEventListener("mouseleave",()=>{
            if (this.state.mouseOver===false) return;
            this.setState({mouseOver:false});
        });

        
        
    }

    saveTitle=async()=>{

        console.log("save title is launched with this.props.qObj",this.props.qObj);
        
        if (this.state.title.trim()==""){
            alert("לא ניתן לשמור כותרת ריקה");
            return;
        }
        
        let titleData={...titleData,title:this.state.title,noteType:"title",from_para_id:this.props.aboveParaId};

        if (this.props.qObj.id){
            
            console.log("Updating existing title");
            titleData.notemapId=this.props.qObj.id;
            titleData.noteId=this.props.qObj.title_id;
            titleData.isExisting=true;

        }else{

            console.log("Creating a new title");
            titleData.isExisting=false;
        }    

        console.log("titleData",titleData);

        let [titleRes,err]=await Auth.superAuthFetch('/api/notes/saveNote',{headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },method:'POST',body:JSON.stringify( {noteData:titleData})});

        if (err){
            console.log("Could not save title, aborting with err",err);
            return;
        }
        
        console.log("titleRes~~~~~~~",titleRes);

        titleData['title_id']=titleRes.noteId;
        titleData.id=titleRes.noteId;

        console.log("title data last",titleData);
        
        if (titleData.isExisting){
            this.props.TextStore.replaceNoteById('titles',titleData,titleData.id);
            this.setState({userEditMode:false});

        }else{
            this.props.TextStore.replaceNewNote('titles',titleData);    
        }

        

        //this.props.onClose();

    }

    setEditTitle=()=>{
        this.setState({userEditMode:true,title:this.singleTitle},()=>{
            setTimeout(()=>{document.querySelector('.title-input').focus();},100);
            
        });
    }

    changeTitle=(e)=>{
        
        this.setState({title:e.target.value});
        //this.state.form[name]=e.target.value;
    }

    cancelEdit=()=>{

        if (this.props.qObj.id){
            this.setState({userEditMode:false});
            return;
        }

        //let child=this.ref.current;
        //child.parentNode.removeChild(child);
        this.props.TextStore.removeNewNote('titles');

    }

    removeTitle=async(e)=>{

        console.log("removeTitle is launched");
        
        try{
            let titleId=this.props.qObj["title_id"];
            let [res,err]=await Auth.superAuthFetch('/api/Notesmaps/removeNote?noteType=title&noteId='+titleId);
            
            this.props.TextStore.removeNoteById('titles',titleId);

        }catch(err){
            console.log("err",err);
        }
        
    }
    
    
    render(){

        let editDisabled=false;
        this.singleTitle=this.props.qObj.title;
        if (this.props.qObj.sub_title && this.props.qObj.sub_title !==null){
            this.singleTitle=this.props.qObj.title+", "+this.props.qObj.sub_title;
        }

        //console.log("qObj?",this.props.qObj);
        
        //console.log("this.props.qObj",this.props.qObj);
        let isEditMode=!this.props.qObj.id || this.state.userEditMode;

        //Disabling edit mode for generic titles!
        if (this.props.qObj.is_generic && this.props.qObj.is_generic==1) {editDisabled=true};

        //console.log("this.props.qObj.id?",this.props.qObj.id);
        //console.log("isEditMode?",isEditMode);
        
        if (isEditMode){
            return (

                <div className="title-box" ref={this.ref}>
                    <div className='title-hover'>
                        <input className="title-input" type="text" onChange={this.changeTitle} placeholder="הקלד כותרת" value={this.state.title || ""} />
                        <span className='user-circle'>{this.state.userFirstLetter}</span>
                        <i className="fas fa-check" onClick={this.saveTitle} / >
                        <i className="fas fa-times" onClick={this.cancelEdit} />
                    </div>                
            </div>

            )
        }


        return (

            <div className="title-box" ref={this.ref} >

            {editDisabled || !this.state.mouseOver || !this.props.TextStore.isCustomized ? 

                <div className="title">{this.singleTitle}
                <span className='user-circle'>{this.state.userFirstLetter}</span>
                </div> 

                : 

                <div className='title-hover'>
                    <div className="title">{this.singleTitle}</div>
                    <img className='icon edit-pen-icon' src={editPenIcon} onClick={this.setEditTitle} />
                    <i className='far fa-trash-alt' onClick={this.removeTitle} />
                    <span className='user-circle'>{this.state.userFirstLetter}</span>
                </div>}

            </div>
        );
    }
}
import React, { Component } from 'react';
import AsyncTools from './../modules/tools/AsyncTools';
import Auth from './../modules/auth/Auth';
import spinner from './../img/spinner.svg';
import LangTools from './../modules/tools/LangTools';
//import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

export default class PersonBox extends Component{

    constructor(props){
        super(props);
        this.ctx=this.props.ctx;
        
        this.state={
            location:this.props.locationObj,
            personName:this.props.locationObj.term,
            description:this.props.locationObj.description || "",
        }

        console.log("locationObj",this.state.location);


    }
    
    componentDidMount(){

        document.querySelector('.person-box .para-quote').innerHTML='"'+this.props.para.innerHTML+'"';
        

    }

    



    closeBox=()=>{
        
        this.ctx.elementsHandler.removeElement(this.props.elementKey);
    }

    

    render(){

        //console.log("this.props.google",this.props.google);

        const mapStyles = {
          width: '90%',
          height: '250px',


        };

        return (
            <div className="person-box capish-common-box">
              
              <div className="title">דמויות: {this.state.personName}</div>
                
              <div className="body-container">

                <div className="para-quote"></div>
                      
                <div className="description">{this.state.description}</div>

                <div className="buttons-container">
                        <div className="save-btn btn" onClick={this.closeBox}>סגירה</div>
                </div>

              </div>
                
                
            </div>
        );
    }
}
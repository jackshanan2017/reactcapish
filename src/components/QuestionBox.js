import React, { Component } from 'react';
import CommentsHandler from './../handlers/CommentsHandler';
import AsyncTools from './../modules/tools/AsyncTools';
import Auth from './../modules/auth/Auth';
import {Modal} from 'react-bootstrap';
import Consts from './../constants/Consts';
import { observer,inject } from 'mobx-react';
import qIconHover from './../img/question-icon-hover.png';

const classNames=require('classnames');
const aa=["thirdAnswer","fourthAnswer","fifthAnswer","sixthAnswer"];
const QUESTION_MENU=0;
const OPEN_QUESTION_FORM=1;
const AMERICAN_QUESTION_FORM=2;

class QuestionBox extends Component{

    constructor(props){
        super(props);
        this.ctx=this.props.ctx;
        
        //console.log("QuestionBox this.props.sObj?",this.props.sObj);

        this.state={
            editTitle:'הוספת שאלה',
            editQuestionBox:false,   
            question:this.props.qObj.question,
            firstAnswer:this.props.qObj.firstAnswer,
            secondAnswer:this.props.qObj.secondAnswer,
            openedQuestion:this.props.qObj.openedQuestion,
            aa:[],
            isOpened:this.props.qObj.isOpened,            
            bodyView:QUESTION_MENU,            
            checkboxes:{firstAnswer:false,secondAnswer:false,thirdAnswer:false,fourthAnswer:false,fifthAnswer:false,sixthAnswer:false},
            showPlusButton:true,
            form:{
                question:"",firstAnswer:"",
                secondAnswer:"",
                additionalAnswers:[],
                rightAnswer:null,
                openedAnswer:"",
                openedQuestion:""
            },
            line:this.props.sObj.sText
        };

        //console.log("THIS.props.qObj.ID??",this.props.qObj.id);
        aa.forEach((a,k)=>{if (this.props.qObj[a]) this.state.aa.push(this.props.qObj[a]);})

    }

     //static getDerivedStateFromProps(nextProps, prevState){
         //return {}
     //}
     
     //componentDidUpdate(prevProps,prevState){
         //console.log("new props?",this.props);    
         //console.log("prevProps.qObj.question",prevProps.qObj.question);
         //console.log("this.props.qObj.question",this.props.qObj.question);
     //}


    onClose=()=>{

        if (this.props.qObj.id){
            this.setState({editQuestionBox:false});
        }else{
            this.props.TextStore.removeNewNote('questions');    
        }
        
        this.props.textHolder.isContextMenuEnabled=true;
        //this.ctx.setState({cMenu:{display:'none'}});
    }


    saveQuestion=async()=>{
        
        //console.log("Save question is launched");

        let isOpened=this.state.bodyView==OPEN_QUESTION_FORM || this.state.isOpened;

        //console.log("isOpened?",(isOpened ? "TRUE" : "FALSE"));

        if (isOpened){

            if (this.state.form.openedQuestion.trim()=="" || this.state.form.openedAnswer.trim()==""){
                alert("כדי לשמור את השאלה יש להזמין תשובה ושאלה");
                return;
            }

        }else{

            if (this.state.form.question.trim()=="" || this.state.form.firstAnswer.trim()=="" || this.state.form.secondAnswer.trim()==""){
                alert("כדי לשמור את השאלה יש להזמין לפחות שאלה אחת ושתי תשובות שונות");
                return;
            }
        }
        
        let noteData={};

        if (isOpened){

            noteData={
                openedQuestion:this.state.form.openedQuestion,
                openedAnswer:this.state.form.openedAnswer
            };

        }else{

            noteData={
                question:this.state.form.question,
                firstAnswer:this.state.form.firstAnswer,
                secondAnswer:this.state.form.secondAnswer,
                rightAnswer:this.state.form.rightAnswer
            };
            aa.forEach(a=>{if (this.state.form[a]) noteData[a]=this.state.form[a];})
        }

        noteData.noteType ="question";
        noteData.isOpened = isOpened ? 1 : 0;
        
        let sObj=this.props.sObj;
        let sText=this.props.sObj.sText;

        
        if (this.props.qObj.id){
            noteData.last_para_id=this.props.qObj.last_para_id;
            noteData.notemapId=this.props.qObj.id;
            noteData.noteId=this.props.qObj.question_id;
            noteData.isExisting=true;

        }else{
            noteData.isExisting=false;    
            noteData.last_para_id=this.props.TextSelectorParser.lastParaId;
            if (noteData.last_para_id==null){
                console.log("last para id is null");
                return;
            }
        }    
        
        console.log("noteData before save",noteData);
        
        let [commentRes,err]=await Auth.superAuthFetch('/api/notes/saveNote',{   headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },method:'POST',body:JSON.stringify( {noteData}) });

        if (err){
            console.log("Could not save question, aborting with err",err);
            return;
        }

        console.log("commentRes?!",commentRes);

        if (noteData.noteId){
            noteData.id=noteData.noteId;
        }else{
            noteData.id=commentRes.noteId;
            noteData.question_id=commentRes.noteId;    
        }
        
        
        if (noteData.isExisting)
        {
            console.log("Replace note by id ",noteData.id);
            this.props.TextStore.replaceNoteById('questions',noteData,noteData.id);
            this.setState({editQuestionBox:false,...noteData});

        }else{
            console.log("noteData for question box for replaceNewNote",noteData);
            this.props.TextStore.replaceNewNote('questions',noteData);            
            this.setState({editQuestionBox:false,...noteData});
        }
        
        this.props.textHolder.isContextMenuEnabled=true;
       
    }

    addAnswerBox=()=>{
        //console.log("addAnswerBox is launched");
        let aAnswers=this.state.form.additionalAnswers;
        let iName="";
        if (aAnswers.length>3){
            this.setState({showPlusButton:false});
            return;
        }        
        //let aa=this.aa;
        iName=aa[aAnswers.length];
        //console.log("aAnswers.length?",aAnswers.length);
        //console.log("iName?",iName);
        aAnswers.push({name:iName});
        //console.log("aAnswers?",aAnswers);
        this.setState({additionalAnswers:[...aAnswers]});


    }

    removeQuestion=async(e)=>{

        console.log("removeQuestion is launched");
        let content=e.target.innerHTML;
        let [res,err]=await Auth.superAuthFetch('/api/Notesmaps/removeNote?noteType=question&noteId='+this.props.qObj.id);
        try{
            let child=document.querySelector("[question-data-index='"+this.props.qObj.id+"']");
            child.parentNode.removeChild(child);
        }catch(err){
            console.log("err",err);
        }
        this.removeConfirmBox();
        //this.closeBox();
    }

    removeConfirmBox=()=>{
        this.ctx.elementsHandler.removeElement(Consts.CONFIRM_ELEMENT_BOX);        
        
    }

    launchDeleteQuestionConfirmBox=()=>{
        
        
        let ConfirmDeletePopup=()=>{
            return(
                <Modal className='capish-modal confirm-box' show={true}>
                  <Modal.Header className='modal-header'></Modal.Header>
                  <Modal.Body>
                      <div className='confirm-box-message'>האם להסיר את השאלה?</div>
                  </Modal.Body>
                  <Modal.Footer className='btns-row'>
                    
                    <div className='col-xs-6'>
                        <button variant="primary" className='capish-btn' onClick={this.removeQuestion}>
                          אישור
                        </button>
                    </div>
                    <div className='col-xs-6'>
                    <button variant="primary" className='capish-btn' onClick={this.removeConfirmBox}>
                      ביטול
                    </button>
                    </div>                    
                  </Modal.Footer>
                </Modal>
            );
        }
        

        this.ctx.elementsHandler.addElement(Consts.CONFIRM_ELEMENT_BOX,
            <ConfirmDeletePopup 
            key={Consts.CONFIRM_ELEMENT_BOX}
            />);
    }

    handleChange=(name,e)=>{
        //this.setState({form[name]:e.target.value});
        let f={...this.state.form};
        f[name]=e.target.value;
        this.setState({form:f});
        //this.state.form[name]=e.target.value;
    }

    isCheckbox=(val)=>{

    }

    handleCheckBoxes=(e)=>{

        let rightAnswer=null;
        console.log("handle check boxes with e.target.checked",e.target.checked);
        let cb=this.state.checkboxes;
        Object.keys(cb).forEach((k)=>{
            if (k==e.target.value) return;
            cb[k]=false;
        });
        cb[e.target.value]=!cb[e.target.value];
        Object.keys(cb).forEach((k)=>{
            if (cb[k]){
                rightAnswer=k;
            }
        });
        let form=this.state.form;
        form.rightAnswer=rightAnswer;
        //this.setState({checkboxes:cb,form});
        //console.log("FORM?",form);
    }

    editQuestionBox=()=>{
        let form=this.state.form;
        //form:{question:"",firstAnswer:"",secondAnswer:"",additionalAnswers:[],rightAnswer:null},
        form.question=this.props.qObj.question;
        form.firstAnswer=this.props.qObj.firstAnswer;
        form.secondAnswer=this.props.qObj.secondAnswer;
        aa.forEach((aa,k)=>{
            if (form.additionalAnswers[aa]){
                form[aa]=form.additionalAnswers[aa];
            }
        }); 
        this.setState({editQuestionBox:true,form,editTitle:'עריכת שאלה'});
    }

    switchBodyView=()=>{
        
        switch (this.state.bodyView){
            case QUESTION_MENU:
                return this.showQuestionMenu();
            break;
            case OPEN_QUESTION_FORM:
                return this.showOpenQuestionForm();
            break;
            case AMERICAN_QUESTION_FORM:
                return this.showAmericanQuestionForm();
            break;
        }
    }

    showQuestionMenu=()=>{

        return (
            <div>
                <div className="body q-type">
                    <div className='q-title'>בחר את סוג השאלה</div>
                        <div className='q-type-btn' onClick={()=>this.setState({bodyView:OPEN_QUESTION_FORM})}>שאלה פתוחה</div>
                        <div className='q-type-btn' onClick={()=>this.setState({bodyView:AMERICAN_QUESTION_FORM})}>שאלה אמריקאית</div>
                 </div>
            
                <div className="buttons-container">
                    <div className="save-btn btn" onClick={this.onClose}>ביטול</div>
                </div>

            </div>);
    }

    showOpenQuestionForm=()=>{

        return (<div>
        <div className="body open-q">
            <div className='question-row'>
                <input className="question" type="text" onChange={(e)=>this.handleChange("openedQuestion",e)} placeholder="הקלד שאלה" value={this.state.form.openedQuestion} />
            </div>
            <div className='answer-row'>
                <textarea className='open-answer' onChange={(e)=>this.handleChange("openedAnswer",e)} value={this.state.form.openedAnswer} placeholder='הקלד תשובה'/>
            </div>
        </div>

        <div className="buttons-container">
            <div className="save-btn btn" onClick={this.saveQuestion}>שמירה</div>
            <div className="save-btn btn" onClick={this.onClose}>ביטול</div>
        </div>

        </div>);

    }
    showAmericanQuestionForm=()=>{
        return (<div>

                <div className="body">
                    <div className='question-row'>
                        <input className="question" type="text" onChange={(e)=>this.handleChange("question",e)} placeholder="הקלד שאלה" value={this.state.form.question} />
                    </div>
                    <div className='answer-row'>
                        <input type='checkbox' checked={this.state.checkboxes['firstAnswer']} onChange={this.handleCheckBoxes} className='fas fa-check' name='isRight' value='firstAnswer' />
                        <input type="text" className='answer-box' onChange={(e)=>this.handleChange("firstAnswer",e)} placeholder="הקלד תשובה" value={this.state.form.firstAnswer} />
                    </div>
                    
                    <div className='answer-row'>
                        <input type='checkbox' checked={this.state.checkboxes['secondAnswer']} onChange={this.handleCheckBoxes} className='fas fa-check' name='isRight' value='secondAnswer' />
                        <input type="text" className='answer-box' onChange={(e)=>this.handleChange("secondAnswer",e)} placeholder="הקלד תשובה" value={this.state.form.secondAnswer} />
                    </div>

                     {this.state.form.additionalAnswers.map((aa,k)=>                             
                         <div className='answer-row' key={k}>
                             <input type='checkbox' className='fas fa-check' checked={this.state.checkboxes[aa.name]} onChange={this.handleCheckBoxes} name='isRight' value={aa.name} />
                             <input type="text" className='answer-box' name={aa.name} onChange={(e)=>this.handleChange(aa.name,e)} placeholder="הקלד תשובה" value={this.state.form[aa.name] || ""} />
                         </div>

                      )}

                    <div className={this.state.showPlusButton===true ? '' :'hide'}>                        
                    <i className="add-answer fas fa-plus-circle fa-2x" onClick={this.addAnswerBox}/>
                    </div>
                </div>

                <div className="buttons-container">
                    <div className="save-btn btn" onClick={this.saveQuestion}>שמירה</div>
                    <div className="save-btn btn" onClick={this.onClose}>ביטול</div>
                </div>

                </div>);
    }
    
    
    render(){

        let editQuestionBox= !this.props.qObj.id || this.state.editQuestionBox;
        let isOpened = this.state.isOpened;

        if (editQuestionBox){

            return(
            <div className="new-question-box">
                <div className="title"><img src={qIconHover} className='edit-q-icon' />{this.state.editTitle}</div>
                <div className="body-container">                    
                    {this.switchBodyView()}
                </div>
            </div>
        );

        }

        


        let aAnswers=[];
        aa.forEach((aa,k)=>{if (this.props.qObj[aa]) aAnswers.push(<li key={k}><input type='checkbox' />{this.props.qObj[aa]}</li>);});
        
        
        if (isOpened){
            return(
                <div className="question-box view-mode opened-question" question-data-index={this.props.qObj.id}>
                <i className="far fa-trash-alt" onClick={this.launchDeleteQuestionConfirmBox}/>
                <i className="far fa-edit" onClick={this.editQuestionBox}/>
                <div className="opened-q-title">רגע לפני שממשיכים לקרוא, בואו נראה מה הבנו עד כה</div>
                <div className="opened-question">{this.state.openedQuestion}</div>
                <div className="opened-answer"><textarea placeholder='הקלד תשובה'/></div>
            </div>);
        }else{

            return (
            <div className="question-box" question-data-index={this.props.qObj.id}>
                <i className="far fa-trash-alt" onClick={this.launchDeleteQuestionConfirmBox}/>
                <i className="far fa-edit" onClick={this.editQuestionBox}/>
                <div className="question">{this.state.question}</div>
                <ul className="answers-list">
                    <li><input type='checkbox' />{this.state.firstAnswer}</li>
                    <li><input type='checkbox' />{this.state.secondAnswer}</li>
                    {aAnswers}
                </ul>
            </div>

        );

        }

        
    }
}

export default inject('TextSelectorParser')(observer(QuestionBox));
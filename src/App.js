/*
capish.office@gmail.com
E2PSzAmJ-5-ldKnl
*/

import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Redirect } from 'react-router';
import Login from './modules/auth/Login';
import Auth from './modules/auth/Auth';
import DashboardHomeIndex from './modules/dashboard/dashboard-home';
import { observer,inject } from 'mobx-react';
import { extendObservable } from 'mobx';
import Home from './scenes/Home';
import StudyPage from './scenes/StudyPage';
import PersonalArea from './scenes/PersonalArea';
import PrivateRoute from './modules/auth/PrivateRoute';
import ImageUploaderView from './modules/samples/ImageUploaderView';


function waitFor(t){
    return new Promise((res,rej)=>{
        setTimeout(()=>{res()},t);
    })
}

class App extends Component {

    constructor(props) {
        super(props);
        this.state={isAuth:false};
        
    }

    async componentDidMount(){
        
        this.isAuth=await Auth.isAuthenticated();
        this.setState({isAuth:true});
        
    }

    render() {
        //console.log("hello mobx",this.props.ExampleStore.first); //an example to access mobx!

        if (!this.state.isAuth){
            return(
            <Router>
                <div className="App">
                    <Route exact path="/" component={Home} />                    
                </div>
            </Router>
            );
        }
        
        return(
            <Router>
                <div className="App">
                    
                    {/* <PrivateRoute path="/admin" Redirect={()=><Redirect to='/admin/home' />}/>  */}
                    <Route exact path="/" component={Home} />                                
                    <PrivateRoute exact path="/study-page" component={StudyPage} />                    
                    <PrivateRoute exact path="/personal-area" component={PersonalArea} />                    
                    <PrivateRoute path="/admin" component={DashboardHomeIndex} />
                    <Route path='/image-uploader' component={ImageUploaderView} />   
                </div>
            </Router>
        );
    }
}
export default inject('TextStore')(observer(App));
import validate from 'validate.js';

const ValTools={
    
    deepTest(obj,path,funcArr){
    
    let val=validate.getDeepObjectValue(obj, path);
    
    if (!val){ return ["Value doesn't exist with path ("+path+")",null];}
    
    for (let i=0;i<funcArr.length;i++){
        
        let f=funcArr[i];
        
        if (f==='isNotEmpty') {
            if (validate.isEmpty(val)){
                
                return ['Value is empty',null];
            }
            continue;
        }

        //console.log("f["+i+"]?",f(val));

        if (!f(val)) {return ['Failed',null];}
    }

    return [null,val];

    
    }
}
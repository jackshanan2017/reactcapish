//node -r esm server/GenerateSefariaDownloadList.js
'use strict';

import BibleIndex from './../src/handlers/BibleIndex';


class GenerateSefariaDownloadList{

    
    
    constructor(){
        
        let sefariaUrl="https://www.sefaria.org/api/texts/{CHAPTER_STRING}?commentary=1";
        this.bibleIndex=new BibleIndex();
        let list=this.bibleIndex.getChaptersStringForSefaria();
        list.forEach(l=>{
            console.log("wget "+sefariaUrl.replace("{CHAPTER_STRING}",l)+" -O "+l.toLowerCase()+".json");
        });


    
    }

    
    

}


(async()=>{


new GenerateSefariaDownloadList();


})();


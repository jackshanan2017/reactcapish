module.exports = {

    isObject(item) {
        return (item && typeof item === 'object' && !Array.isArray(item));
    },

    mergeDeep(target, source) { // this function merges objects. it also merges the sub-objects.
        let output = Object.assign({}, target);
        if (this.isObject(target) && this.isObject(source)) {
            Object.keys(source).forEach(key => {
                if (this.isObject(source[key])) {
                    if (!(key in target))
                        Object.assign(output, { [key]: source[key] });
                    else
                        output[key] = this.mergeDeep(target[key], source[key]);
                } else {
                    Object.assign(output, { [key]: source[key] });
                }
            });
        }
        return output;
    },
    generatePassword(length = 8) {
        var charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    },

    deleteUserId(object) {
        if (this.isObject(object))
            Object.keys(object).forEach(item => {
                if (item == "CustomUser") {
                    object.CustomUser.id = null;
                }
                else {
                    ['owner', 'ownerId', 'playerId', 'commentorId'].forEach(field => {
                        item === field ? object[item] = null : null;
                    })
                    if (this.isObject(object[item])) {
                        item = this.deleteUserId(object[item], true);

                    }
                }
            })

        return object;
    }
}
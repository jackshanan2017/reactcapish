//node -r esm server/import-locations-data.js
'use strict';

import BibleIndex from './../src/handlers/BibleIndex';
import AsyncTools from './../src/modules/tools/AsyncTools';
import axios from 'axios';
import _ from 'lodash';

const csv = require('csv-parser');
const fs = require('fs');
const loopback = require('loopback');
const boot     = require('loopback-boot');
const path     = require('path');
const app      = module.exports = loopback();


app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
require('./routes')(app);

const waitFor = (ms) => {
        return new Promise((resolve, reject) => {
            setTimeout(() => { resolve(ms) }, ms)
        })
    }

class TitlesImporter{
  
  constructor(){

    this.newRow={};
    this.results=[];
    this.ref={};
    this.bibleIndex=new BibleIndex();

  }

  

  async import(){

    try{
      await this.readCSV();
      await this.recordToDB();

    }catch(err){
      console.log("import err",err);
    }



    //console.log("paragraphs?",app.models.Paragraphs);



  }

  async recordToDB(){

    let noResults=[];
    let defer=AsyncTools.defer();

    try{

    

    if (this.results.length==0){
      console.log("No data to record to db");
      defer.resolve();
      return defer;
    }

    
    await app.models.Notesmap.destroyAll({userId:1,noteType:'title'});
    await app.models.Titles.destroyAll({isGeneric:1});

    for(let i=0;i<this.results.length;i++){

        let row=this.results[i];
        console.log("Row",row);

        //let paraData={'and':[{book:row.fromBookEn,chapter:row.fromChapterNum,paraNum:row.fromParaNum}]};
        let paraData={book:row.fromBookEn,chapter:row.fromChapterNum,paraNum:row.fromParaNum};
        
        //console.log("paraData",paraData);
        let paraRes=await app.models.Paragraphs.findOne({where:paraData});

        //console.log("paraRes",paraRes);

        
        if (paraRes==null){
            console.log("No results for paraData",paraData);
            //await waitFor(100);
            row.err="No corresponding paragraph in db";
            noResults.push(row);
            continue;
        }

        if (!row.title){
            row.err="No title for this record";
            noResults.push(row);
            continue;
        }

        let titleData={title:row.title,isGeneric:1};
        if (row.subTitle!=''){titleData.subTitle=row.subTitle};

        let titleRes=await app.models.Titles.create(titleData);

        console.log("titleRes",titleRes);

        if (!titleRes.id){
            throw new Error("could not create a new entry for title with data?",titleData);
        }

        let nMapData={fromParaId:paraRes.id,titleId:titleRes.id,userId:1,noteType:'title'};

        let nMapRes=await app.models.Notesmap.create(nMapData);

        if (!nMapRes.id){
            throw new Error("could not create a new entry for NotesMap with data?",nMapData);
        }
        


        //if (i>1) break;
        //let noteData={title:row.title}
    }
/*    
    for(let i=0;i<this.results.length;i++){

      let row=this.results[i];
      //console.log("\n\nSaving row",row);
      
      if (row.refs.length==0){
        console.log("Row index (%d) has no refs, next");
        continue;
      }

      //for (let refIndex=0;refIndex<row.refs.length;refIndex++){

        


        let q={where:{book:ref.book,chapter:ref.chapter,paraNum:ref.paraNum}};
        console.log("app.models.Paragraphs.findOne with q",q);
        
        let refRes={};
        try{
          
          refRes=await app.models.Paragraphs.findOne(q);

        }catch(refErr){

            console.log("Paragraphs query err",err);

        }

        console.log("refRes",refRes);

        if (refRes==null){
          console.log("No para index, next");
          continue;
        }



        let keys=['title','Latitude','Longitude','Periods','fewWords','body','alsoCalled'];
        let locationData={};
        
        keys.map(k=>{
          if (row[k]){
            if (k=='fewWords' || k=='alsoCalled' ){
              locationData[k]=row[k];
            }else{
              locationData[k.toLowerCase()]=row[k];  
            }
            
          }  
        });

        //locationData.alsoCalled=await this.generateLocationKeywords(locationData);

        console.log("locationData",locationData);
        //await new Promise((res,rej)=>{setTimeout(()=>{  res();  },1000  )})   
        

        let locationRes=await app.models.Locations.findOrCreate({where:{title:row.title}},locationData);
        //console.log("locationRes?",locationRes);

        if (locationRes.length==0 || !locationRes[0].id){
          console.log("Could not create new record on Location model, next");
          continue;
        }


        let notesMapData={lastParaId:refRes.id,noteType:'location',locationId:locationRes[0].id};
        //console.log("NOTES MAP DATA?",notesMapData);
        let notesMapRes=await app.models.Notesmap.findOrCreate({where:notesMapData},notesMapData);

        console.log("Created new NotesMap with id",notesMapRes[0].id);
        

      //}





      //if (i>3) break;



    }*/

    }catch(err){
        console.log("err",err);
    }    
    
    console.log("no results",noResults);
    console.log("num of no results",noResults.length);
    defer.resolve();
    return defer;
  }


  async readCSV(){

    let defer=AsyncTools.defer();

    console.log("LocationsImport.import launched");
    fs.createReadStream(path.join(__dirname,'./../scripts/csv/all_titles.csv'))

    .pipe(csv())

    .on('data', (data) => {
        
        let enBook=null;
        
        if (data.FromBook){
            console.log("ToBook",data.ToBook);
            enBook=this.bibleIndex.getEnglishBookName(data.FromBook,'he');
            console.log("enBook",enBook);
            if (enBook!==null){
                    data.fromBookEn=enBook;
            }

        }

        if (data.FromChapter){
            data.fromChapterNum=this.bibleIndex.heNumToIndex(data.FromChapter);
        }
        if (data.FromPara){
            data.fromParaNum=this.bibleIndex.heNumToIndex(data.FromPara);
        }

        data.subTitle=data.Title3 ? data.Title2+", "+data.Title3 : data.Title2;
        data.title=data.Title1;

        delete data.Title1;
        delete data.Title2;
        delete data.Title3;
        
        delete data.ToPara;
        delete data.ToChapter;
        delete data.ToBook;
        
        if (data.fromBookEn)
               this.results.push(data);

        //this.newRow={};
        enBook=null;
    }

        )
    .on('end', (e) => {

        //console.log("END with e",e);
        console.log("results",JSON.stringify(this.results,null,'\t'));
        //console.log("Specifics",specifics);
        //console.log("this.results length",this.results.length);
        defer.resolve();
      
    });

    return defer;
  

  }


}



(async()=>{


await boot(app, __dirname);

//const Paragraphs=app.models.Paragraphs;

await (new TitlesImporter).import();

process.exit(0);

})();

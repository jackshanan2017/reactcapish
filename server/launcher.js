'use sctrict';

//const BibleIndex=require('./../src/handlers/BibleIndex');
import BibleIndex from './../src/handlers/BibleIndex';
import AsyncTools from './../src/modules/tools/AsyncTools';
//import Paragraphs from './../common/models/paragraphs';
import axios from 'axios';

const loopback = require('loopback');
const boot     = require('loopback-boot');
const path     = require('path');
const app      = module.exports = loopback();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
require('./routes')(app);


class SefariaImporter{
    
    constructor(){
        this.bibleIndex=new BibleIndex();
        //console.log("bookNameByIndex(0,he)?",this.bibleIndex.getBookNameByIndex(0,'he'));
        //console.log("bookNameByIndex(1,en)?",this.bibleIndex.getBookNameByIndex(1,'en'));
    }

    
    async beginRecursiveImport(book,chapter=1){

        if (book==null && chapter==null){
            book="Genesis";
            chapter=1;
        }
        //if (chapter==null){
            //chapter=1;
        //}

        let nextRef=await this.fetchSefaria(book,chapter);
        
        if (nextRef!==null){
            console.log("nextRef?",nextRef);
            let refs=nextRef.split(" ");
            //console.log("refs",refs);
            if (refs.length==2){
                book=refs[0];
                chapter=refs[1];
                await this.beginRecursiveImport(book,chapter);
            }
        }
        


    }

    async fetchSefaria(book=null,chapter=null){

        let url='https://www.sefaria.org/api/texts/'+book+'.'+chapter;
        let res=null;

        console.log("Fetching url",url);
        try{
            res=await axios.get(url);

        }catch(err){
            console.log("Sefaria fetch err",err);
        }

        
        await app.models.Paragraphs.saveParasToDB(res.data.he,book,chapter);

        console.log("res.data.next",res.data.next);
        return res.data.next;

    }

    async beginImport(){

        let bIndex=this.bibleIndex.getBibleIndex();
        //console.log("bIndex?",bIndex);
        //console.log("bIndex[Gensis]?",bIndex['Genesis']);

        let err=null,res=null;
        let book=null;

        console.log("bIndex.length?",bIndex.length);
        for (let bi=0;bi<Object.keys(bIndex).length;bi++){

        
            book=Object.keys(bIndex)[bi];
            
            //let book=bIndex[bi];

            
            await this.beginRecursiveImport(book);

            //if (bi>1) break;
            

        }

    }

}


(async()=>{


await boot(app, __dirname);

//const Paragraphs=app.models.Paragraphs;

(new SefariaImporter()).beginImport();


})();

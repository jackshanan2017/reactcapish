//node -r esm server/SefariaImporter.js
'use strict';

import BibleIndex from './../src/handlers/BibleIndex';
import AsyncTools from './../src/modules/tools/AsyncTools';
import axios from 'axios';

const util = require('util')
const loopback = require('loopback');
const boot     = require('loopback-boot');
const path     = require('path');
const app      = module.exports = loopback();
const validate = require('validate.js');
const fs = require("fs");
const http=require('http');
const hyperquest=require('hyperquest');
const dJSON = require('dirty-json');

var request = require('request')
  , JSONStream = require('JSONStream')
  , es = require('event-stream');

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
require('./routes')(app);

const SAVE_COMMENTARY = true;
const SAVE_PARAGRAPHS = false;

function waitFor(t){
    return new Promise((res,rej)=>{
        setTimeout(()=>{res()},t);
    })
}



function deepTest(obj,path,funcArr){
    
    let val=validate.getDeepObjectValue(obj, path);
    
    if (!val){ return ["Value doesn't exist with path ("+path+")",null];}
    
    for (let i=0;i<funcArr.length;i++){
        
        let f=funcArr[i];
        
        if (f==='isNotEmpty') {
            if (validate.isEmpty(val)){
                
                return ['Value is empty',null];
            }
            continue;
        }

        //console.log("f["+i+"]?",f(val));

        if (!f(val)) {return ['Failed',null];}
    }

    return [null,val];

    
}




class SefariaImporter{

    
    
    constructor(){
        this.bibleIndex=new BibleIndex();
        //this.books=['Genesis','Exodus','Leviticus','Numbers','Deuteronomy'];
        this.books=['Genesis'];
        this.aggIndex={};
        let bookHeName="";
        
        this.books.forEach(book=>{
            bookHeName=this.bibleIndex.getBookNameByLang(book,'he');
            this.aggIndex[book]={name:{he:bookHeName},chapters:[]};
        });

        //console.log("bookNameByIndex(0,he)?",this.bibleIndex.getBookNameByIndex(0,'he'));
        //console.log("bookNameByIndex(1,en)?",this.bibleIndex.getBookNameByIndex(1,'en'));
    }

    
    async beginRecursiveImport(book,chapter=1){

        if (book==null && chapter==null){
            book="Genesis";
            chapter=1;
        }

        //chapter=14;



        let nextRef=await this.fetchSefaria(book,chapter);
        
        if (nextRef!==null){
            
            console.log("nextRef?",nextRef);
            let refs=nextRef.split(" ");
            
            if (refs.length==2){
                book=refs[0];
                chapter=refs[1];
                await this.beginRecursiveImport(book,chapter);
            }
        }
        


    }

    async fetchSefaria(book=null,chapter=null){

        let res=null;

        //book='Genesis';
        //chapter=1;

        /*
        let url='https://www.sefaria.org/api/texts/'+book+'.'+chapter
        
        if (SAVE_COMMENTARY){url+='?commentary=1';  } 
        
        console.log("Fetching url",url);

        await new Promise((resolve,rej)=>{

            request({url:url},()=>{
                console.log("Done,Waiting 60 seconds..");
                setTimeout(()=>{
                        
                        resolve();
                },60000);
            })
            
            .pipe(fs.createWriteStream('output.txt'))    
            .on('response',(err,response,body)=>{
                console.log("Response ends, Streaming into output.txt...");
            })


        });
        */
        
        if (book.toLowerCase()=='genesis' && chapter==6) chapter=7;

        let sourceFile=path.resolve(__dirname,'../../../../capish/'+book.toLowerCase()+'.'+chapter+'.json');
        console.log("Reading from file",sourceFile);
        console.log("Is file exists?",fs.existsSync(sourceFile));
        let rawdata = fs.readFileSync(sourceFile);
        
        try{
            res = JSON.parse(rawdata);
        }catch(err){
            console.log("Error parsing json",err);
        }

        
        if (SAVE_COMMENTARY){
            await this.saveCommentary(res,book,chapter);    
        }
        
        if (SAVE_PARAGRAPHS){

            let numOfPars=res.he.length;
            this.aggIndex[book].chapters[chapter-1]=numOfPars;
            console.log("this.aggIndex?",this.aggIndex);
            await fs.writeFileSync(path.resolve(__dirname,'../src/constants/agg-index.json'),JSON.stringify(this.aggIndex,null,'\t'));
            await app.models.Paragraphs.saveParasToDB(res.he,book,chapter);
        }
        
        
        
        return res.next;

    }

    async saveCommentary(data,book,chapter){

        
        let [err,cs]=deepTest(data,'commentary',[validate.isArray,'isNotEmpty']);
        
        if (err) {
            console.log("deepTest err with",err);
            return;
        }

        console.log("Commentary exist with length",cs.length);

        for (let i=0;i<cs.length;i++){

            let com=cs[i];
            //console.log("Commentary",com);
            
            if (validate.isEmpty(com.anchorRef)){
                console.log("No anchorRef commentary (book:"+book+",chapter:"+chapter+")");
                continue;

            }else{
                //console.log("Able to save commentary (book:"+book+",chapter:"+chapter+") with ref",com.anchorRef);

            }

            let anchors=com.anchorRef.split(" ");
            let [_chapter,_paraNum]=anchors[1].split(":");
            _chapter=parseInt(_chapter);
            _paraNum=parseInt(_paraNum);
            let _book=anchors[0];

            if (isNaN(_chapter) || isNaN(_paraNum)){

                console.log("_chapter",_chapter);
                console.log("_paraNum",_paraNum);

                //console.log("typeof _chapter",typeof _chapter);
                //console.log("typeof _paraNum",typeof _paraNum);
                
                console.log("Could not fetch _chapter or _paraNum from anchors, next",anchors);

                await waitFor(500);
                continue;
            }
            if (this.books.indexOf(_book)==-1){
                console.log("_book is not listed here (%s), next",_book);
                await waitFor(500);
                continue;
            }

            if (validate.isEmpty(com.he)){
                console.log("\n\nNo commentary Hebrew (he) field?, next",com);
                await waitFor(50);
                continue;
            }

            if (validate.isEmpty(com.sourceHeRef)){
                console.log("No commentary sourceHeRef?, next",com);
                await waitFor(50);
                continue;
            }


            let commData={index:com._id,bodyHe:com.he,titleHe:com.sourceHeRef };

            if (!validate.isEmpty(com.type)) commData.type=com.type;
            if (!validate.isEmpty(com.collectiveTitle.he)) commData.collectiveTitleHe=com.collectiveTitle.he;
            if (!validate.isEmpty(com.collectiveTitle.en)) commData.collectiveTitleEn=com.collectiveTitle.en;
            if (!validate.isEmpty(com['index_title'])) commData.indexTitleEn=com['index_title'];
            if (!validate.isEmpty(com.heVersionTitleInHebrew)) commData.heVersionTitleInHebrew=com.heVersionTitleInHebrew;


            let categories=[
            'ספורנו','רש"י','משנה תורה', 'מצודת ציון','ג"בלר','המכח תומולעת','א״בשר','א"ביר','לאננח וניבר',
            'מלבי"ם','כלי יקר','גור אריה','ביאור שטיינזלץ','הנוי וניבר','ינועמש טוקלי','רבוב תורעה','רבד קמעה',
            'אור החיים','אבן עזרא','תלמוד ירושלמי','םידיגנ ירבד',
            'תרגום אונקלוס','תניא','תוספתא','תוספות יום טום','תוספות',
            'שו"ת במראה','רשב"א','רשב"ם','שד"ל','רמב"ן',
            'תקוני הזהר','רד"ק','רבנו בחיי','משך חכמה','ליקוטי מוהר"ן',
            'זוהר','זוהר חדש','דעת זקנים','ברטנורא',
            'הרמב"ם','אדרת אליהו','אברבנאל','אבות דרבי נתן',
            'שיחות הר"ן','תרגום יונתן','תרגום ירושלמי',
            'תרגום ניאופיטי','שמירת הלשון'
            ];
            categories.forEach(cat=>{
                
                    if (commData.collectiveTitleHe.includes(cat)){
                        commData.myCategory=cat;
                    }
                
            });
            if (!commData.myCategory){commData.myCategory=commData.collectiveTitleHe;}

            let paraId=null;
            try{
                paraId=await app.models.Paragraphs.findOne({where:{book:_book,chapter:_chapter,paraNum:_paraNum}});
                if (!paraId.id) throw new Error();
                
                
            }catch(err){
                console.log("Cannot find corresponding paragraph for book(%s), chapter(%s), paraNum(%d), next",_book,_chapter,_paraNum);
                await waitFor(2000);
                continue;
            }
            
            console.log("PARA ID",paraId.id);


            let commRes=[];
            try{
                commRes=await app.models.Commentaries.findOrCreate({where:{index:com._id}},commData);
            }catch(err){
            console.log("findOrCreate err on models.Commentaries, probably data is too long");
               // console.log("findOrCreate err",err)
                await waitFor(50);
                continue;
            }

            
            if (commRes.length==0 || !commRes[0].id){
              console.log("Could not create new record on Commentaries model, next");
              await waitFor(50);
              continue;
            }



            let notesMapData={lastParaId:paraId.id,noteType:'commentary',commentaryId:commRes[0].id};

            
            let notesMapRes=null;
            try{
            notesMapRes=await app.models.Notesmap.findOrCreate({where:notesMapData},notesMapData);
            }catch(err){
                console.log("NOTES MAP DATA?",notesMapData);
                console.log("Could not create notes_map, err",err);
                process.exit(0);
            }

            console.log("Created new NotesMap with id",notesMapRes[0].id);
            

        }

        //process.exit(0);

    }

    async beginImport(){

        let bIndex=this.bibleIndex.getBibleIndex();
        //console.log("bIndex?",bIndex);
        //console.log("bIndex[Gensis]?",bIndex['Genesis']);

        let err=null,res=null;
        let book=null;

        //console.log("bIndex.length?",bIndex.length);
        for (let bi=0;bi<Object.keys(bIndex).length;bi++){

        
            book=Object.keys(bIndex)[bi];

            if (book!='Genesis') process.exit();
            
            //book='Exodus';
            //chapter=14;
            console.log("\n\n~~~BOOK~~~",book);
            
            //let book=bIndex[bi];

            
            await this.beginRecursiveImport(book);
            //break;
            //process.exit(0);

            //if (bi>1) break;
            

        }

    }

}


(async()=>{


await boot(app, __dirname);

//const Paragraphs=app.models.Paragraphs;

(new SefariaImporter()).beginImport();


})();


/*
1. Iterate through persons_locations files
2. match location line, para line
3. extract book,chapter,para_num, word_num, instanceWord, originWord
4. ask if it is a person or location
5. if it's a person - write into persons table with notes_map
6. if it's a location - search for a match, ask if it's correct
7. if it is not correct, write into notes_map and locations with long,lat null
*/


//node -r esm server/PersonsLocationsImporter.js
'use strict';

import BibleIndex from './../src/handlers/BibleIndex';
import AsyncTools from './../src/modules/tools/AsyncTools';
import axios from 'axios';
import _ from 'lodash';

const csv = require('csv-parser');
const fs = require('fs');
const loopback = require('loopback');
const boot     = require('loopback-boot');
const path     = require('path');
const app      = module.exports = loopback();
const {once}   = require('events');
const readline = require('readline');

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
require('./routes')(app);


function filterHebrew(st){
  
  //console.log("t0",text);
  //return st;
  let fSt="";
  st=st.replace(/,\-\־_;/," ");
  
  for (let i=0;i<st.length;i++){
    //console.log("CHAR?",text[i]);
    let c=st[i];
    if (c.match(/[אבגדהוזחטיכלמנסעפצקרשתץףםן]/)){
      fSt+=""+c;
    }else{
        //console.log(c+"");
    }
  }

  return fSt;


}

class PersonsLocationsImporter{
  
  constructor(){

    this.newRow={};
    this.results=[];
    this.ref={};
    //this.books=['Genesis','Exodus','Leviticus','Numbers','Deuteronomy'];
    
    this.books=[
      {file:'GenesisProperNouns.txt',book:'Genesis'},
      {file:'ExodusProperNouns.txt',book:'Exodus'},
      {file:'LeviticusProperNouns.txt',book:'Leviticus'},
      {file:'NumbersProperNouns.txt',book:'Numbers'},
      {file:'DeuteronomyProperNouns.txt',book:'Deuteronomy'}
    ]
    this.matched=[];
    this.errs=[];

  }

  

  async import(){

    try{
      
      console.log("Cleaning notes_map with note_type=location ...");
      await app.models.Notesmap.destroyAll({noteType:'location'});
      console.log("Cleaning notes_map with note_type=unknown ...");
      await app.models.Notesmap.destroyAll({noteType:'unknown'});

      for (let x=0;x<this.books.length;x++){
          console.log("\n\n~~~~~ Parsing %s ~~~~~~\n\n",this.books[x].book);
          await AsyncTools.waitFor(3000);
          await this.parseFile(this.books[x]);
      }
      
      //await this.recordToDB();

    }catch(err){
      console.log("import err",err);
    }
    //console.log("paragraphs?",app.models.Paragraphs);
  }



  async saveInterpretation(oc){

        console.log("saveInterpretation is launched");

        oc.phase=filterHebrew(oc.phase);
        oc.inter=filterHebrew(oc.inter);

        
        let interRes=null;
      
        interRes=await app.models.Interpretations.find({where:{phase:oc.phase}});
      

        console.log("interRes",interRes);

        if (interRes.length==0){

          console.log("interRes is null for phase:",oc.phase);
          //process.exit(0);
          await app.models.Interpretations.create({phase:oc.phase,alts:JSON.stringify([oc.inter])});

        }else{
          interRes=interRes[0];
          console.log("inter res is not null");
          let alts=JSON.parse(interRes.alts);
          console.log("alts",alts);
          console.log("inter",oc.inter);

          if (alts.find((x)=>x==oc.inter)){

            console.log("Alt exist, nothing to do\n\n")


          }else{
            
            console.log("No such alt, adding new alt (%s)\n\n",oc.inter);
            alts.push(oc.inter);
            oc.alts=JSON.stringify(alts);
            await app.models.Interpretations.upsert({alts:oc.alts,id:interRes.id,phase:interRes.phase});
            //process.exit(0);
          }
        }
        

  }

  async saveLocation(oc,locRes){

    //console.log("match location with oc",oc);
    //process.exit(0);

    let verbose=false;

    oc.phase=filterHebrew(oc.phase);

    if (verbose) console.log("LOCATION MATCHED");
    //process.exit(0);
    this.matched.push({locRes:locRes,phase:oc.phase});
    
    let paraData={book:oc.book,chapter:oc.chapter,paraNum:oc.paraNum};
    let paraRes=await app.models.Paragraphs.findOne({where:paraData});
    if (paraRes==null){
      this.errs.push({msg:'could not find corresponding para for paraData',paraData});
      return;  
    }
    //if (paraRes.id==997) verbose=true;
    if (verbose) console.log("\n\n\n\n~~~~~~~~~\n\nmatchLocation with oc",oc);
    if (verbose) console.log("locRes",locRes);
    if (verbose) console.log("paraRes",paraRes);

    let notesMap={locationId:locRes.id,wordNum:oc.wordNum,lastParaId:paraRes.id,noteType:'location',term:oc.inter,userId:1};
    
    let nMapRes=await app.models.Notesmap.create(notesMap);

    if (!nMapRes.id){
          throw new Error("could not create a new entry for NotesMap with data?",nMapData);
    }

    
    if (verbose) console.log("\n\n~~~New entry created for notes_map",nMapRes);
    if (verbose) console.log(oc);
    //if (verbose) console.log("oc",oc);
    
  }

  async saveUnknown(oc){

    let paraData={book:oc.book,chapter:oc.chapter,paraNum:oc.paraNum};
    let paraRes=await app.models.Paragraphs.findOne({where:paraData});
    if (paraRes==null){
      this.errs.push({msg:'could not find corresponding para for paraData',paraData});
      return;  
    }
    oc.inter=filterHebrew(oc.inter);
    let notesMap={wordNum:oc.wordNum,lastParaId:paraRes.id,noteType:'unknown',term:oc.phase,contextTerm:oc.inter,userId:1};
    let nMapRes=await app.models.Notesmap.create(notesMap);

    if (!nMapRes.id){
          throw new Error("Could not create a new entry for NotesMap with data?",nMapData);
    }

    console.log("\n\n~~~New entry created for notes_map",nMapRes);
    console.log(oc);
    //if (oc.chapter=='4' && oc.paraNum=='17') process.exit(0);



  }



  async saveRecord(oc){

        //await this.saveInterpretation(oc);
        let locRes=await app.models.Locations.findOne({where:{or:[ {alsoCalled:{like:oc.phase}},{title:oc.phase}]} });
        if (locRes!==null){
          await this.saveLocation(oc,locRes);
        }else{
          await this.saveUnknown(oc);
        }

  }


  async parseFile(bookObj){
    
    let book=bookObj.book;
    let fileName=bookObj.file;
    let defer=AsyncTools.defer();
    let oc={};    
    let i=0;
    let lines = fs.readFileSync(path.join(__dirname,'./../scripts/csv/persons_locations/'+fileName), 'utf-8').split('\n').filter(Boolean);
    let line=null;
    let isFirstPara=false;

    for (let i=0;i<lines.length;i++){

      line=lines[i];
      //console.log("\nline",line);
      let isPara=(i%2!=0 && i>0);
      if (!isPara){
        //console.log("not para (%d)",i);
        let res=line.match(/\.(\d+)\.(\d+)/);        
        if (res!=null && res[1] && res[2]){
          oc={chapter:res[1],paraNum:res[2],book:book};  
          
        }
      }else{

        if (line.indexOf('__')!==-1){

          line=line.replace(/\s+/,' ');
          let res=line.match(/(\S+)__(\S+)/g);
          //console.log("res?",res);
          if (res==null){continue;}
          for (let ir=0;ir<res.length;ir++){

            let rres=res[ir].split('__');

            if (rres!=null && rres[0] && rres[1]){
            
              oc.inter=rres[0];
              oc.phase=filterHebrew(rres[1]);
              let words=line.split(" ");
              oc.wordNum=words.indexOf(res[ir]);
              oc.para=line;
              
              await this.saveRecord(oc);
           
            }
          }
        }
      }
    }
    defer.resolve();


  //console.log("location matched",this.matched);
  console.log("total matched",this.matched.length);
  
  //defer.resolve();
  return defer;
  

  }


}



(async()=>{


await boot(app, __dirname);

//const Paragraphs=app.models.Paragraphs;

await (new PersonsLocationsImporter).import();

process.exit(0);

})();

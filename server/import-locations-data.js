//node -r esm server/import-locations-data.js
'use strict';

import BibleIndex from './../src/handlers/BibleIndex';
import AsyncTools from './../src/modules/tools/AsyncTools';
import axios from 'axios';
import _ from 'lodash';

const csv = require('csv-parser');
const fs = require('fs');
const loopback = require('loopback');
const boot     = require('loopback-boot');
const path     = require('path');
const app      = module.exports = loopback();


app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));
require('./routes')(app);

class LocationsImporter{
  
  constructor(){

    this.newRow={};
    this.results=[];
    this.ref={};
    this.books=['Genesis','Exodus','Leviticus','Numbers','Deuteronomy'];

  }

  async generateLocationKeywords(lData){

    //console.log("\n\nlData?",lData);
    let title=lData.title;
    let newTitle=title;
    let alsoCalled=lData.alsoCalled;
    
      if (title.match(/[\(\)\?\/\,]/)){

      //console.log("title contains non alpha numeric chars",title);

      if (title.match(/\(.*\)/)  ){
         newTitle=title.replace(/\(.*\)/g,"");
      }
      if (title.match(/\?+/)  ){
         console.log("Contains multiple ?");
         newTitle=title.replace(/\?+/g,"");
      }

    }else{
      //console.log("normal title",title);
    }

    newTitle=newTitle.trim();
    //console.log("newTitle",newTitle);

    let cWords=[];

    if (alsoCalled!==null && alsoCalled!=='' && alsoCalled!==undefined){
      
      cWords=alsoCalled.split(",");
      cWords=cWords.map(cw=>{
        cw=cw.trim();
        if (cw.match(/\./g)){
          cw=cw.replace(/\./g,"");
        }


        return cw;
      });
      //console.log("cWords after map",cWords);



    }
    cWords.push(newTitle);

    let alt=cWords.join(",");


    

    return alt;


  }

  async import(){

    try{
      await this.readCSV();
      await this.recordToDB();

    }catch(err){
      console.log("import err",err);
    }



    //console.log("paragraphs?",app.models.Paragraphs);



  }

  async recordToDB(){

    let defer=AsyncTools.defer();

    if (this.results.length==0){
      console.log("No data to record to db");
      defer.resolve();
      return defer;
    }

    for(let i=0;i<this.results.length;i++){

      let row=this.results[i];
      //console.log("\n\nSaving row",row);
      
      if (row.refs.length==0){
        console.log("Row index (%d) has no refs, next");
        continue;
      }

      for (let refIndex=0;refIndex<row.refs.length;refIndex++){

        let ref=row.refs[refIndex];
        if (ref.book==null){
          console.log("ref is null, next");
          continue;
        }
        if (this.books.indexOf(ref.book)==-1){
          console.log("ref.book (%s) is unknown, next",ref.book);
          continue;
        }

        console.log("ref.book (%s) is valid",ref.book);
        //if (this.books.indexOf(ref))

        let q={where:{book:ref.book,chapter:ref.chapter,paraNum:ref.paraNum}};
        console.log("app.models.Paragraphs.findOne with q",q);
        
        let refRes={};
        try{
          
          refRes=await app.models.Paragraphs.findOne(q);

        }catch(refErr){

            console.log("Paragraphs query err",err);

        }

        console.log("refRes",refRes);

        if (refRes==null){
          console.log("No para index, next");
          continue;
        }



        let keys=['title','Latitude','Longitude','Periods','fewWords','body','alsoCalled'];
        let locationData={};
        
        keys.map(k=>{
          if (row[k]){
            if (k=='fewWords' || k=='alsoCalled' ){
              locationData[k]=row[k];
            }else{
              locationData[k.toLowerCase()]=row[k];  
            }
            
          }  
        });

        locationData.alsoCalled=await this.generateLocationKeywords(locationData);

        console.log("locationData",locationData);
        //await new Promise((res,rej)=>{setTimeout(()=>{  res();  },1000  )})   
        

        let locationRes=await app.models.Locations.findOrCreate({where:{title:row.title}},locationData);
        //console.log("locationRes?",locationRes);

        if (locationRes.length==0 || !locationRes[0].id){
          console.log("Could not create new record on Location model, next");
          continue;
        }


        let notesMapData={lastParaId:refRes.id,noteType:'location',locationId:locationRes[0].id};
        //console.log("NOTES MAP DATA?",notesMapData);
        let notesMapRes=await app.models.Notesmap.findOrCreate({where:notesMapData},notesMapData);

        console.log("Created new NotesMap with id",notesMapRes[0].id);
        

      }





      //if (i>3) break;



    }
    console.log("defer.resolve?");
    defer.resolve();
    return defer;

  }

  async readCSV(){

    let defer=AsyncTools.defer();

    console.log("LocationsImport.import launched");
    fs.createReadStream('./scripts/csv/locations_2018-08-01.csv')
    .pipe(csv())
    .on('data', (data) => {
        
        Object.keys(data).forEach((k)=>{
            
            //console.log("k %s: %s",k,data[k]);
            
            if (k.indexOf('Title')!==-1){
                
                //console.log("TITLE?",data[k]);
                this.newRow.title=data[k];
                return;
            }

            if (k.indexOf('Tanakh')!==-1){
                
                this.newRow.references=data[k].split(";");
                this.newRow.references.forEach(r=>{
                    
                    let parts=r.split('.');

                    //if (parts[0]=='1' && parts[1]=='1'){
                        //specifics.push({title:this.newRow.title,ref:r});
                    //}
                    let book=this.books[parts[0]]?this.books[parts[0]]:null;
                    this.ref={book,chapter:parseInt(parts[1])+1,paraNum:parseInt(parts[2])+1};
                    
                    if (!this.newRow.refs){this.newRow.refs=[]};
                    this.newRow.refs.push(this.ref);

                })

                return;
            }


            this.newRow[k]=data[k];
            this.ref={};


        });

        this.results.push(this.newRow);
        this.newRow={};
    }

        )
    .on('end', () => {

        //console.log("results",JSON.stringify(this.results,null,'\t'));
        //console.log("Specifics",specifics);
        //console.log("this.results length",this.results.length);
        defer.resolve();
      
    });

    return defer;
  

  }


}



(async()=>{


await boot(app, __dirname);

//const Paragraphs=app.models.Paragraphs;

await (new LocationsImporter).import();

process.exit(0);

})();

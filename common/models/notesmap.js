'use strict';

//import AsyncTools from './../../src/modules/tools/AsyncTools';
const axios = require('axios');
const squel = require('squel');
const validate =require('validate.js');
//const _ =require('lodash');


module.exports = function (Notesmap) {

        

    Notesmap.removeNote=(noteId,noteType,options,cb)=>{

        
        (async()=>{

            console.log("Remove note is launched with noteId(%d), noteType(%s), options(%o)",noteId,noteType,options);
            
            const token = options && options.accessToken;
            const userId = token && token.userId;

            console.log("userId",userId);

            try{

                if (validate.isEmpty(userId) || !validate.isNumber(userId)){
                    throw new Error("Invalid userId, aborting removeNote");
                }

                let query={};
                switch(noteType){
                    case 'comment':
                        query={where:{noteId:noteId,noteType:noteType,userId:userId}};        
                    break;
                    case 'question':
                        query={where:{questionId:noteId,noteType:noteType,userId:userId}};        
                    break;
                    case 'title':
                        query={where:{titleId:noteId,noteType:noteType,userId:userId}};        
                    break;
                }

                console.log("query",query);

                let res=null;
            
                
                res=await Notesmap.find(query);
                console.log("res?",res);

                if (!(res && res[0] && res[0].id && validate.isNumber(res[0].id) && res[0].id>0)){
                    throw new Error("No notesmap id, cannot delete note, aborting removeNote");
                }

                let delRes=null;
            
                delRes=await Notesmap.destroyById(res[0].id);

                console.log("delRes?",delRes);

                let delNoteRes=null;

                let model=null;
                console.log("noteType?",noteType);
                switch(noteType){
                    case 'comment':  model= Notesmap.app.models.Notes;    break;
                    case 'question': model= Notesmap.app.models.Questions;break;
                    case 'title':    model= Notesmap.app.models.Titles;   break;
                }
                delNoteRes=await model.destroyById(noteId);

                console.log("delNoteRes for noteType (%s) and noteId(%d)",noteType, noteId, delNoteRes);
            
            
            }catch(err){

                console.log("Notesmap.removeNote err",err); 
                return cb(null,{});

            }

            
            cb(null,{});


        })(cb);

    }

    
    

    Notesmap.remoteMethod('removeNote', {
        http: {verb: 'get'},
        accepts:[
            {'arg': 'noteId','type': 'number','http': {'source': 'query'}},
            {'arg': 'noteType','type': 'string','http': {'source': 'query'}},
            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
        ],
        returns: { type: 'object',root:true }
        });

    




    
}


'use strict';


module.exports = function (Notes) {


    Notes.getNotesForParaRange=function(fromBook,fromParaNum,toBook,toParaNum,cb){

        let fromBook='',toBook='',fromParaNum=1,toParaNum=22;
        
        //conncet relation to noteId
        //apply query
        //return
        //apply drawNotes on client side...




    }

    Notes.saveNote=function(noteData,options,cb){

        (async()=>{
            try{

            let app=Notes.app;

            const token = options && options.accessToken;
            const userId = token && token.userId;

            if (!userId){
                console.log("No authentication, cannot save note");
                return cb({},null);
            }

            console.log("Saving note with data noteData:",noteData);
            
            let model=null,newNoteData={};
            
            switch(noteData.noteType){
                
                case "question":
                    model=Notes.app.models.Questions;
                    
                    if (noteData.isOpened==1){
                        newNoteData={
                            isOpened:1,
                            openedQuestion:noteData.openedQuestion,
                            openedAnswer:noteData.openedAnswer
                        };
                    }else{
                        newNoteData={
                            isOpened:0,
                            rightAnswer:noteData.rightAnswer,
                            question:noteData.question,
                            firstAnswer:noteData.firstAnswer,
                            secondAnswer:noteData.secondAnswer,
                        };
                        let aa=['thirdAnswer','fourthAnswer','fifthAnswer','sixthAnswer'];
                         aa.forEach(aa=>{if (noteData[aa]) newNoteData[aa]=noteData[aa];});
                    }

                    newNoteData.noteType=noteData.noteType;                     
                    console.log("NOTE DATA FOR QUESTION",newNoteData);

                break;
                case "comment":
                    model=Notes;
                    newNoteData={note:noteData.note,quote:noteData.quote,noteType:noteData.noteType};
                break;
                case "title":
                    model=Notes.app.models.Titles;
                    newNoteData={title:noteData.title,noteType:noteData.noteType};
                break;
            }

            if (model==null){
                console.log("Could not save note because noteData.noteType is not defined or not in switch/case");
                return cb({err:"Could not save note"},null);
            }
            
            
            let res=null;

            if (noteData.isExisting && noteData.isExisting==true && !isNaN(noteData.noteId) && noteData.noteId>0){
                
                console.log("Updating notes model",noteData.noteType);

                await model.update({id:noteData.noteId},newNoteData);
                res={id:noteData.noteId};
            }else{
                console.log("Creating new record with newNoteData",newNoteData);
                res=await model.create(newNoteData);    
            }


            console.log("Notes.create/update res",res);
            

            let noteMapData={};
            
            switch(noteData.noteType){
                case "question":noteMapData={noteType:noteData.noteType,userId:userId,questionId:res.id,lastParaId:noteData.last_para_id};break;
                case "comment":noteMapData={noteType:noteData.noteType,userId:userId,noteId:res.id,toParaId:noteData.to_para_id,fromParaId:noteData.from_para_id,fromChar:noteData.from_char,toChar:noteData.to_char};break;
                case "title":noteMapData={noteType:noteData.noteType,userId:userId,titleId:res.id,fromParaId:noteData.from_para_id};break;
            }
            
            //let noteId=res.id;

            if (noteData.isExisting && noteData.isExisting==true && !isNaN(noteData.notemapId) && noteData.notemapId>0){
                console.log("No need to update notesmap model...")
                //await Notes.app.models.Notesmap.update({id:noteData.notemapId},noteMapData);
            }else{
                let r=await Notes.app.models.Notesmap.create(noteMapData);    
                console.log("Notesmap.create res",r);
            }
            
            
            return cb(null,{id:res.id,noteId:res.id});
            

            }catch(err){
                console.log("err",err);
                return cb(null,{});

            }

        })();
        

    }

    
    

    Notes.remoteMethod('saveNote', {
        http: {
            verb: 'post'
        },
        accepts:[
            {'arg': 'noteData','type': 'object'},
            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
        ],
        returns: { type: 'object',root:true }
    });


}


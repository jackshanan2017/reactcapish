'use strict';

const axios = require('axios');
const squel = require('squel');
const _ =require('lodash');
//const bibleIndex = require('/../../src/consts/bible-index.json');
const fs=require('fs');
const path=require('path');


const defer = () => {
        
        let res, rej,promise = new Promise((resolve, reject) => {res = resolve; rej = reject;});
        promise.isDone=false;
        promise.resolve = res; 
        promise.reject = rej;
        return promise;
}

module.exports = function (Paragraphs) {

        
    //Get customized paragraphs
    Paragraphs.getParagraphsByParas=(fromParaId,toParaId,cb)=>{

        console.log("getParagraphsByParas fromParaId (%d), toParaId (%d)",fromParaId,toParaId);
        
        let cWhere={id:{between:[fromParaId,toParaId]}};

        return Paragraphs.getParas(cWhere,true,cb);
    
    }

    Paragraphs.getAllBooks=(fromBook,toBook,fromChapter,fromParaNum,toChapter,toParaNum)=>{

        let bibleIndexJson=fs.readFileSync(path.resolve(__dirname,'../../src/constants/bible-index.json'));
        let bibleIndex=JSON.parse(bibleIndexJson);
        //console.log("bibleIndex",bibleIndex);

        let booksList=Object.keys(bibleIndex);

        let fromBookIndex=null;
        let toBookIndex=null;

        try{
            fromBookIndex=booksList.indexOf(fromBook);
            if (fromBookIndex==-1) throw new Error();

            toBookIndex=booksList.indexOf(toBook);
            if (toBookIndex==-1) throw new Error();

        }catch(err){
            
            console.log("err",err);
            throw new Error('Could not find index for either fromBook ('+fromBook+') or toBook ('+toBook+')');
        }

        //console.log("fromBookIndex?",fromBookIndex);
        //console.log("toBookIndex?",toBookIndex);
        let booksParas=[];

        if (fromBookIndex==toBookIndex){

            booksParas.push({book:booksList[fromBookIndex],allParas:false,fromParaNum,toChapter,fromChapter,toParaNum});

        }else{

            for (let i=fromBookIndex;i<=toBookIndex;i++){

                if (i==fromBookIndex){
                    booksParas.push({book:booksList[i],fromChapter,fromParaNum});
                    continue;    
                }
                if (i==toBookIndex){
                     booksParas.push({book:booksList[i],allParas:false,toParaNum,toChapter});
                    continue;       
                }

                booksParas.push({book:booksList[i],allParas:true});

            
            }

        }
        

        //console.log("booksParas",booksParas);
        
        //console.log("keys?",keys);
        return booksParas;


    }


    
    
    Paragraphs.getParagraphsRange=(fromBook,fromChapter,fromParaNum,toBook,toChapter,toParaNum,options,cb)=>{

        (async()=>{

        


        let forDocument=false;
        let bookParas=[];

        console.log("getParagraphsRange fromBook",fromBook);
        console.log("getParagraphsRange fromChapter",fromChapter);
        console.log("getParagraphsRange fromParaNum",fromParaNum);
        console.log("getParagraphsRange toBook",toBook);
        console.log("getParagraphsRange toChapter",toChapter);
        console.log("getParagraphsRange toParaNum",toParaNum);


        fromParaNum++;
        toParaNum++;

        let r=null,finalRes={paras:[],notes:[],questions:[],titles:[]};
        
        try{


            bookParas=Paragraphs.getAllBooks(fromBook,toBook,fromChapter,fromParaNum,toChapter,toParaNum);

            console.log("bookParas?",bookParas);
            console.log("bookParas.length?",bookParas.length);
            
                let cWhere={};
                if (bookParas.length==1){
                    //where book='genesis' and ((chapter=1 and paraNum>fromParaNum) or (chapter>2)
                    if (fromChapter==toChapter){
                        console.log("CASE 1");
                        cWhere={and:[
                            {book:fromBook,chapter:fromChapter,paraNum:{gte:fromParaNum}},
                            {book:fromBook,chapter:fromChapter,paraNum:{lte:toParaNum}}
                            ]
                        };

                    }else{
                        console.log("CASE 2");
                        
                            //fromChapter=3
                            //toChapter=4

                        cWhere={or:[
                            {book:fromBook,chapter:fromChapter,paraNum:{gte:fromParaNum}},
                            {book:fromBook,chapter:toChapter,paraNum:{lte:toParaNum}}
                            ]

                        }
                        
                        if (toChapter-fromChapter>1){
                                cWhere.or.push({book:fromBook,chapter:{between:[fromChapter+1,toChapter-1]} });
                        }   


                    }
                    
                }else{


                    let orQuery={},orQueries=[];
                    for (let bpIndex=0;bpIndex<bookParas.length;bpIndex++){

                        let bookPara=bookParas[bpIndex];
                        console.log("Looking paras for book ",bookPara);

                        if (bookPara.fromChapter){

                            //console.log("CASE 3");
                            orQueries.push({book:bookPara.book,chapter:bookPara.fromChapter,paraNum:{gte:bookPara.fromParaNum}});
                            orQueries.push({book:bookPara.book,chapter:{gt:bookPara.fromChapter}});
                        }

                        if (bookPara.allParas){
                            //console.log("CASE 4");
                            orQuery={book:bookPara.book};
                            orQueries.push(orQuery);
                        }

                        if (bookPara.toChapter){
                            //console.log("CASE 5");
                            orQueries.push({book:bookPara.book,chapter:{lt:bookPara.toChapter}});
                            orQueries.push({book:bookPara.book,chapter:bookPara.toChapter,paraNum:{lte:bookPara.toParaNum}});
                        }

                        console.log("orQuery before push",orQuery);

                    }
                    console.log("orQueries",orQueries);
                    cWhere={or:orQueries};

                }


                r=await Paragraphs.find({where:cWhere});

                //console.log("cWhere res?",r);

                finalRes.paras=r;

                        
                if (finalRes.paras.length>0){

                    //console.log("Fetching also notes for paras");
                    let fromParaId=finalRes.paras[0].id;
                    let toParaId=finalRes.paras[finalRes.paras.length-1].id;
                    let paras=finalRes.paras.map(para=>para.id);
    
                    finalRes.notes=await Paragraphs.getParasNotes(fromParaId,toParaId);
                    finalRes.questions=await Paragraphs.getParasQuestions(paras);
                    finalRes.titles=await Paragraphs.getParasTitles(paras);
                    finalRes.genericTitles=await Paragraphs.getParasGenericTitles(paras);
                    finalRes.locations=await Paragraphs.getParasLocations(paras);
                    finalRes.persons=await Paragraphs.getParasPersons(paras);
                    finalRes.unknowns=await Paragraphs.getParasUnknowns(paras);
                    
                
                }


        }catch(err){
            console.log(err);
            return cb(null,null);

        }

        

        cb(null,finalRes);

        })(cb);

        

    }

    //Get non customized paragraphs
    
    Paragraphs.getParagraphs=(book,chapter,options,cb)=>{

        //const token = options && options.accessToken;
        //const userId = token && token.userId;
        //if (!userId){
            //console.log("No authentication, cannot get paragraphs");
            //return cb({},null);
        //}
        
        let cWhere={book:book,chapter:chapter,userId:1};

        return Paragraphs.getParas(cWhere,false,cb);

    }



    Paragraphs.getParas=(cWhere=null,forDocument=false,cb)=>{

        
        (async()=>{
            
            let r=null,finalRes={paras:[],notes:[],questions:[],titles:[]};

        
            console.log("cWhere?",cWhere);

            
            try{r=await Paragraphs.find({where:cWhere});}catch (err){console.log("Paragraphs.find err",err);}

            if (r.length<5 && !forDocument){

                console.log("Less than five results from local db, fetching paras from Sefaria...");
                let paras = await Paragraphs.getParaFromSefaria(book,chapter);
                await Paragraphs.saveParasToDB(paras,book,chapter);

                try{r=await Paragraphs.find({where:{book,chapter}});}catch (err){console.log("Paragraphs.find err",err);}
                //return new Promise((res,rej)=>res(paras));
                //console.log("finalRes.paras",paras);
                finalRes.paras=r;

``
            }else{
                console.log("We have (%d) paras in local db, returning from local db...",r.length);
                //return new Promise((res,rej)=>res(r));            
                finalRes.paras=r;
            }

            //console.log("paragraphs find for book (%s) and chapter (%d)",book,chapter);

            if (finalRes.paras.length>0){

                //console.log("Fetching also notes for paras");
                let fromParaId=finalRes.paras[0].id;
                let toParaId=finalRes.paras[finalRes.paras.length-1].id;
                let paras=finalRes.paras.map(para=>para.id);

                try{
                
                finalRes.notes=await Paragraphs.getParasNotes(fromParaId,toParaId);
                finalRes.questions=await Paragraphs.getParasQuestions(paras);
                finalRes.titles=await Paragraphs.getParasTitles(paras);
                finalRes.genericTitles=await Paragraphs.getParasGenericTitles(paras);
                finalRes.locations=await Paragraphs.getParasLocations(paras);
                
                }catch(err){
                    console.log("getParasNotes or getParasQuestions err",err);
                }
            }
                        

            //console.log("paras finalRes",finalRes);

            cb(null,finalRes);
        
        })(cb);
        
        
    }

    Paragraphs.getParasTitles=async(paras)=>{
        
        let query= squel.select()
        .from("notes_map")
        .left_join("titles","t","t.id=notes_map.title_id")
        .where("t.id is not null")
        .where("notes_map.note_type='title'")
        //.where("notes_map.user_id != 1")
        .where("t.is_generic is null")
        .where("from_para_id in("+paras.join(',')+")")
        .toString();

        
        const con=Paragraphs.app.dataSources['msql'].connector;
        
        let p=defer();

        console.log("notes_map query for titles",query);
        
        con.execute(query,[],(err,res)=>{
            if (err) {p.reject(err);return}
            p.resolve(res);
        });

        return p;
    }

    Paragraphs.getParasGenericTitles=async(paras)=>{
        
        let query= squel.select()
        .from("notes_map")
        .left_join("titles","q","q.id=notes_map.title_id")
        .where("q.id is not null")
        .where("q.is_generic is not null")
        .where("notes_map.user_id = 1")
        .where("from_para_id in("+paras.join(',')+")")
        .toString();

        const con=Paragraphs.app.dataSources['msql'].connector;
        
        let p=defer();
        
        con.execute(query,[],(err,res)=>{
            if (err) {p.reject(err);return}
            p.resolve(res);
        });

        return p;
    }

    Paragraphs.getParasQuestions=async(paras)=>{

        
        let query= squel.select()
        .from("notes_map")
        .left_join("questions","q","q.id=notes_map.question_id")
        .where("last_para_id in("+paras.join(',')+")")
        .where("q.id is not null")
        .toString();

        //console.log("notes_map query for questions",query);
        const con=Paragraphs.app.dataSources['msql'].connector;
        
        let p=defer();
        
        con.execute(query,[],(err,res)=>{

            if (err) {p.reject(err);return}
            p.resolve(res);

            
        });

        return p;

    }

    Paragraphs.getParasUnknowns=async(paras)=>{

        console.log("getParasUnkowns is launched");
                
        let query= squel.select()
        .from("notes_map")
        .where("last_para_id in("+paras.join(',')+")")
        .where("note_type ='unknown'")
        .toString();

        console.log("notes_map query for unkowns",query);
        const con=Paragraphs.app.dataSources['msql'].connector;
        
        let p=defer();
        
        con.execute(query,[],(err,res)=>{
            if (err) {p.reject(err);return}
            p.resolve(res);  
        });

        return p;

    }

    Paragraphs.getParasLocations=async(paras)=>{

        
        let query= squel.select()
        .from("notes_map")
        .left_join("locations","l","l.id=notes_map.location_id")
        .where("last_para_id in("+paras.join(',')+")")
        .where("l.id is not null")
        .toString();

        console.log("notes_map query for locations",query);
        const con=Paragraphs.app.dataSources['msql'].connector;
        
        let p=defer();
        
        con.execute(query,[],(err,res)=>{
            if (err) {p.reject(err);return}
            p.resolve(res);  
        });

        return p;

    }

    Paragraphs.getParasPersons=async(paras)=>{

        
        let query= squel.select()
        .from("notes_map")
        .left_join("persons","p","p.id=notes_map.person_id")
        .where("last_para_id in("+paras.join(',')+")")
        .where("p.id is not null")
        .toString();

        console.log("notes_map query for persons",query);
        const con=Paragraphs.app.dataSources['msql'].connector;
        
        let p=defer();
        
        con.execute(query,[],(err,res)=>{
            if (err) {p.reject(err);return}
            p.resolve(res);  
        });

        return p;

    }

    Paragraphs.getParasNotes=async(fromParaId,toParaId)=>{

        //console.log("paras",paras);
        
        //console.log("fromParaId",fromParaId);
        //console.log("toParaId",toParaId);
        
        let query=squel.select().from("notes_map")
        .left_join("notes","notes","notes.id=notes_map.note_id")
        .left_join("Images","i","notes.imageId=i.id")
        
        .where("notes.id is not null")
        .where("from_para_id>=?",fromParaId)
        .where("to_para_id<=?",toParaId)
        .toString();

        console.log("notes_map query",query);
        const con=Paragraphs.app.dataSources['msql'].connector;
        
        let p=defer();
        
        con.execute(query,[],(err,res)=>{

            if (res && res.length>0){
                console.log("notes res length" ,res.length);    
            }
            
            if (err) {p.reject(err);return}
            
            const hostName = process.env.NODE_ENV == 'production' ? '' : 'http://localhost:8080';            
            
            res.forEach(r=>{
                r.image={path:`${hostName}/imgs/${r.category}/${r.id}.${r.format}`}
            });
            p.resolve(res);

            
        });

        return p;



    }


    Paragraphs.test=()=>{
        console.log("Paragraphs test");
    }
    Paragraphs.saveParasToDB=async(paras,book,chapter)=>{

        console.log("save paras to db with paras",paras);
        console.log("paras length?",paras.length);
        
        let r=null;
        for (let i=1;i<=paras.length;i++){

            r=await Paragraphs.findOne({where:{book:book,chapter:chapter,paraNum:i}});
            
            if (r==null){
                await Paragraphs.create({book:book,chapter:chapter,paraNum:i,text:paras[i-1]});
                console.log("Para (%d) has been saved",i);
            }else{
                console.log("Para (%d) already exist",i);
            }


        }

    }

    Paragraphs.getParaFromSefaria=async(book,chapter)=>{

        let res=null;
        try{
            res=await axios.get('https://www.sefaria.org/api/texts/'+book+'.'+chapter)
        }catch(err) {
            console.log(err);
        };
        return new Promise( (resolve,rej)=>{resolve(res.data.he);} );

    }
    
    //console.log("typeof remoteMethod",typeof Paragraphs.remoteMethod);
    

    

    
    Paragraphs.remoteMethod('getParagraphsRange', {
        http: {verb: 'get'},
        accepts:[
            {'arg': 'fromBook','type': 'string','http': {'source': 'query'}},
            {'arg': 'fromChapter','type': 'number','http': {'source': 'query'}},
            {'arg': 'fromParaNum','type': 'number','http': {'source': 'query'}},
            {'arg': 'toBook','type': 'string','http': {'source': 'query'}},
            {'arg': 'toChapter','type': 'number','http': {'source': 'query'}},
            {'arg': 'toParaNum','type': 'number','http': {'source': 'query'}},

            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
        ],
        returns: { type: 'object',root:true }
        });
    Paragraphs.remoteMethod('getParagraphs', {
        http: {verb: 'get'},
        accepts:[
            {'arg': 'book','type': 'string','http': {'source': 'query'}},
            {'arg': 'chapter','type': 'number','http': {'source': 'query'}},
            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
        ],
        returns: { type: 'object',root:true }
        });

    
    
}


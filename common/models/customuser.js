'use strict';

var logUser = require('debug')('model:user');
var path = require('path');
const asyncTools = require('./../../server/modules/AsyncTools.js');
const tools = require('./../../server/modules/tools');
const squel = require('squel');
const { exeQuery } = require('./../../server/modules/db/queries');
const to = require('./../../server/modules/to');

module.exports = function (Customuser) {
	const rolesEnum = {
		0: 'NOROLE',
		1: 'ADMIN',
		2: 'SUPERADMIN',
		3: 'TEACHER',
		4: 'SCHOOL'
	}

	const rolesEncodes = {
		NOROLE:     "miremerijfgivn238svnsdfsdf",
		ADMIN:      "gmrkipgm$2300femkFSFKeo375",
		SUPERADMIN: "spf%#kfpoFFAa2234adAA244asZZv",
		TEACHER:    "fkoewpAQEK345FF2gr437",
		SCHOOL:     "ccNgfs45KMNddojip##zzmMMqlpa"
	}

	/** This call adds custom behaviour to the standard Loopback login.
	 *
	 *  Since it uses the User.login function of the User model, let's also
	 *  keep the same parameter structure.
	 */

	const emailOptions = {
		type: 'email',
		senderAddress: 'eran.gep@gmail.com',
		subject: 'תודה על ההרשמה',
		text: 'נא אשר את ההרשמה על ידי לחיצה על הקישור הבא: ',
		closing: '\n\nתודה, \nצוות קאפיש',
		redirect: 'http://localhost:3000',
		// redirect: '/verified',
		host: '0.0.0.0',
		port: 8080
	}

	function getRoleCode(roleId) {
		return rolesEncodes[rolesEnum[roleId]];
	}

	//send verification email after registration 
	Customuser.afterRemote('create', function (context, user, next) {

		console.log("Custom user after create is launched");
		const sgMail = require('@sendgrid/mail');
		const SENDGRID_API_KEY = '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';
		// sgMail.setApiKey(process.env.SENDGRID_API_KEY);
		sgMail.setApiKey(SENDGRID_API_KEY);


		//TODO Shira change back to sendgrid
		var options = {
			mailer: sgMail,
			type: emailOptions.type,
			to: user.email,
			from: emailOptions.senderAddress,
			subject: emailOptions.subject,
			text: emailOptions.text + '\n\t{href}' + emailOptions.closing,
			template: path.resolve(__dirname, '../../server/views/verify.ejs'),

			//determine host according to env
			//redirect to react's app.... 
			// http://localhost:3000/verified-login or http://localhost:3000/login?popup=

			redirect: emailOptions.redirect,
			user: user,
			host: emailOptions.host,
			port: emailOptions.port
		};

		//send the email, and callback is invoked after the actions takes place
		if (!user.emailVerified) { //not sure this if is neccessary
			user.verify(options, function (err, response) {
				console.log("user.verify is launched");
				if (err) {
					console.log("verification err?",err);
					Customuser.deleteById(user.id);
					return next(err);
				}

				console.log("The verification email was now sent with the email-options: ", options);
				return next();


				//context.res.render('response'); //if using this then response.ejs should be empty!
				//without rendering the response the client side doesn't know that the response was accepted
				/*
				context.res.render('response', {
					title: 'Signed up successfully',
					content: 'Please check your email and click on the verification link ' +
						'before logging in.',
					redirectTo: '/login',
					redirectToLinkText: 'Log in'
				});
				*/
			});
		}
	});


	// this function saves the extra data of the user after user was verified
	// the extra_data_verified is a strigified json saved in CustomUser table
	Customuser.saveExtraDataVerified = async function (user, cb) {
		console.log("save extra data verified is launched now!!");

		let extraDataVerified = {};
		try {
			extraDataVerified = JSON.parse(user.extra_data_verified);
		} catch (error) {
			return cb(`Couldn't parse the extra data verified of current user. 
				Cannot save user with their data. Removing user from database.`, false);
		}

		let success = true;
		for (let modelName in extraDataVerified) {

			let modelData = extraDataVerified[modelName];
			for (let column in modelData) {
				if (modelData[column] == -1) {
					modelData[column] = user.id;
				}
			}

			//create
			let [err, content] = await to(Customuser.app.models[modelName].create(modelData));
			if (err) {
				console.log("Error saving data to %s model", modelName);
				success = false;
			}
			else {
				console.log("Success saving  to model %s: content", modelName, content);
			}
		}

		return cb(null, success);
	}

	Customuser.afterRemote('prototype.verify', function (context, user, next) {
		context.res.render('response', {
			title: 'A Link to reverify your identity has been sent ' +
				'to your email successfully',
			content: 'Please check your email and click on the verification link ' +
				'before logging in',
			redirectTo: 'localhost:3000/home',
			redirectToLinkText: 'Log in'
		});
	});


	Customuser.registerOrLoginByUniqueField = (uField, uData, roleId, cb) => {
		(async () => {
			let query = { where: {} };
			query.where[uField] = uData[uField];
			console.log("quiery", query);
			let [err, res] = await asyncTools.to(Customuser.findOne(query));
			if (err) {
				console.log("Error on serch by field", err);
				return cb(err);
			}
			if (res) {
				console.log(`found by ${uField}`, res);
				return Customuser.directLoginAs(res.id, roleId, cb);
			}
			//create new user in db.
			let pass = tools.generatePassword(8);
			uData.password = pass;
			uData.emailVerified = 1;
			uData.verificationToken = null;
			let [error, newUser] = await asyncTools.to(Customuser.create(uData));
			if (error) {
				return cb(err);
			}
			logUser("~~we created new user~~\n", newUser, newUser.id);

			return Customuser.directLoginAs(newUser.id, roleId, cb); //creates accessToken for user
		})();
	}

	Customuser.directLoginAs = function (userId, roleId = null, fn) {

		userId = parseInt(userId);

		//var self=this;
		//debug("forceLogoutCurrentUser err",err);
		if (typeof roleId == "function") { //make roleid optional
			if (!fn) fn = roleId;
			roleId = null;
		}
		fn = fn || utils.createPromiseCallback();

		var realmDelimiter;
		var realmRequired = false;//!!(self.settings.realmRequired || self.settings.realmDelimiter);
		var query = { id: userId };

		this.findOne({ where: query }, (err, user) => {

			var defaultError = new Error('login failed');
			defaultError.statusCode = 401;
			defaultError.code = 'LOGIN_FAILED';

			var credentials = { ttl: 60 * 60, password: 'wrong-one', email: user.email };

			async function tokenHandler(err, token) {
				if (err) return fn(err);
				token.__data.user = user;
				if (roleId !== null) {
					token.__data.roleCode = getRoleCode(roleId);
					return fn(err, token);
				}
				Customuser.app.models.RoleMapping.findOne({ where: { principalId: userId } }, (err, roleRes) => {
					token.__data.roleCode = getRoleCode(roleRes && roleRes.roleId);
					return fn(err, token);
				})
			}

			if (err) {
				logUser('An error is reported from User.findOne: %j', err);
				return fn(defaultError);
			}

			if (!user) {
				logUser('No matching record is found for user %s', query.email || query.username);
				return fn(defaultError);
			}

			if (user.createAccessToken.length === 2) {
				logUser("user.createAccessToken.length is 2 ?", user.createAccessToken.length);
				user.createAccessToken(credentials.ttl, tokenHandler);
			} else {
				logUser("user.createAccessToken.length is NOT 2 ?", user.createAccessToken.length);
				user.createAccessToken(credentials.ttl, credentials, tokenHandler);
			}

		});

		return fn.promise;
	}

	Customuser.getUsersList = function (callback) {
		// let userModel = Customuser.app.models.User;
		return Customuser.find({}, function (errorrr, data) {
			if (errorrr) {
				logUser("error in find")
				console.log("ERROR!");
				return callback(errorrr)
			}

			let payload = data.map((item, index) => {
				return { id: item.id, username: item.username }
			})
			return callback(null, payload)
		});
	}

	Customuser.extendedLogin = function (credentials, include, callback) {
		// Invoke the default login function\
		// let userModel = Customuser.app.models.User;
		let rolemap = Customuser.app.models.RoleMapping;
		logUser("credn", credentials)
		logUser("this: ", this);
		logUser("login: ", Customuser.login);

		Customuser.login(credentials, include, function (loginErr, loginToken) {
			if (loginErr) {
				logUser("login error", loginErr);
				return callback(loginErr);
			}
			console.log("user logged!");
			loginToken = loginToken.toObject();

			//let userDetails=Customuser.find({filter:{include:{school}})

			Customuser.find({where:{id:loginToken.userId}},(err,userDetails)=>{
					
					if (err){
						console.log("Could not find user with id",loginToken.userId);
						return callback(null, null);
					}
					console.log("userDetails",userDetails);
					console.log("loginToken",loginToken);
					loginToken.userName=userDetails[0].username;

					rolemap.find({ where: { principalId: loginToken.userId } }, (err, userrole) => {
						if (err)
							return callback(err, null);
						loginToken.role = userrole[0] && userrole[0].roleId || null;
						if (!loginToken.role) return callback("no role", null);
						loginToken.compArr = getRoleCode(loginToken.role);
						logUser("login token:", loginToken);
						return callback(null, loginToken);
						//return component arr and save in session storage
					});

			});

			
		});
	};

	Customuser.getUser = (userId, options) => {
		return Customuser.findById(userId);
	}

	Customuser.getCurrUserRelData = (options, cb) => {
		const token = options && options.accessToken;
		const userId = token && token.userId;

		if (!userId) { console.log("couldnt find user"); return cb(null, null); }

		let relFields = {
			realm: true,
			email: true,
			city: true,
			education: true,
			days: true,
			phone: true,
			address: true,
			distance: true,
			profession: true,
			mainImageId: true
		};

		return Customuser.findById(userId, { fields: relFields });
	}

	Customuser.registerUser = (fd, options, cb) => {
		console.log("Customuser.registerUser is launched");

		const token = options && options.accessToken;
		const userId = token && token.userId;

		if (!userId) {
			return cb("AUTHORIZATION_ERROR", null);
		}

		//else update user
		fd.id = userId;

		(async (cb) => {

			let [error, userUpserted] = await to(Customuser.upsert(fd));
			if (error) { console.log("error upserting user", error); return cb(error, null); }

			//set emailVerified = true so the user can login
			let query = squel
				.update({ separator: "\n" })
				.table("CustomUser")
				.setFields({ emailVerified: true })
				.where("id=?", userId);

			let [err, userRes] = await exeQuery(query.toString(), Customuser.app);

			//if (err || !userRes) { console.log("exeQuery, setting extradataverified=true err:", err || userRes); cb({}); }

			/*
			Customuser.saveExtraDataVerified(userUpserted, (error, ok) => {
				if (!ok) {
					console.log("error saving extra data, deleting user: ", error);
					Customuser.deleteById(userId);
					return cb({});
				}

				return cb(null, { ok: true });
			});
			*/

			return cb(null, { ok: true });



		})(cb);
	}

	Customuser.authenticate = function (options, cb) {

		//console.log("authenticate is launched?!");

		const token = options && options.accessToken;
        const userId = token && token.userId;
        
        if (!userId){
            //console.log("No authentication");
            return cb(null,{isAuthenticated:false});
        }else{
        	//console.log("Authenticated");
        	return cb(null,{isAuthenticated:true});
        }

	}

	// this function is called after the user confirmed his email
	Customuser.confirm = function (uid, token, redirect, fn) {
		console.log("Customuser.confirm is launched now")
		fn = fn || utils.createPromiseCallback();
		this.findById(uid, function (err, user) {
			
			if (err) {
				return fn(err);
			}


			if (user && user.verificationToken === token) {
				user.verificationToken = null;
				user.emailVerified = true;
				user.save(function (err) {
					if (err) {
						fn(err);
					} else {
						fn();
						/*
						Customuser.saveExtraDataVerified(user, (error, ok) => {
							if (!ok) {
								console.log("error saving extra data, deleting user: ", error);
								Customuser.deleteById(user.id);
								return fn(error);
							}
						});
						*/
					}
				});

			} else {
				if (user) {
					err = new Error('Invalid token: %s', token);
					err.statusCode = 400;
					err.code = 'INVALID_TOKEN';
				} else {
					err = new Error('User not found: %s', uid);
					err.statusCode = 404;
					err.code = 'USER_NOT_FOUND';
				}
				fn(err);
			}
			
		});
		return fn.promise;
	};

	//-----------------REMOTE METHODS DECLARATION BEGINS

	Customuser.remoteMethod('getUsersList', {
		http: {
			verb: 'get'
		},
		returns: { arg: 'res', type: 'object' }
	});

	Customuser.remoteMethod('extendedLogin', {
		'http': {
			'path': '/elogin',
			'verb': 'post'
		},
		'accepts': [
			{
				'arg': 'credentials',
				'type': 'object',
				'description': 'Login credentials',
				'required': true,
				'http': {
					'source': 'body'
				}
			},
			{
				'arg': 'include',
				'type': 'string',
				'description': 'Related objects to include in the response. See the description of return value for more details.',
				'http': {
					'source': 'query'
				}
			}
		],
		'returns': [
			{
				'arg': 'token',
				'type': 'object',
				'root': true
			}
		]
	});

	Customuser.remoteMethod(
		'confirm',
		{
			description: 'Confirm a user registration with identity verification token.',
			accepts: [
				{ arg: 'uid', type: 'string', required: true },
				{ arg: 'token', type: 'string', required: true },
				{ arg: 'redirect', type: 'string' },
			],
			http: { verb: 'get', path: '/confirm' },
		}
	);

	Customuser.remoteMethod(
		'authenticate',
		{
			http: { verb: 'get'},
			description: 'Confirm a user authentication',
			accepts: [
				{ arg: "options", type: "object", http: "optionsFromRequest" }
			],
			returns: { type: 'object',root:true }
			
		}
	);

	Customuser.remoteMethod('getCurrUserRelData', {
		http: {
			verb: 'get'
		},
		accepts: [
			{ arg: "options", type: "object", http: "optionsFromRequest" }
		],
		returns: { arg: 'res', type: 'object', root: 'true' }
	});

	Customuser.remoteMethod('registerUser', {
		accepts: [
			{ arg: 'fd', type: 'object' },
			{ arg: "options", type: "object", http: "optionsFromRequest" }
		],
		returns: { arg: 'res', type: 'object', root: 'true' }
	});

	//-----------------REMOTE METHODS DECLARATION ENDS
};

// CREATE TABLE `CustomUser` (
// 	`id` int(11) NOT NULL AUTO_INCREMENT,
// 	`realm` varchar(512) DEFAULT NULL,
// 	`username` varchar(512) DEFAULT NULL,
// 	`password` varchar(512) DEFAULT NULL,
// 	`credentials` text,
// 	`email` varchar(512) NOT NULL,
// 	`emailVerified` tinyint(1) DEFAULT NULL,
// 	`verificationToken` varchar(512) DEFAULT NULL,
// 	`mainImageId` int(11) DEFAULT NULL,
// 	PRIMARY KEY (`id`)
//   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 
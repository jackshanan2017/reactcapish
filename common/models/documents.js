'use strict';

const LoopBackContext = require('loopback-context');


module.exports = function (Documents) {



    Documents.getUserDocument=function(docId,options,cb){

        console.log("get user document is launched with docId",docId);

        if (isNaN(docId)){
            console.log("Cannot Documents.getUserDocument cause no docId was provided");
            return cb({},null);
        }

        const token = options && options.accessToken;
        const userId = token && token.userId;


        if (!userId){
            console.log("No authentication, cannot load user document");
            return cb({},null);
        }



        (async(cb)=>{

            let res=[];
            try{
                res=await Documents.find({where:{id:docId,userId:userId}});

            }catch(err){
                console.error("Documents.find err",err)
                return cb({},null);
            };
            console.log("documents res",res);
            if (res.length==0){
                console.log("Could not find document (%d) for user Id (%d)",docId,userId);
                return cb({},null);
            }

            Documents.app.models.Paragraphs.getParagraphsByParas(res[0].fromParaId,res[0].toParaId,(err,docRes)=>{

                if (err) return cb({},null);
                docRes.title=res[0].title;
                return cb(null,docRes);
            });



        })(cb);

    }



    Documents.getUserDocuments=function(options,cb){

        
        
    console.log("get user documents is launched");

    (async(options,cb)=>{

        const token = options && options.accessToken;
        const userId = token && token.userId;


        if (!userId){
            console.log("No authentication, cannot load user documents");
            return cb({msg:"User must be logged in in order to load his documents"},null);
        }

        let res=null;
        try{
            res=await Documents.find({where:{userId},include:['CustomUser']});
            console.log("documents.find res?",res);
        }catch(err){
            return cb({},null);
        }

        res.forEach(doc=>{
           if (doc.CustomUser && doc.CustomUser.password){delete doc.CustomUser.password;}
           if (doc.CustomUser && doc.CustomUser.email){delete doc.CustomUser.email;}
        });

        //res=res.map(doc=>{
            
            //doc.CustomUser=JSON.stringify(doc.CustomUser);
            //doc.CustomUser={username:doc.CustomUser.username,id:doc.CustomUser.id,mainImageId:doc.CustomUser.mainImageId};
            //return doc;
        //})

        return cb(null,res);

    })(options,cb);



    }

    Documents.saveDocument=function(docData,options,cb){
        
        (async(docData,options,cb)=>{

            console.log("Saving note with data noteData:",docData);
            console.log("Saving note with data options:",options);
            
            const token = options && options.accessToken;
            const userId = token && token.userId;

            //console.log("USER ID?",userId);

            if (!userId){
                console.log("No authentication, cannot save document");
                return cb({msg:"User must be logged in in order to save his documents"},null);
            }

            if (!(docData.title && docData.title!=="" && docData.fromParaId && !isNaN(docData.fromParaId) && docData.toParaId && !isNaN(docData.toParaId))){
                console.log("One or more of the required fields is missing on document data",docData);
                return cb({msg:"One/Some Required fields were missing"});
            }

            
            docData.userId=userId;

            let docRes=null;

            try{
                docRes=await Documents.upsertWithWhere({fromParaId:docData.fromParaId,toParaId:docData.toParaId,title:docData.title},docData);

            }catch(err){
                console.log("find err",err);
            }

            console.log("doc res?",docRes);
            

            


        })(docData,options,cb);
        


        

    }

    
    

    Documents.remoteMethod('saveDocument', {
        http: {
            verb: 'post'
        },
        accepts:[
            {'arg': 'docData','type': 'object'},
            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
            
        ],
        returns: { type: 'object',root:true }
    });

    Documents.remoteMethod('getUserDocuments', {
        http: {verb: 'get'},
        accepts:[{"arg": "options", "type": "object", "http": "optionsFromRequest"}],
        returns: { type: 'object',root:true }
    });

    Documents.remoteMethod('getUserDocument', {
        http: {verb: 'get'},
        accepts:[
        {'arg': 'docId','type': 'number'},
        {"arg": "options", "type": "object", "http": "optionsFromRequest"}],
        returns: { type: 'object',root:true }
    });


}


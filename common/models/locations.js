'use strict';

module.exports = function (Locations) {

    Locations.matchNewLocation=(term,contextTerm,notemapId,locationId,long,lat,description,isNew,options,cb)=>{

        const token = options && options.accessToken;
        const userId = token && token.userId;
        console.log("userId",userId);
        
        console.log("Notesmap.matchNewLocation with args:");
        console.log("term",term);
        console.log("notemapId",notemapId);
        console.log("locationId",locationId);
        console.log("long",long);
        console.log("lat",lat);
        console.log("description",description);
        console.log("isNew",isNew);

        (async()=>{

            let lId=null;
            try{

                 
                 if (isNew && notemapId>0){
                     let newLoc=await Locations.create({title:term,latitude:lat,longitude:long,fewWords:description});
                     console.log("newLoc?",newLoc);
                     lId=newLoc.id;
                     if (!newLoc.id) throw new Error('Could not create a new record for locations with data ',{title:term,latitude:lat,longitude:long,fewWords:description});
                         
                 }else{
                     lId=locationId;
                 }

                 let uRes=await Locations.app.models.Notesmap.updateAll(
                     {or:[{noteType:'unknown',contextTerm:contextTerm},{noteType:'unknown',term:term},{id:notemapId}]},
                     {locationId:lId,noteType:'location'}

                     );
                 

            }catch(err){
                console.log("err",err);
                return cb(null,{});
            }

            return cb(null,{});

        })();

    }
    Locations.getLocationsForChapterByParaId=function(paraId,options,cb){

        (async()=>{

            console.log("getLocationsForChapterByParaId is launched with paraId(%d)",paraId);


            let paraObj={};
            let locs=[];
            
            try{
                
                paraObj=await Locations.app.models.Paragraphs.findOne({where:{id:paraId}});

                console.log("paraObj",paraObj);

                if (!paraObj.id) {
                    throw new Error("Could not find corresponding paragraph for paraId",paraId);
                    return;
                }

                let paras=await Locations.app.models.Paragraphs.find({where:{book:paraObj.book,chapter:paraObj.chapter}});

                console.log("paras",paras);

                let closeParaIds=[];
                paras.forEach(para=>{
                        closeParaIds.push(para.id)
                });

                let locations=await Locations.app.models.Notesmap.find({include:['Locations'],where: {noteType:'location',lastParaId: {inq: closeParaIds}}});

                locations=JSON.parse(JSON.stringify(locations));
                locations.filter(l=>l.Locations.id);

                let uniqueLoc=new Map();
                locations.forEach(loc=>{if (!uniqueLoc.has(loc.Locations.id)){uniqueLoc.set(loc.Locations.id,loc);}});

                locs=[...uniqueLoc.values()];
                console.log("Locations distincy by Locations.id?",locs);

            }catch(err){
                console.log("Err",err);
                return cb(null,[]);
            }

            

            cb(null,locs);

        })(cb);



    }

    
    Locations.remoteMethod('getLocationsForChapterByParaId', {
        http: {verb: 'get'},
        accepts:[
            {'arg': 'paraId','type': 'number','http': {'source': 'query'}},
            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
        ],
        returns: { type: 'array',root:true }
        });

    Locations.remoteMethod('matchNewLocation', {
        http: {verb: 'post'},
        accepts:[
            {'arg': 'term','type': 'string'},
            {'arg': 'contextTerm','type': 'string'},
            {'arg': 'notemapId','type': 'number'},
            {'arg': 'locationId','type': 'number'},
            {'arg': 'long','type': 'number'},
            {'arg': 'lat','type': 'number'},
            {'arg': 'description','type': 'string'},
            {'arg': 'isNew','type': 'boolean'},
            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
        ],
        returns: { type: 'object',root:true }
    });


}


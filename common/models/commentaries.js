'use strict';

const allowedComm=['Rashi','Sforno','Ramban','IbnEzra','Steinsaltz','Metzudat'];


module.exports = function (Commentaries) {


    Commentaries.getCommentariesForParaRange=function(fromParaId,toParaId,options,cb){

        (async()=>{

            console.log("getCommentariesForParaRange is launched with fromParaId(%d), toParaId(%d)",fromParaId,toParaId);


            let comms=[];
            try{
                comms=await Commentaries.app.models.Notesmap.find({include:['Commentaries'],where:{lastParaId:fromParaId,noteType:'commentary'}});}
            catch(err){
                console.log("Could not get Commentaries, err",err);
                return cb(null,[]);
            }

            console.log("comms",comms);

            if (comms.length==0){
                console.log("Could not find any commentaries");
            }

            comms=JSON.parse(JSON.stringify(comms));


            let commsMap={};
            let gComms=[],allowed=false;
            comms.forEach(comm=>{
                //console.log("iterating comm ",comm);
                
                if (comm.Commentaries && comm.Commentaries.collectiveTitleEn){

                    let group=comm.Commentaries.collectiveTitleEn.replace(/[\"\-\s']/g,'');

                    //console.log("commentary group",group);
                    allowed=false;
                    allowedComm.forEach(aComm=>{
                        if (group.indexOf(aComm)!==-1) allowed=true;
                    });

                    if (!allowed) return;
                    
                    if (commsMap[group]){
                        gComms=commsMap[group];
                    }else{
                        gComms=[];
                    }

                    gComms.push({paraIndex:comm.lastParaId,...comm.Commentaries});
                    commsMap[group]=gComms;

                }
            });
            
            //commsMap=[...commsMap];

            //console.log("Commentaries output",JSON.stringify(commsMap,null,'\t'));
            cb(null,commsMap);

        })(cb);



    }

    

    
    

    Commentaries.remoteMethod('getCommentariesForParaRange', {
        http: {verb: 'get'},
        accepts:[
            {'arg': 'fromParaId','type': 'number','http': {'source': 'query'}},
            {'arg': 'toParaId','type': 'number','http': {'source': 'query'}},
            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
        ],
        returns: { type: 'array',root:true }
        });


}


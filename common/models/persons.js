'use strict';

module.exports = function (Persons) {

Persons.matchNewPerson=(term,contextTerm,personId,description,notemapId,isNew,options,cb)=>{

        const token = options && options.accessToken;
        const userId = token && token.userId;
        console.log("userId",userId);
        
        console.log("Notesmap.matchNewLocation with args:");
        console.log("term",term);
        console.log("personId",personId);
        console.log("description",description);
        console.log("notemapId",notemapId);
        console.log("isNew",isNew);

        (async()=>{

            let pId=null;
            try{                 
                 if (isNew && personId==0){
                     let newPer=await Persons.create({term,description});
                     console.log("newPer?",newPer);
                     pId=newPer.id;
                     if (!newPer.id) throw new Error('Could not create a new record for persons with data ',{term,description});
                         
                 }else{
                     pId=personId;
                 }

                 let uRes=await Persons.app.models.Notesmap.updateAll(
                     {or:[{noteType:'unknown',contextTerm:contextTerm},{noteType:'unknown',term:term},{id:notemapId}]},
                     {personId:pId,noteType:'person'}

                     );
                 

            }catch(err){
                console.log("err",err);
                return cb(null,{});
            }

            return cb(null,{});

        })();

    }

    Persons.remoteMethod('matchNewPerson', {
        http: {verb: 'post'},
        accepts:[
            {'arg': 'term','type': 'string'},
            {'arg': 'contextTerm','type': 'string'},
            {'arg': 'personId','type': 'number'},
            {'arg': 'description','type': 'string'},
            {'arg': 'notemapId','type': 'number'},            
            {'arg': 'isNew','type': 'boolean'},
            {"arg": "options", "type": "object", "http": "optionsFromRequest"}
        ],
        returns: { type: 'object',root:true }
    });

}


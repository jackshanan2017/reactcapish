delete from questions;
delete from notes_map where note_type='question';

delete from notes_map where title_id in (select id from titles where is_generic is null);
delete from titles where is_generic is null;

delete from notes_map where note_type='comment';
delete from notes;
